#Cleanup
rm(list=ls())
cat("\014")

#Options setup
options(datatable.optimize=2) #For CUDA
options(warn=-1)


#Set working dir
work.dir = "/home/herz/Documents/ABI/EagleEye/Code/Explore"
setwd(work.dir)
file.name = "Oakland Union Table.xlsx"

#Import Libraries
#Pipleline and Data Libraries
library(dplyr)
library(tidyr)
library(data.table)
library(readxl)

#Plotting Libraries
library(plotly)
library(ggplot2)
library(ggvis)
library(GGally)

#new labraries
library(Rmisc)

#Program variables/settings
sample.size = -1
plot.pairs  = FALSE

#Read Data
df.poc = read_excel(path = file.name,skip = 1)
dt.poc = as.data.table(df.poc)
rm(df.poc)

#Sample Data
if (sample.size>0){
    sample.size = min(sample.size,nrow(dt.poc))
    dt.poc = sample_n(dt.poc, size = sample.size)
}

#Define Column Types
#Actively used variables
var.active <- c( "consolidatedName",
                 "consolidatedLatitude",
                 "consolidatedLongitude",
                 "abi_buyer",
                 "google_category",
                 "google_rating",
                 "google_price",
                 "google_zagat_selected",
                 "google_total_reviews",
                 "google_n_photos",
                 "channel",
                 "subchannel",
                 "segment",
                 "abi_volume",
                 "volume_potential",
                 "knownPOC",
                 "influencer",
                 "classification")

#Numeric variables
var.numeric <- c( "consolidatedLatitude",
                  "consolidatedLongitude",
                  "google_rating",
                  "google_price",
                  "google_zagat_selected",
                  "google_total_reviews",
                  "google_n_photos",
                  "abi_volume")

#assign categorical predictors // classification is predictor or target?
var.categorical <- c("google_category",
                     "channel",
                     "subchannel",
                     "segment",
                     "knownPOC")

#targets for training
var.target <-  c(  "abi_buyer",
                  "influencer",
                  "volume_potential",
                  "classification")


dt.poc <- dt.poc[, var.active, with = FALSE]


#Calculate indecies of the numeric categorical and target variables
idx.numeric <- match(var.numeric, names(dt.poc))
idx.categorical <- match(var.categorical, names(dt.poc))
idx.target <- match(var.target, names(dt.poc))


#Ensuring numeric data types are numeric.
for (i in seq_along(idx.numeric)) {
  idx <- idx.numeric[i]
  set(dt.poc, NULL, idx, as.numeric(dt.poc[[idx]]))
}

#Pair Plots


#plot maps
library(ggmap)
library(mapproj)

map <- get_map(location = c(-122.2, 37.75), zoom = 10, maptype="terrain")

PlotScatter <- function(dt.plot, col.name, map, na.rm = TRUE){
  g = ggmap(map)
  g = g + geom_point(data=dt.plot, 
              mapping = aes_string(x="consolidatedLongitude", y="consolidatedLatitude", color=column),
              alpha = .5, size = 0.05, na.rm = na.rm) 
  return(g)
}

PlotDensityMapCategorical <- function(dt, column, map, legend = FALSE){
  unique.vals = unique(dt[[column]])
  plot.list = list()
  
  for(i in seq_along(unique.vals)){
    dt.filtered <- dt %>%
      filter(.[[column]] == unique.vals[i])
    
    plot.list[[i]] = ggmap(map) +
      stat_density2d(data=dt.filtered, bins = 10,
                     mapping = aes(fill =..level.., alpha=..level.., 
                                   x=consolidatedLongitude, y=consolidatedLatitude),  
                     geom="polygon") +
      scale_alpha(range = c(0.00, 0.4), guide = FALSE) +
      scale_fill_gradient(low = "yellow", high = "red", trans="log", guide = legend) +
      labs(title=unique.vals[i])
  }
  return(plot.list)
}

plot.list.buyer <- PlotDensityMapCategorical(dt.poc, "abi_buyer", map)
png("abi_buyer.png", width= 1500, height=700)
multiplot(plotlist = plot.list.buyer, cols = 3)
dev.off()


plot.list.segment <- PlotDensityMapCategorical(dt.poc, "segment", map)
png("segment.png", width= 2000, height=1000)
multiplot(plotlist = plot.list.segment, cols = 6)
dev.off()

g <- PlotScatter(dt.poc, "segment", map)
g

PlotScatter(dt.poc, "segment", map)