#Cleanup
rm(list=ls())
cat("\014")

#Options setup
options(datatable.optimize=2) #For CUDA
options(warn=-1)


#Set working dir
work.dir = "./"
setwd(work.dir)
file.name = "Oakland Union Table.xlsx"

#Import Libraries
#Pipleline and Data Libraries
library(dplyr)
library(tidyr)
library(data.table)
library(readxl)

#Plotting Libraries
library(plotly)
library(ggplot2)
library(ggvis)
library(GGally)

#new labraries
library(Rmisc)

#Program variables/settings
sample.size = -1
plot.pairs  = FALSE

#Read Data
df.poc = read_excel(path = file.name,skip = 1)
dt.poc = as.data.table(df.poc)
rm(df.poc)

#Sample Data
if (sample.size>0){
    sample.size = min(sample.size,nrow(dt.poc))
    dt.poc = sample_n(dt.poc, size = sample.size)
}

#Define Column Types
#Actively used variables
var.active <- c( "consolidatedName",
                 "consolidatedLatitude",
                 "consolidatedLongitude",
                 "abi_buyer",
                 "google_category",
                 "google_rating",
                 "google_price",
                 "google_zagat_selected",
                 "google_total_reviews",
                 "google_n_photos",
                 "channel",
                 "subchannel",
                 "segment",
                 "abi_volume",
                 "volume_potential",
                 "knownPOC",
                 "influencer",
                 "classification")

#Numeric variables
var.numeric <- c( "consolidatedLatitude",
                  "consolidatedLongitude",
                  "google_rating",
                  "google_price",
                  "google_zagat_selected",
                  "google_total_reviews",
                  "google_n_photos",
                  "abi_volume")

#assign categorical predictors // classification is predictor or target?
var.categorical <- c("google_category",
                     "channel",
                     "subchannel",
                     "segment",
                     "knownPOC")

#targets for training
var.target <-  c(  "abi_buyer",
                  "influencer",
                  "volume_potential",
                  "classification")


dt.poc <- dt.poc[, var.active, with = FALSE]


#Calculate indecies of the numeric categorical and target variables
idx.numeric <- match(var.numeric, names(dt.poc))
idx.categorical <- match(var.categorical, names(dt.poc))
idx.target <- match(var.target, names(dt.poc))


#Ensuring numeric data types are numeric.
for (i in seq_along(idx.numeric)) {
  idx <- idx.numeric[i]
  set(dt.poc, NULL, idx, as.numeric(dt.poc[[idx]]))
}

#Pair Plots

if(plot.pairs){
  ggpairs(dt.poc, columns=idx.numeric, colour='segment',
          lower = list(continuous = wrap("points", alpha = 0.3,    size=0.2), 
                       combo = wrap("dot", alpha = 0.4,            size=0.2) ))
  
  ggpairs(dt.poc, columns=c(idx.numeric, idx.target), colour='segment',
          lower = list(continuous = wrap("points", alpha = 0.3,    size=0.2), 
                       combo = wrap("dot", alpha = 0.4,            size=0.2) ))
}

#Plot Funtions
PlotJitter = function(data,x,y,T.Func = identity,outlier.color = NA) {
  # Make Jitter pplot of variable x vs variable y, 
  # at least one of the variabels has to be categorical
  #
  # Args:
  # data: data.table of data.frame object
  # x   : column name of data, that will be plotted along x axis
  # y   : column name of data, that will be plotted along y axis
  # tFunc: tranformation function that is applied to y, by default identity
  # outlier.color: outlier color, if set to NA, then no outliers are plotted
  #
  # Returns:
  #   ggplot object
  # 
  # Example:
  # PlotJitter(df, 'class', 'grades', tFunc = log)
  plot <- data %>% 
    ggplot(aes(x= .[[x]],y= T.Func(.[[y]]))) +
    geom_jitter(alpha=0.05, na.rm = TRUE) +
    geom_boxplot(color = "red", outlier.colour = outlier.color, fill = NA) +
    xlab(x) + ylab(y)
  return(plot)
}


PlotPie <- function(data,column,title=column)
{
  pieChart <- data %>%
    group_by_(column) %>%
    summarise(total = n()) %>%
    plot_ly(labels= .[[column]]  ,values = total, type = "pie") %>%
    layout(title = paste(title, "Pie Chart", sep=" "))
  return(pieChart)
}
