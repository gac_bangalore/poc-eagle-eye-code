PlotClusters <- function(data) {
library(leaflet)
cluster.plot <- data %>%
  leaflet() %>% addTiles() %>% addMarkers(
  clusterOptions = markerClusterOptions(),
  popup = ~paste(consolidatedName, abi_volume, sep="<br/>")
)

return(cluster.plot)
}

##Test case
myData = dt.poc %>%
  filter(abi_buyer == "Yes") %>%
  rename(lat = consolidatedLatitude,lng = consolidatedLongitude)
  
PlotClusters(myData)

