import numpy as np
import pandas as pd
import os.path
import re
from tools.reports import report

def preprocessing(df):
    pass


def sellerIdx(df, col='sellerIdx'):
    '''
    This function is to create a new colume named sellerIdx which to
    indicate whether a POC is beer seller or not

    :param df: uniontable
    :param col: new column name, default is "sellerIdx"
    :type df: pandas data frame
    :type col: str
    :return:     extended uniontable withh sellerIdx
    '''

    # Names of google categories that sell beer
    yes_class = ['airport',
                 'amusement_park',
                 'bar',
                 'bowling_alley',
                 'casino',
                 'convenience_store',
                 'grocery_or_supermarket',
                 'liquor_store',
                 'night_club',
                 'stadium'
                 ]

    # Names of google categories that maybe sell beer
    maybe_class = ['art_gallery',
                   'cafe',
                   'department_store',
                   'food',
                   'meal_delivery',
                   'meal_takeaway',
                   'pharmacy',
                   'restaurant',
                   'shopping_mall',
                   'aquarium',
                   'spa',
                   'lodging',
                   'movie_theater',
                   'museum',
                   'train_station'
                   ]

    # Extract the category variable
    col_input = ['google_category', 'google_type', 'google_types', 'google_segmentation']
    all_cols = list(df.columns.values)
    col_target = (set(col_input) & set(all_cols))

    # If no matched variables found, then sellerIdx is NA
    if len(col_target) == 0:
        df[col] = np.nan
        print(" Warning: google_category is not available")
    else:
        # Create new column with all "NO"
        df[col] = "NO"
        col_target = list(col_target)[0]

        # If there are category values in yes class, then mark as Yes
        # If there are category values in maybe class, then mark as Maybe
        for index, row in df.iterrows():
            if isinstance(row[col_target], str):
                categories = row[col_target].split(',')

                if len((set(categories) & set(yes_class))) > 0:
                    df.set_value(index, col, "YES")
                elif len((set(categories) & set(maybe_class))) > 0:
                    df.set_value(index, col, 'Maybe')



    return df


def buyerIdx(df, col='buyerIdx'):
    '''
    This function is to create a new colume named buyerIdx which to
    indicate whether a POC is beer buyer or not

    :param df: uniontable
    :param col: new column name, default is "buyerIdx"
    :type df: pandas data frame
    :type col: str
    :return:     extended uniontable with buyerIdx
    '''

    abi_buyer = 'abi_buyer'
    abi_volume = ['abi_volume', 'abi_vol']

    # Extract the category variable
    all_cols = list(df.columns.values)
    col_target = (set(abi_volume) & set(all_cols))

    # If no matched variables found, then sellerIdx is NA
    if len(col_target) == 0:
        if abi_buyer in list(df.columns.values):
           df[col] = df['abi_buyer'].apply(lambda x:str(x).upper())
           df.loc[pd.isnull(df[col]), col] = "NO"
        else:
           df[col] = np.nan
           print(" Warning: neither abi_buyer nor abi_vol is available")

    else:
        col_target = list(col_target)[0]
        df[col] = "NO"
        df.loc[df[col_target] > 0, col] = "YES"


    return df


def knownIdx(df, col='knownIdx'):
    '''
    This function is to create a new colume named knownIdx which to
    indicate whether a POC is known in abi internal database

    :param df: uniontable
    :param col: new column name, default is "knownIdx"
    :type df: pandas data frame
    :type col: str
    :return:     extended uniontable with knownIdx
    '''

    abi_buyer = 'abi_buyer'
    abi_id = 'abi_id'

    if abi_id in list(df.columns.values):
        df[col] = "NO"
        df.loc[pd.notnull(df['abi_id']), col] = "YES"

    elif abi_buyer in list(df.columns.values):
        df[col] = "NO"
        df.loc[pd.notnull(df['abi_buyer']), col] = "YES"
    else:
        df[col] = np.nan
        print("Warning: abi_buyer is not available")

    return df


def channels_subs_segment(df, col=['channel', 'subchannel', 'segment']):
    """

    :param df: data frame (union table)
    :param col: list of column names to be generated
    :return: extended union table with channel, sub_channel, and segment
    """

    abi, flags = check_google_yelp_foursquare(df)

    look_up_tables = []

    col_input = []

    for index, flag in enumerate(flags):
        look_up_tables.append(read_table("data/reference/" + flag + "_Category.csv"))
        look_up_tables[index] = look_up_tables[index].set_index('category')
        col_input.append(flag.lower()+"_category")
        col_input.append(flag.lower()+"_types")
        col_input.append(flag.lower()+"_segmentation")
        df[col[0] + str(index+1)] = np.nan
        df[col[1] + str(index+1)] = np.nan
        df[col[2] + str(index+1)] = np.nan
        df[col[0] + str(index+1)] = df[col[0] + str(index+1)].astype('object') # pandas should do that by default, but it's not!!
        df[col[1] + str(index+1)] = df[col[1] + str(index+1)].astype('object')
        df[col[2] + str(index+1)] = df[col[2] + str(index+1)].astype('object')

    df[col[0]] = np.nan
    df[col[1]] = np.nan
    df[col[2]] = np.nan

    # Extract the category variable
    all_cols = list(df.columns.values)
    col_target = list_intersection(col_input, all_cols)
    # it should be 3 at max "dedupe_inputs_csv_example: set("Foursquare_type","Yelp_category","Google_segmentation")"
    # If no matched variables found, then all 3 new columns are NA

    if len(col_target) == 0 and abi == "NO":
        print(" Warning: not enough information to calculate channels, sub_channels, segments")

    elif len(col_target) == 0 and abi == "YES":
        if "abi_channel" in df.columns.values.tolist():
           df[col[0]] = df['abi_channel']
        else:
            df[col[0]] = np.nan
        if "abi_sub_channel" in df.columns.values.tolist():
           df[col[1]] = df['abi_sub_channel']
        else:
            df[col[1]] = np.nan
        if "abi_segment" in df.columns.values.tolist():
           df[col[2]] = df['abi_segment']
        else:
            df[col[2]] = np.nan

    else:
        for i in range(len(col_target)):
            col_target = col_target[i]
            for index, row in df.iterrows():
                if isinstance(row[col_target], str):
                    categories = row[col_target].split(',')[0]  # take the first word
                    if categories.lower() in look_up_tables[i].index.values.tolist():
                        df.set_value(index, col[0]+str(i+1), (look_up_tables[i].loc[categories.lower(), col[0]]))
                        df.set_value(index, col[1]+str(i+1), (look_up_tables[i].loc[categories.lower(), col[1]]))
                        df.set_value(index, col[2]+str(i+1), (look_up_tables[i].loc[categories.lower(), col[2]]))
            col_target = list_intersection(col_input, all_cols)

        col_target = list_intersection(col_input, all_cols)

        if abi == "YES":
            abi_in = 1
            if "abi_channel" in df.columns.values.tolist():
                df[col[0]+"0"] = df['abi_channel']
            else:
                df[col[0]+"0"] = np.nan
            if "abi_sub_channel" in df.columns.values.tolist():
                df[col[1]+"0"] = df['abi_sub_channel']
            else:
                df[col[1]+"0"] = np.nan
            if "abi_segment" in df.columns.values.tolist():
                df[col[2]+"0"] = df['abi_segment']
            else:
                df[col[2]+"0"] = np.nan

        elif abi == "NO":
            abi_in = 0

        for i in range(len(col_target) + abi_in):

            df[col[0]] = df[col[0]].fillna(df[col[0]+str(i+1-abi_in)])
            df[col[1]] = df[col[1]].fillna(df[col[1]+str(i+1-abi_in)])
            df[col[2]] = df[col[2]].fillna(df[col[2]+str(i+1-abi_in)])

        for i in range(len(col_target) + abi_in):
            df = df.drop(col[0]+str(i+1-abi_in), 1)
            df = df.drop(col[1]+str(i+1-abi_in), 1)
            df = df.drop(col[2]+str(i+1-abi_in), 1)

    return df


def list_intersection(a,b):
    """

    :param a: list
    :param b: list
    :return: list of the intersection between a and b, with order maintained from the first list

    set(a) & set(b): does not maintain order
    """
    c = []
    for element in a:
        if element in b:
            c.append(element)

    return c


def r_in_python(df):
    '''
    This function is to transform a pandas data frame into R data frame, call R script to do filter calculations,
    and then return a modified pandas data frame.

    :param df: uniontable
    :type df: pandas data frame
    :return:     extended uniontable


    '''
    #import is done here to use try except
    import rpy2.robjects as ro
    import pandas.rpy.common as com

    # Convert pandas to R data frame
    rdf = com.convert_to_r_dataframe(df)



    # In R, remove all variables from the workspace and consle
    ro.r('rm(list = ls())')
    ro.r('cat("\014")')

    # In R, create a data frame "uniontable"
    ro.globalenv['uniontable'] = rdf

    # Call the R script
    ro.r.source("filter_calculation/filterCalculation_new.R")

    return (com.load_data('uniontable'))


def  check_google_yelp_foursquare(df):
    """
    :param df: union table
    :return: list of columns' names based on input file name

    dedupe_inputs_csv_example: check_google_yelp_foursquare(["abi.csv","google.csv","another_file.csv"]) ==> ['google_category', 'google_type', 'google_segmentation']
    """
    path_str  = " ".join(df.columns.values.tolist())
    pattern1 = re.search('[fF][oO][uU][rR][sS][qQ][uU][aA][rR][eE]', path_str)
    pattern2 = re.search('[yY][eE][lL][pP]', path_str)
    pattern3 = re.search('[gG][oO][oO][gG][lL][eE]', path_str)
    pattern4 = re.search('[aA][bB][iI]', path_str)

    flags = []

    if pattern1 is not None:
        flags.append("Foursquare")

    if pattern2 is not None:
        flags.append("Yelp")

    if pattern3 is not None:
        flags.append("Google")

    if pattern4 is not None:
        abi = "YES"
    else:
        abi = "NO"

    return abi, flags


def filterCalculation(filepath = ["output/uniontable.csv"]):
    '''
    this function calls all other functions and generates analysis.csv
    :param filepath: output/uniontable.csv
    :type filepath: string
    :return:     analysis.csv an extended uniontable.csv

    '''


    print("Running Analysis")
    df = read_table(filepath[0])

    #df.to_csv(filepath[0][:len(filepath[0])-4] + "_recordLinkage.csv", sep=',', index=False)
    df.to_csv(os.path.splitext(filepath[0])[0] + "_recordLinkage.csv", sep=',', index=False)

    # renaming the old one

    print("Creating Seller Index")
    df = sellerIdx(df, col='sellerIdx')

    report.num_sellers = df["sellerIdx"].tolist().count("YES")
    report.num_non_sellers = df["sellerIdx"].tolist().count("NO")
    report.num_maybe_sellers = df["sellerIdx"].tolist().count("Maybe")

    report.sellers.loc['sellers'] = [report.num_sellers, report.num_non_sellers, report.num_maybe_sellers]

    print("Creating Known Index")
    df = knownIdx(df, col='knownIdx')

    report.num_known = df["knownIdx"].tolist().count("YES")
    report.num_unknown = df["knownIdx"].tolist().count("NO")

    report.known.loc['known'] = [report.num_known, report.num_unknown]

    print("Creating Buyer Index")
    df = buyerIdx(df, col='buyerIdx')

    report.num_buyers = df["buyerIdx"].tolist().count("YES")
    report.num_non_buyers = df["buyerIdx"].tolist().count("NO")

    report.buyers.loc['buyers'] = [report.num_buyers, report.num_non_buyers]

    print("Creating Channels, Sub_channels, Segment")
    df = channels_subs_segment(df, col=['channel', 'subchannel', 'segment'])



    print("Calcualting influencers and predicting volume")
    try:
        df = r_in_python(df)
        df.to_csv(filepath[0],sep=',',index = False)
        print("%s is updated" % (filepath[0]))
    except:
        df.to_csv(filepath[0],sep=',',index = False)
        print("Warning: filterCalculation_new.R was not used")
        print("%s is updated" % (filepath[0]))


def read_table(filename):
    df = pd.read_csv(filename, sep=',')
    return df

