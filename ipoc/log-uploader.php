<?php

define('LOG_FILE', '/home/caiovlp/poc-eagle-eye-python/ipoc/abi-data-importer.log');
define('BUCKET_PATH', 'gs://abi_places/data-import');

$last_upload_md5 = NULL;

while (TRUE) {
  if (file_exists(LOG_FILE)) {
    $md5 = md5_file(LOG_FILE);
    
    if ($md5 !== $last_upload_md5) {
      print exec('gsutil cp ' . LOG_FILE . ' ' . BUCKET_PATH . '/output.log') . "\n";
      $last_upload_md5 = $md5;
    }
  }

  sleep(1);
}