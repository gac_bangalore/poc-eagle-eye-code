<?php

set_time_limit(0);

define('REPO_ROOT', '/home/caiovlp/poc-eagle-eye-python');
define('SCRIPT_FILE', 'automated_eagle_eye.sh');
define('INPUT_PATH', REPO_ROOT . '/input');
define('OUTPUT_PATH', REPO_ROOT . '/output');
define('GSUTIL', '/root/google-cloud-sdk/bin/gsutil');
define('RESULTS_FILE', OUTPUT_PATH . '/unionTableAll.csv');
define('CLASSIFICATION_FILE', OUTPUT_PATH . '/classification.csv');
define('FILE_STATUS', '/tmp/status.json');
define('BUCKET_PATH', 'gs://abi_places/data-import');
define('DATABASE', 'abi_places_prod');
define('F1_ID_COLUMN_NAME', 'f1id');
define('F2_ID_COLUMN_NAME', 'f2id');

function main()
{
    print "Starting import process...\n";
    while (TRUE)
    {
        $status = read_status();
        if (in_array($status['status'], array('waiting', 'error', 'done', 'under_review', 'reviewed')))
        {
            wait_until_uploaded($status);
        }
        else if ($status['status'] == 'uploaded')
        {
            cleanup_and_update($status);
            copy_input_files($status);
            run_code($status);
            uploading_results($status);
            $success = import_results($status);

            if ($success)
            {
                complete_job($status);
            }
            else
            {
                set_error_status($status);
            }
        }
        else
        {
            set_error_status($status);
        }
    }
}

function wait_until_uploaded($status)
{
    while ($status['status'] != 'uploaded')
    {
        sleep(10);
        $status = read_status();
    }

    // Force job restart.
    exit();
}

function cleanup_and_update($status)
{
    print_timestamp('cleanup_and_update');
    print("updating code...\n");
    $command = 'cd ' . REPO_ROOT;
    print run_command($command);

    print run_command('rm -rf ' . INPUT_PATH);
    print run_command('rm -rf ' . OUTPUT_PATH);

    print run_command($command . '; git reset --hard');
    print run_command($command . '; git clean -fd');
    print run_command($command . '; git pull origin master');

    if (is_dir(INPUT_PATH)) {
        print run_command('rm -rf ' . INPUT_PATH);
    }

    if (is_dir(OUTPUT_PATH)) {
        print run_command('rm -rf ' . OUTPUT_PATH);
    }

    mkdir(INPUT_PATH);
    mkdir(OUTPUT_PATH);
}

function copy_input_files($status)
{
    print_timestamp('copy_input_files');
    print("moving files...\n");
    $status['status'] = 'moving';
    save_status($status);

    if (is_dir(INPUT_PATH)) {
        exec("rm -rf " . INPUT_PATH);
    }

    mkdir(INPUT_PATH);

    $command = GSUTIL . ' cp -a public-read ' . BUCKET_PATH . '/input/* ' . INPUT_PATH . '/';
    print run_command($command, FALSE);
}

function run_code($status)
{
    print_timestamp('run_code begin');
    $status['status'] = 'processing';
    save_status($status);

    print("running code...\n");
    $command = 'cd ' . REPO_ROOT . '; ./' . SCRIPT_FILE;
    print run_command($command);
    print_timestamp('run_code end');
}

function uploading_results($status)
{
    print_timestamp('uploading_results');
    print("uploading results...\n");
    $status['status'] = 'uploading_results';
    save_status($status);

    if (file_exists(RESULTS_FILE)) {
        $copy_results_cmd = GSUTIL . ' cp -a public-read ' . RESULTS_FILE;

        $command = $copy_results_cmd . ' ' . BUCKET_PATH . '/results.csv';
        print run_command($command, FALSE);

        $filename = 'results_' . $status['cityId'] . '_' . date('Ymd-His') . '.csv';

        $command = $copy_results_cmd . ' ' . BUCKET_PATH . '/history/' . $filename;
        print run_command($command, FALSE);

        // Update cities table with the filename and date.
        update_city_with_filename($status['cityId'], $filename);
    }
    else {
        print "Error - unionTableAll.csv NOT FOUND!\n";
        set_error_status($status);
    }
}

function update_city_with_filename($cityId, $filename)
{
    $conn = get_mysql_connection();
    $sql = "UPDATE cities SET resultsFile = '$filename', lastImportedDate = NOW() where cityId = $cityId;";

    if (!$conn->query($sql)) {
        printf("Error: %s\n", $conn->error);
    }

    $conn->close();
}

function get_number_of_lines($file)
{
    $file_obj = file($file);
    return count($file_obj);
}

function get_file_columns($file, $prefix)
{
    $results = array();
    $handle = fopen($file, 'r');

    if (($columns = fgetcsv($handle)) !== FALSE)
    {
        $results = $columns;
    }

    fclose($handle);

    return $results;
}

function get_default_columns() {
    return array(
        'id',
        'name',
        'address',
        'city',
        'zip',
        'lat',
        'lng',
    );
}

function get_column_names($default = TRUE) {
    $conn = get_mysql_connection();

    if ($default) {
        $columns = get_default_columns();
    } else {
        $columns = array();
    }

    $sql = "SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '" . DATABASE . "' AND TABLE_NAME = 'places_custom_fields';";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc())
        {
            $columns[] = $row['column_name'];
        }
    } else {
        echo "0 results";
    }

    $conn->close();

    return $columns;
}

function import_results($status)
{
    print_timestamp('import_results');
    print("importing results...\n");
    $status['status'] = 'importing_results';
    save_status($status);

    return insert_update_results($status);
}

function get_num_lines($file)
{
    $num = 0;

    while (($values = fgetcsv($file)) !== FALSE) {
        $num++;
    }

    fseek($file, 0);

    return $num;
}

function insert_update_results($status)
{
    $results = array();
    $file = fopen(RESULTS_FILE, 'r');

    if ($file === FALSE)
    {
        printf("ERROR: Results file could not be found.\n");
        return FALSE;
    }

    $num_records = get_num_lines($file) - 1;
    $conn = get_mysql_connection();
    $db_columns = get_column_names(FALSE);
    $fields_mapping = array();

    printf('Creating/updating ' . $num_records . " places...\n");

    $record_index = 0;
    while (($values = fgetcsv($file)) !== FALSE)
    {
        $record_index++;
        if (empty($fields_mapping)) {
            $fields_mapping = get_fields_mapping($db_columns, $values);
        }
        else
        {
            $id_index = $fields_mapping['imported_id_index'];
            $placeId = $values[$id_index];

            /**
             * If the imported id is not NA, there's an actual record to be
             * either inserted or updated.
             */
            if ($placeId !== 'NA')
            {
                // Inserting into places table.
                $sql_values = array(
                    'cityId' => $status['cityId'],
                    'status' => '1',
                    'lastUpdated' => '@NOW@',
                );

                foreach ($fields_mapping['places_columns'] as $index => $db_column_name) {
                    $sql_values[$db_column_name] = $values[$index];
                }

                $sql = build_insert_update_sql('places', $placeId, $sql_values);

                if (!$conn->query($sql)) {
                    printf("Error: %s\n", $conn->error);
                }

                // Inserting into places_custom table.
                $sql_values = array();
                foreach ($fields_mapping['custom_columns'] as $index => $db_column_name) {
                    $sql_values[$db_column_name] = $values[$index];
                }

                $sql = build_insert_update_sql('places_custom_fields', $placeId, $sql_values);

                if (!$conn->query($sql)) {
                    printf("Error: %s\n", $conn->error);
                }
            }
        }

        if (($record_index % 100) == 0) {
            printf('Processed ' . $record_index . ' out of ' . $num_records . "\n");
        }
    }

    printf('Processed ' . $record_index . ' out of ' . $num_records . "\n");

    fclose($file);
    $conn->close();

    return TRUE;
}

function build_insert_update_sql($table, $placeId, $values)
{
    $insert_sql = 'INSERT INTO ' . $table . ' (placeId';
    $values_sql = "VALUES ('" . mysql_escape_string($placeId) . "'";
    $update_sql = '';

    foreach ($values as $db_column_name => $value)
    {
        $insert_sql .= ',' . $db_column_name;

        if ($value == '@NOW@') {
          $values_sql .= ",NOW()";
        } else {
          $values_sql .= ",'" . mysql_escape_string($value) . "'";
        }
        $update_sql .= ",$db_column_name = VALUES($db_column_name)";
    }

    $insert_sql .= ')';
    $values_sql .= ')';
    $update_sql = trim($update_sql, ",");

    return "$insert_sql\n$values_sql\nON DUPLICATE KEY UPDATE\n$update_sql;";
}

function get_fields_mapping($db_columns, $header)
{
    $fields_mapping = array();

    foreach ($db_columns as $column) {
        $index = array_search($column, $header);
        if ($index !== FALSE) {
            $fields_mapping['custom_columns'][$index] = $column;
        }
    }

    $places_mapping = array(
        'name' => 'consolidated_name',
        //'formattedAddress' => 'consolidatedAddress',
        'addressCity' => 'consolidated_city',
        'addressState' => 'consolidated_state',
        'addressZipCode' => 'consolidated_postal_code',
        'addressCountry' => 'consolidated_country',
        'latitude' => 'consolidated_latitude',
        'longitude' => 'consolidated_longitude',
    );

    foreach ($places_mapping as $db_column => $file_column) {
        $index = array_search($file_column, $header);
        if ($index !== FALSE) {
            $fields_mapping['places_columns'][$index] = $db_column;
        }
    }

    $index = array_search('consolidated_id', $header);
    if ($index !== FALSE) {
        $fields_mapping['imported_id_index'] = $index;
    }

    return $fields_mapping;
}

function complete_job($status)
{
    print_timestamp('import_results');
    $status['status'] = 'done';
    save_status($status);
}

function save_status($status, $sync = TRUE)
{
    file_put_contents(FILE_STATUS, json_encode($status));

    if ($sync) {
        $command = GSUTIL . ' cp -a public-read ' . FILE_STATUS . ' ' . BUCKET_PATH . '/status.json';
        print run_command($command, FALSE) . "\n";
    }
}

function read_status()
{
    // Copy status file
    $command = GSUTIL . ' cat ' . BUCKET_PATH . '/status.json';
    $contents = run_command($command);

    if (empty($contents))
    {
        $status = array('status' => 'waiting');
        save_status($status, FALSE);

        return $status;
    }

    return json_decode($contents, TRUE);
}

function get_mysql_connection()
{
    $servername = "173.194.80.237";
    $username = "root";
    $password = "Ab1Pl@c3s";

    // $servername = "127.0.0.1";
    // $username = "root";
    // $password = "";

    $conn = new mysqli($servername, $username, $password, DATABASE);
    //$conn = new mysqli($servername, $username, $password, 'abi_places');

    // Check connection
    if ($conn->connect_error) {
        die("Mysql Connection failed: " . $conn->connect_error . "\n");
    }

    return $conn;
}

function set_error_status($status) {
    print_timestamp('set_error_status');
    print "ERROR - Process terminated with status: " . $status['status'] . "\n";
    $status['status'] = 'error';
    save_status($status);
}

function run_command($command, $return_output = TRUE)
{
    $output_string = '';
    exec($command, $output, $return);

    if ($return_output || $return != 0) {
        foreach ($output as $line)
        {
            $output_string .= $line . "\n";
        }
    }

    return $output_string;
}

function print_timestamp($name) {
    echo 'Profiling(' . $name . '): ' . date('Y-m-d H:i:s') . "\n";
}

main();