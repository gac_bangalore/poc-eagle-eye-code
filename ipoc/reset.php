<?php

define('FILE_STATUS', '/tmp/status.json');
define('BUCKET_PATH', 'gs://abi_places/data-import');

function save_status($status, $sync = TRUE)
{
    file_put_contents(FILE_STATUS, json_encode($status));

    if ($sync) {
        $command = 'gsutil cp -a public-read ' . FILE_STATUS . ' ' . BUCKET_PATH . '/status.json';
        echo $command;
        print run_command($command);
    }
}

function run_command($command)
{
    $return = '';
    exec($command, $output);
    foreach ($output as $line)
    {
        $return .= $line . "\n";
    }

    return $return;
}

$status['status'] = 'uploaded';
$status['cityId'] = 3;
save_status($status);