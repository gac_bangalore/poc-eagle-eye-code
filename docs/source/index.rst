.. POC Eagle Eye documentation master file, created by
   sphinx-quickstart on Fri Mar 18 15:25:36 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to POC Eagle Eye's documentation!
=========================================

This document will detail the Record Linkage Algorithm for the Point of Connection Eagle Eye Project at the Anheuser-Busch InBev Bud Analytics Lab.

.. figure:: /images/ABI_logo.png
   :alt: ABInBev Logo
   :align: Center
   :scale: 100%

.. toctree::
   :maxdepth: 2

   design
   specs
   code
   appendix

