Design and Goals
=============================================

The goal of the Point of Connection (POC) Eagle Eye Project is to give businesses the tools they need to make smarter decisions selling AnheuAnheuser-Busch InBevser products.

One important aspect of the project is to deliver a user-friendly interface that allows a user to run data analysis on a user-defined geographical area using local business datasets. This document particularly focuses on the project's Record Linkage module that runs a record matching algorithm to merge simliar records together. The module outputs cleaner data from a collection of datasets. Due to hardware limitations it was necessary to design a module that could best meet the goal of merging datasets with duplicate records removed. Examination of these challenges is reviewed in Appendix: Alternative Module Designs.


Project Specification
~~~~~~~~~

A brief overview of the POC Eagle Eye Project is given below as a primer to the Record Linkage module specification.

System Design
---------

The POC Eagle Eye runs a  

