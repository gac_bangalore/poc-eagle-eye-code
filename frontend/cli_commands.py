from tools.record_linkage import link_records
from tools import geocoder as geo
from tools import preprocessor as pre
import logging
import platform
import multiprocessing as mp
import argparse
from datetime import datetime
from tools.reports import report


from tools import config
from filter_calculation import filter_cal
from tools.reports import report
from tools import check_file
from tools import config

def log_sysinfo():
    
    logger.info("Python version  : {}" .format(platform.python_version()))
    logger.info("Compiler        : {}" .format(platform.python_compiler()))
    logger.info("System     : {}" .format(platform.system()))
    logger.info("Release    : {}" .format(platform.release()))
    logger.info("Machine    : {}" .format( platform.machine()))
    logger.info("Processor  : {}" .format(platform.processor()))
    logger.info("CPU count  : {}" .format(mp.cpu_count()))
    logger.info("Interpreter: {}" .format(platform.architecture()[0]))
    nodeName = platform.node()
    if nodeName:
        logger.info("Computer Network Name: {}" .format(nodeName))
    else:
        logger.info("Computer Network Name: Could not determine name.")

def parse_commands():
    parser = argparse.ArgumentParser(description='Runs data analysis on given datasets.')
    parser.add_argument('-m', '--match', action='store_true',
                        help='Skip geocoding, and perform preprocessing and record matching.')
    parser.add_argument('-g', '--geocode', action='store_true',
                        help='Run preprocessing and geocoding.')
    parser.add_argument('-s', '--skip_check', action='store_true',
                        help='Skip check and run preprocessing, geocoding and record matching.')

    parser.add_argument('-a', '--analysis', action='store_true',
                        help='Perform analysis on record linage output.')

    parser.add_argument('-ma', '--matching_analysis', action='store_true',
                        help='Run record matching and perform analysis.')

    parser.add_argument('filePaths', nargs='+',
                        help='File paths to datasets to perform data analysis.')
    parser.add_argument('-rl', '--legacy', action='store_true',
                        help='Use legacy records linkage algorithm.')
    parser.add_argument('-bl','--budlink', action='store_true',
                        help='Use Bud Link records linkage algorithm.')
    parser.add_argument('-sr', '--save_report', action='store_true',
                        help='Save report.')

    args = parser.parse_args()
    process_commands(args)
    
def process_commands(cmds):
    """ Identifies the command used in a command-line terminal.
        :param commands: The command/s to process in an array.
    """
    time = datetime.now()

    #enable legacy record linkage from command line
    if cmds.legacy:
        config.DEDUPE = False

    if cmds.budlink:
        config.DEDUPE = True

    if cmds.save_report:
        report.save_report = True

    if cmds.skip_check:
        logger.info("Running preprocessing and Gecoder.")
        geo_filePaths = [preprocess_geo(filePath, False, False) for filePath in cmds.filePaths]
        logger.info("Running record linkage.")
        link_records(geo_filePaths)
    elif cmds.geocode:
        if cmds.match:
            logger.info("Running preprocessing and Gecoder.")
            geo_filePaths = [preprocess_geo(filePath, True, False) for filePath in cmds.filePaths]
            logger.info("Running record linkage.")
            link_records(geo_filePaths)
        else:
            logger.info("Running preprocessing and Gecoder.")
            [preprocess_geo(filePath, True, False) for filePath in cmds.filePaths]
    elif cmds.match:
        if all(check_file.check_all_files(cmds.filePaths)):
            logger.info("Running preprocessing.")
            #UNCOMMENT BELOW STATEMENTS FOR PREPROCESSING ALWAYS ON
            #geo_filePaths = [preprocess_geo(filePath, False, True) for filePath in cmds.filePaths]
            logger.info("Running record linkage.")
            # link_records(geo_filePaths)
            link_records(cmds.filePaths)
        else:
            logger.info("check your input files formats")
            print("check your input files' formats")

    elif cmds.matching_analysis:
        if all(check_file.check_all_files(cmds.filePaths)):
            logger.info("Running record linkage and R analysis.")
            link_records(cmds.filePaths)
            filter_cal.filterCalculation()
        else:
            logger.info("check your input files formats")
            print("check your input files' formats")
    elif cmds.analysis:
        logger.info("Perform analysis on record linage output.")
        filter_cal.filterCalculation(cmds.filePaths)
    else:
        logger.info("Running preprocessing and Gecoder.")
        geo_filePaths = [preprocess_geo(filePath, True, False) for filePath in cmds.filePaths]
        logger.info("Running record linkage.")            
        link_records(geo_filePaths)
    logger.info("Process took {}." .format(datetime.now() - time))

    report.save_reports()
    
def preprocess_geo(filename, checkGeocode=None, skipGeocode=None):
    """ Performs preprocessing and checks if geocoding is required. 
        Performs geocoding if necessary.
        :param filename: File path of input file.
        :type filename: String
        :return: Returns file path of output CSV file.
        :rtype: String
    """
    inPath, outPath, outputFieldNames, encode, excelExtension = pre.preprocessing(filename)
    if skipGeocode:
        return outPath
    else:
        if checkGeocode:
            logger.info("Checking if Geocoding required.")
            if geo.needs_geocoding(inPath, outputFieldNames, encode, excelExtension):
                logger.info("Running Geoencoder.")
                return geo.geocode(inPath, outPath, outputFieldNames, encode, excelExtension)
            else:
                return outPath
    logger.info("Running Geocoder.")
    return geo.geocode(inPath, outPath, outputFieldNames, encode, excelExtension)

logger = logging.getLogger('POC.logger')
