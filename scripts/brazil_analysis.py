import numpy as np
import pandas as pd
import logging
import csv
import os
from tools.core import BudLink

from tools import record_linkage as rl


#Setup working directory and list of files for preprocessing

#work_dir = os.path.join('data','Brazil','Recife')
#filelist = ['google_recife.csv', 'CUSTOMER_LIST_RECIFE_v1.3.csv']

work_dir = os.path.join('data','Brazil','Sao Paulo')
#filelist = ['abi_sao_paulo.csv', 'google_sao_paulo.csv']
#filelist = ['deduplicated_sample_abi_sao_paulo.csv', 'deduplicated_sample_google_sao_paulo.csv']
filelist = ['deduplicated_abi_sao_paulo.csv', 'deduplicated_google_sao_paulo.csv']
filelist = [os.path.join(work_dir, file) for file in filelist]

#init Budlink instance and read google file
brLink = BudLink(filelist)
brLink.read_input_files()

brLink.deduper_settings['load_settings'] = True
brLink.deduper_settings['load_json'] = True
brLink.deduper_settings['active_learning'] = False
brLink.deduper_settings['blocking'] = True
brLink.deduper_settings['enforce_blocking_fields'] = True

brLink.linker_settings['load_settings'] =False
brLink.linker_settings['load_json'] = True
brLink.linker_settings['active_learning'] = False
brLink.linker_settings['blocking'] = True
brLink.linker_settings['enforce_blocking_fields'] = True


#
# if brLink.deduplicate_files:
#      brLink.load_deduper()
#      brLink.deduplicate_all()
# #      # TODO: Add class parameter to save deduplicated files
# #      # TODO: Add functionality to save deduplicated files

if brLink.link_files:
    brLink.load_linker()
    brLink.linker_match_all()
    brLink.save_union_table_csv()

# df_pair = [ brLink.feature_df_list[0], brLink.feature_df_list[1]]
# duplicate_list = rl.find_cross_duplicates(df_pair)
# print('\n')
# print(len(duplicate_list))
# print(len(duplicate_list[0]))

