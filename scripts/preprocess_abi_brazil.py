import numpy as np
import pandas as pd
import logging
import csv
import os
import psutil
from tools.utils import standardize, time_now, update_progress, progress_bar, find_encoding
from tools.reports import report
from tools import config
from tools.core import BudLink


#Setup working directory and list of files for preprocessing

# workdir = 'data/Brazil/Recife'
# filelist = ['places_rt_full_1105_1466696694199.csv', 'CUSTOMER_LIST_RECIFE_v1.3.xlsx']
# filelist = ['places_rt_full_1105_1466696694199.csv']

work_dir = os.path.join('data','Brazil','Sao Paulo')
filelist = ['CUSTOMER_LIST_SP_v1.4.csv', 'places_rt_full_1104_1467212266642.csv']
filelist = [os.path.join(work_dir, file) for file in filelist]

#init Budlink instance and read google file
brLink = BudLink(filelist)
abi_df = brLink.input_to_dataframe(0)

def isfloat(value):
  try:
    float(value)
    return True
  except ValueError:
    return False


def filter_gps(x, bounds):
    """
    bounds: tuple  (min, max)
    """
    min_coord = bounds[0]
    max_coord = bounds[1]

    # print(x, min_coord)
    if(type(x) == str):
        if isfloat(x):
            x = float(x)
        else:
            return None


    result = None
    if x > min_coord and x < max_coord:
        result = x
   # print(x, min_coord)
    return result



#print(google_address_df)
lat_bounds = (-25., -21.)
abi_df['SP_latitude'] = abi_df['SP_latitude'].apply(lambda x: filter_gps(x, lat_bounds))

lon_bounds = (-50., -40.)
abi_df['SP_longitude'] = abi_df['SP_longitude'].apply(lambda x: filter_gps(x, lon_bounds))


preprocessed_df = abi_df

filepath = os.path.join(work_dir, 'abi_sao_paulo.csv')
preprocessed_df.to_csv(filepath, sep=',', na_rep='NA', header=True,
                           index=False, index_label=False, encoding='utf-8',
                           quoting=csv.QUOTE_NONNUMERIC)




#brLink.read_input_files()
