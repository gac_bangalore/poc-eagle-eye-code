import numpy as np
import pandas as pd
import logging
import csv
import os
import psutil
from tools.utils import standardize, time_now, update_progress, progress_bar, find_encoding
from tools.reports import report
from tools import config
from tools.core import BudLink


#Setup working directory and list of files for preprocessing

# workdir = 'data/Brazil/Sao Paulo'
# filelist = ['google_recife.csv', 'CUSTOMER_LIST_RECIFE_v1.3.csv']
#
# filelist = ['places_rt_full_1105_1466696694199.csv', 'CUSTOMER_LIST_RECIFE_v1.3.xlsx']

work_dir = os.path.join('data','Brazil','Sao Paulo')
filelist = ['CUSTOMER_LIST_SP_v1.4.csv', 'places_rt_full_1104_1467212266642.csv']
filelist = [os.path.join(work_dir, file) for file in filelist]

#init Budlink instance and read google file
brLink = BudLink(filelist)
google_df = brLink.input_to_dataframe(1)

print(google_df.columns)

def parse_google_address(google_address_string):
    address_string = google_address_string
    comma_split = address_string.split(',')
    space_split = address_string.split(' ')
    street_split  = address_string.split('-')

    #get street name
    street_name = comma_split[0]
    #print(street_name)

    #find zip code:
    zipcode_available = False
    zipcode = None
    zipcode_idx = -1
    zipcode_string = ''
    for i, split in enumerate(comma_split):
        for c_split in split.split(' '):
            temp_str = c_split
            temp_str = temp_str.replace(' ', '')
            temp_str = temp_str.replace('-', '')
            if temp_str.isdigit() and (len(temp_str) == 8 or len(temp_str) == 5):
                zipcode_available = True
                zipcode = temp_str
                zipcode_idx = i
                zipcode_string = split

    #print(zipcode_string)

    address_string = address_string.replace(zipcode_string, '')

    #get country
    country = 'BR'
    address_string = address_string.replace('Brasil', '')
    address_string = address_string.replace('Brazil', '')

    #get state and city
    #find state and and city split
    state = None
    city = None
    state_city_string = ''
    for i, split in enumerate(comma_split):
        temp_str = split
        dash_split = temp_str.split('-')

        if len(dash_split) == 2:

            temp_city = dash_split[0].replace(' ', '').upper()
            temp_state = dash_split[-1].replace(' ', '')
            temp_state = temp_state.upper()
            if len(temp_state) == 2:
                state = temp_state
                city = temp_city
                state_city_string = split
            elif 'STATE' in temp_state:
                state = 'PE'
                city = temp_city
                state_city_string = split
            elif 'PERNAMBUCO' in temp_state:
                state = 'PE'
                city = temp_city
                state_city_string = split
            #print(temp_state, dash_split)

    address_string = address_string.replace(state_city_string, '')
    address_string = address_string.rstrip(' ')
    address_string = address_string.rstrip(',')
    #print(address_string)

    dash_split = address_string.split('-')

    street_number = None
    street_number = None
    for string in dash_split:
        address_string = string
        address_split = string.split(',')
        if len(address_split) == 2:
            temp_street_name = address_split[0]
            temp_street_number = address_split[1].replace(' ','')

            if len(temp_street_number) < 5:
                street_name = temp_street_name
                street_number = temp_street_number



    # if street_name == None:
    #     print(google_address_string)
    # else:
    #     print(city, state, zipcode, street_name, street_number)

    result_dict = {'google_street_number': street_number,
                   'google_street_name': street_name,
                   'google_city': city,
                   'google_state': state,
                   'google_postal_code': zipcode,
                   'google_country':country}

    return result_dict

google_address_dict = google_df['Address'].apply(parse_google_address).to_dict()

google_address_df = pd.DataFrame.from_dict(google_address_dict, orient='index')

#print(google_address_df)
preprocessed_df = pd.concat([google_df, google_address_df], axis = 1)

cols = ['Place Id', 'Name', 'google_street_number', 'google_street_name', 'google_city', 'google_state', 'google_postal_code', 'google_country',  'Latitude', 'Longitude',
        'Type', 'Phone Number', 'Hours', 'Rating (0-5)', 'Total Ratings', 'Website', 'G+ URL', 'Price Level (0-5)', 'Zagat Selected', 'Zagat Reviews', 'Photos', 'Reviews', 'Last Updated']

preprocessed_df = preprocessed_df[cols]

filepath = os.path.join(work_dir, 'google_sao_paulo.csv')
preprocessed_df.to_csv(filepath, sep=',', na_rep='NA', header=True,
                           index=False, index_label=False, encoding='utf-8',
                           quoting=csv.QUOTE_NONNUMERIC)




#brLink.read_input_files()
