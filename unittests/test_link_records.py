import os
import filecmp
import unittest
import shutil
from unittest import TestCase, TestLoader, TextTestRunner
from tools import geocoder as geo
from tools import preprocessor as pre
from tools import record_linkage as rl
from frontend import cli_commands as ccmd
import pandas as pd
import numpy as np

#set working directory
CURRENT_WORKING_DIRECTORY = os.getcwd()


class TestLinkRecords(TestCase):
    """
    Runs tests for the link_records
    """

    def set_up(self):
        pass

    def tear_down(self):
        pass

    def test_empty_list(self):

        """
        Tests whether link_records fails if an empty fileList is passed
        :return:
        """

        fileList = []
        rl.link_records(fileList)
        assert True, "link_records accpets empty file list"

    def _compare_output(self, inputFiles, outputFile):

        """
        Run link_records and compare expected output with produced output
        :param inputFileList: input files passed to output
        :param outputFile: expected output file, this file is compared with uniontable.csv
        :return:
        """
        print(os.getcwd())
        rl.link_records(fileList=inputFiles)
        linkRecordsOutput = os.path.join(CURRENT_WORKING_DIRECTORY, 'output', 'uniontable.csv')

        # read exxpected table and round geocoordinates up to 6 digits
        dfExpected = pd.read_csv(outputFile)
        dfExpected.iloc[:, 8:10] = dfExpected.iloc[:, 8:10].apply(lambda x: np.round(x, 6))

        #read union table and round geocoordinates up to 6 digits
        dfUniionTable = pd.read_csv(linkRecordsOutput)
        dfUniionTable.iloc[:, 8:10] = dfUniionTable.iloc[:, 8:10].apply(lambda x: np.round(x, 6))

        return dfExpected.equals(dfUniionTable)

    def test_write_uniontable(self):

        fileList = list()
        fileList.append(os.path.join(CURRENT_WORKING_DIRECTORY, 'test_data', 'test1', 'input1.csv'))
        fileList.append(os.path.join(CURRENT_WORKING_DIRECTORY, 'test_data', 'test1', 'input2.csv'))

        rl.link_records(fileList)
        linkRecordsOutput = os.path.join(CURRENT_WORKING_DIRECTORY, 'output', 'uniontable.csv')
        if os.path.isfile(linkRecordsOutput):
            assert True, "link_records writes uniontable.csv"
        else:
            assert False, 'link_records does not write uniontable.csv'

    def test_match(self):

        testDataDir = os.path.join(CURRENT_WORKING_DIRECTORY, 'test_data')

        #create the lists of derictories
        dirList = []
        dirListTestData = []

        for path in os.listdir(testDataDir):
            fullPath = os.path.join(testDataDir,path)
            if os.path.isdir(fullPath):
                dirList.append(fullPath)
                dirListTestData.append(path)

        fileCmpList = []

        #execute test cases
        for fullPath in dirList:
            inputFileList = list()
            inputFileList.append(os.path.join(fullPath, 'input1.csv'))
            inputFileList.append(os.path.join(fullPath, 'input2.csv'))

            expectedOutputFile = os.path.join(fullPath, 'output.csv')

            fileCmpList.append(self._compare_output(inputFileList,expectedOutputFile))
            linkRecordsOutput = os.path.join(CURRENT_WORKING_DIRECTORY, 'output', 'uniontable.csv')
            linkRecordsOutputCopy = os.path.join(fullPath, 'uniontable.csv')
            shutil.copyfile(linkRecordsOutput, linkRecordsOutputCopy)

        #construct an error string
        errorString = '\n'
        for i, item in enumerate(fileCmpList):
            if not item:
                errorString += "test case \'{0}\' failed\n".format(dirListTestData[i])

        if errorString == '\n':
            errorString = 'link_records matching is correct for all test cases'

        self.assertTrue(fileCmpList.count(True) == len(fileCmpList), errorString)

suite = TestLoader().loadTestsFromTestCase(TestLinkRecords)
TextTestRunner(verbosity=2).run(suite)

