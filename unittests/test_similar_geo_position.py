import unittest
import tools.record_linkage as rl


class TestSimilarGeoPosition(unittest.TestCase):


    def setUp(self):
        self.bound = 0.0005

    def tearDown(self):
        pass


    def test_similar_geo_position_calculations(self):
        # testing calculations

        self.assertEqual(rl.similar_geo_position([0,0,0,0,0,1.5,3.2],[0,0,0,0,0,1.5,3.2],self.bound),True)
        # exact
        self.assertEqual(rl.similar_geo_position([0,0,0,0,0,1.5,3.2],[0,0,0,0,0,1.5,-3.2],self.bound),False)
        # exact but negative
        self.assertEqual(rl.similar_geo_position([0,0,0,0,0,1.5,3.2],[0,0,0,0,0,-1.5,-3.2],self.bound),False)
        # exact but negative
        self.assertEqual(rl.similar_geo_position([0,0,0,0,0,1.5,3.2],[0,0,0,0,0,1.5003,3.2003],self.bound),True)
        # negative lat diff and negative long diff
        self.assertEqual(rl.similar_geo_position([0,0,0,0,0,1.5003,3.2],[0,0,0,0,0,1.5,3.2003],self.bound),True)
        # positive lat diff and negative long diff
        self.assertEqual(rl.similar_geo_position([0,0,0,0,0,1.5,3.2003],[0,0,0,0,0,1.5003,3.2],self.bound),True)
        # negative lat diff and positive long diff
        self.assertEqual(rl.similar_geo_position([0,0,0,0,0,1.5003,3.2003],[0,0,0,0,0,1.5,3.2],self.bound),True)
        # positive lat diff and positive long diff
        self.assertEqual(rl.similar_geo_position(["a","b","c","d","e",1.5,3.2],[1,1,1,1,1,1.5004999,3.20050001],self.bound),False)
        # negative lat diff and negative long diff "out of boundary"
        self.assertEqual(rl.similar_geo_position(["a","b","c","d","e",1.50050001,3.2],[1,1,1,1,1,1.5,3.2],self.bound),False)
        # positive lat diff and 0 long diff "out of boundary"






    def test_similar_geo_position_input(self):
        # testing bad input

        with self.assertRaises(IndexError): rl.similar_geo_position([0,0,0,0,1.5,3.2],[0,0,0,0,1.5,3.2], self.bound)
        # "list" input length is less than 7
        self.assertEqual(rl.similar_geo_position([0,0,0,0,0,1,3.2],[0,0,0,0,0,1,3.2], self.bound),False)
        # int input in 5th column


    def test_similar_geo_position_output(self):
        # testing output type

        self.assertIsInstance(rl.similar_geo_position([0,0,0,0,0,1.5,3.2],[0,0,0,0,0,1.5,3.2], self.bound),bool)


""" test record_linkage methods:
1- similar_geo_position
"""


suite = unittest.TestLoader().loadTestsFromTestCase(TestSimilarGeoPosition)
unittest.TextTestRunner(verbosity=2).run(suite)

