import unittest
import tools.record_linkage as rl



class TestIsBigram(unittest.TestCase):
    def setUp(self):
        self.threshold = 0.70

    def tearDown(self):
        pass


    def test_is_bigram_calculations(self):
        # testing calculations
        self.assertEqual(rl.is_bigram("chicago bar","bar chicago",self.threshold),True) # intersection / minimum = 0.8
        self.assertEqual(rl.is_bigram("3361 S Indiana Ave","3362 indiana ave",self.threshold),False) # intersection / minimum = 0.6
        self.assertEqual(rl.is_bigram("---^^^___%%%","-^_%",self.threshold),True) # intersection / minimum = 1
        self.assertEqual(rl.is_bigram("$123/ABC","$123/ABC@",self.threshold),True) # intersection / minimum = 1
        self.assertEqual(rl.is_bigram("This bar is cool!","google!",self.threshold),False) # intersection / minimum ~ 0.1666

    def test_is_bigram_input(self):
        # testing bad input
        self.assertFalse(rl.is_bigram("1234", 1234, self.threshold))
        self.assertFalse(rl.is_bigram(1234, "1234", self.threshold))
        self.assertFalse(rl.is_bigram(1234, 1234, self.threshold))

    def test_is_bigram_output(self):
        # testing output type
        self.assertIsInstance(rl.is_bigram("Foo", "foo", self.threshold), bool)

""" test record_linkage methods:
1- is_bigram
"""

suite = unittest.TestLoader().loadTestsFromTestCase(TestIsBigram)
unittest.TextTestRunner(verbosity=2).run(suite)


