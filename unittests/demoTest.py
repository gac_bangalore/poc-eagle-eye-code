import unittest
from tools.regions import find_region_codes


class demoTest(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test__match_always_pass(self):
        assert True, "I mean for this to pass"

    @unittest.expectedFailure
    def test__match_always_fail(self):
        assert False, "I mean for this to fail"

    def test_region_code(self):
        myRegions = find_region_codes()
        region = myRegions["india"]
        self.assertEqual(region,"in","Works correctly")

    def test_exception(self):
        self.assertRaisesRegex(ValueError, "invalid literal for.*XYZ'$",
                               int, 'XYZ')

    def test_exception2(self):
        with self.assertRaisesRegex(ValueError, 'XLS'):
            int('XYZ')


    def test_addition_both_negative(self):
        actual = -5 + -5
        expected = -10
        self.assertEqual(actual,expected)

suite = unittest.TestLoader().loadTestsFromTestCase(demoTest)
unittest.TextTestRunner(verbosity=2).run(suite)