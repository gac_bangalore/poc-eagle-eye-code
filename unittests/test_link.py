import unittest
import tools.record_linkage as rl
import numpy as np


class TestLink(unittest.TestCase):
    # Test one document
    def setUp(self):
        self.filesNum = 1
        self.maxFileLength = 20
        self.match_grid = np.zeros((self.filesNum, self.maxFileLength), dtype=np.ndarray)
        self.match_grid_full = np.array(
            [[[11], 0, 0, [17], [19], [12], 0, 0, 0, 0, 0, [0], [5], 0, 0, 0, 0, [3], 0, [4]]],
            dtype=object)





    def tearDown(self):
        pass

    def test_link_calculations1(self):
        # Testing empty grid as input and then adding one element at a time (without overlapping)
        rl.link(0,11,self.match_grid,self.filesNum)
        self.expected_grid = np.array([[[11], 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, [0], 0, 0, 0, 0, 0, 0, 0, 0]],
                                      dtype=object)
        self.assertTrue(np.array_equal(self.match_grid,self.expected_grid))

        rl.link(3, 17, self.match_grid, self.filesNum)
        self.expected_grid = np.array([[[11], 0, 0, [17], 0, 0, 0, 0, 0, 0, 0, [0], 0, 0, 0, 0, 0, [3], 0, 0]],
                                      dtype=object)
        self.assertTrue(np.array_equal(self.match_grid, self.expected_grid))


        rl.link(4, 19, self.match_grid, self.filesNum)
        self.expected_grid = np.array([[[11], 0, 0, [17], [19], 0, 0, 0, 0, 0, 0, [0], 0, 0, 0, 0, 0, [3], 0, [4]]],
                                      dtype=object)
        self.assertTrue(np.array_equal(self.match_grid, self.expected_grid))

        rl.link(5, 12, self.match_grid, self.filesNum)
        self.expected_grid = np.array([[[11], 0, 0, [17], [19], [12], 0, 0, 0, 0, 0, [0], [5], 0, 0, 0, 0, [3], 0, [4]]],
                                      dtype=object)
        self.assertTrue(np.array_equal(self.match_grid, self.expected_grid))

    def test_link_calculations2(self):
        # Testing pre-filled grid as input and then adding one element (with overlapping)
        rl.link(5, 14, self.match_grid_full, self.filesNum)
        self.expected_grid = np.array(
            [[[11], 0, 0, [17], [19], [12,14], 0, 0, 0, 0,    0, [0], [5], 0, [5], 0, 0, [3], 0, [4]]],
            dtype=object)
        self.assertTrue(np.array_equal(self.match_grid_full, self.expected_grid))

        rl.link(12, 14, self.match_grid_full, self.filesNum)
        self.expected_grid = np.array(
            [[[11], 0, 0, [17], [19], [12, 14], 0, 0, 0, 0, 0, [0], [5,14], 0, [5,12], 0, 0, [3], 0, [4]]],
            dtype=object)
        self.assertTrue(np.array_equal(self.match_grid_full, self.expected_grid))





suite = unittest.TestLoader().loadTestsFromTestCase(TestLink)
unittest.TextTestRunner(verbosity=2).run(suite)
