import unittest
import os
from tools import record_linkage as rl

CURRENT_WORKING_DIRECTORY = os.getcwd()

class TestRecordLinkage(unittest.TestCase):

    def setUp(self):
        pass
    
    def tearDown(self):
        #os.remove(DataFrame_to_CSV_test_ouptut.csv)
        pass
    
    def test_find_cross_duplicates_simple_duplicates(self):        
        pool = rl.multiple_parallelize()
        df = rl.csv_to_dataframe(CURRENT_WORKING_DIRECTORY 
            + '/unittests/test_data/test_linkage_input.csv')
        df.drop_duplicates(subset=None, inplace=True) 
        duplicates = rl.find_cross_duplicates([df], pool)
        pool.close()
        self.assertFalse(duplicates)
        
    
    def test_find_cross_duplicates_no_matches(self):
        pool = rl.multiple_parallelize()
        df = rl.csv_to_dataframe(CURRENT_WORKING_DIRECTORY 
            + '/unittests/test_data/test_linkage_input_no_matches.csv')
        df.drop_duplicates(subset=None, inplace=True) 
        duplicates = rl.find_cross_duplicates([df], pool)
        pool.close()
        self.assertFalse(duplicates)
        
    def test_find_cross_duplicates_simple_similar_matches(self):
        pool = rl.multiple_parallelize()
        df = rl.csv_to_dataframe(CURRENT_WORKING_DIRECTORY 
            + '/unittests/test_data/test_linkage_input_similar_short.csv')
        df.drop_duplicates(subset=None, inplace=True) 
        duplicates = rl.find_cross_duplicates([df], pool)
        solution = [{0:8}, {0:9}]
        pool.close()
        self.assertCountEqual(duplicates, solution)
        
    def test_output_single_simple_similar_matches(self):
        rl.link_records([CURRENT_WORKING_DIRECTORY 
            + '/unittests/test_data/test_linkage_input_similar_short.csv'])
            
    def test_extra_real_data(self):
        rl.link_records([CURRENT_WORKING_DIRECTORY 
            + ''])
            

    """
    def test_single_simple_doc_match(self):
        rl.link_records([CURRENT_WORKING_DIRECTORY 
            + '/unittests/test_data/test_linkage_input.csv'])
    
    def test_single_no_matches(self):
        rl.link_records([CURRENT_WORKING_DIRECTORY 
            + '/unittests/test_data/test_linkage_input_no_matches.csv'])
    
    def test_multiple__simple_doc_match(self):
        rl.link_records([CURRENT_WORKING_DIRECTORY 
            + '/unittests/test_data/test_linkage_input.csv', 
        CURRENT_WORKING_DIRECTORY 
            + '/unittests/test_data/test_linkage_input.csv'])
    """
    
    
suite = unittest.TestLoader().loadTestsFromTestCase(TestRecordLinkage)
unittest.TextTestRunner(verbosity=100).run(suite)