import unittest
from tools.clean_data import clean_special_characters
from tools.clean_data import standardize_abbreviations




class TestCleanData(unittest.TestCase):


    def test_clean_special_characters(self):

        self.assertEqual(clean_special_characters("_@:gz#"), "gz")
        # testing output value
        self.assertIsInstance(clean_special_characters("Foo"),str)
        # testing output type
        with self.assertRaises(AttributeError): clean_special_characters(1234)
        # testing bad input

    def test_standardize_abbreviations(self):

        self.assertEqual(standardize_abbreviations("x fifth w ave"),"x 5th west avenue")
        # testing output value
        self.assertIsInstance(standardize_abbreviations("Foo"),str)
        # testing output type
        with self.assertRaises(TypeError): standardize_abbreviations(3361)
        # testing bad input


""" test clean_data's methods:
1- clean_special_characters
2- standardize_abbreviations
"""

suite = unittest.TestLoader().loadTestsFromTestCase(TestCLeanData)
unittest.TextTestRunner(verbosity=2).run(suite)
