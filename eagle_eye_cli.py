#!/usr/bin/env python3

#### Import Configuration ####
from tools import config

#### Import reporting tool ####
from tools.reports import report

#### Import Dependencies ####
from tools import utils
from frontend import cli_commands
import os
import time
import logging
import logging.handlers

if config.PROFILE:
    from pycallgraph import PyCallGraph
    from pycallgraph.output import GraphvizOutput
    from pycallgraph import Config
    from pycallgraph import GlobbingFilter



def main():

    #### Disable dedupe logging ####

    dedupe_logger = logging.getLogger('dedupe')
    dedupe_logger.setLevel(logging.CRITICAL)

    #### Initialize Logger ####
    logger = logging.getLogger('POC.logger')
    handler = logging.handlers.RotatingFileHandler(filename='eagle_eye.log', mode='a', backupCount=2)
    handler.setLevel('DEBUG') 
    fmt = logging.Formatter(fmt='[ %(asctime)s ] [ %(name)s ] - [ %(levelname)s ] - %(message)s')
    handler.setFormatter(fmt)
    logger.addHandler(handler)
    logger.setLevel('DEBUG')
    if os.path.isfile('eagle_eye.log'):
        handler.doRollover()
    logger.info('Log opened.')

    #### Initialize report ####
    #You don't have to do anything to initialize the report



    #### Start Processing ####
    cli_commands.log_sysinfo()

    if config.PROFILE:
        filename_profile = os.path.join(os.getcwd(), 'output', 'profiling.png')
        filename_profile = 'profiling.png'
        graphviz = GraphvizOutput(output_file=filename_profile)

        profile_config = Config(max_depth = 10)
        profile_config.trace_filter = GlobbingFilter(include=[
            '*main*'
            '*frontend*',
            '*tools*'
        ],
        exclude=['*pandas*'])

        with PyCallGraph(output=graphviz, config=profile_config):
            cli_commands.parse_commands()

    else:
        cli_commands.parse_commands()

    #### Save Report ####
    #report.saveReport()
    #report.print_report_pdf()

if __name__ == '__main__':
    main()

#Added comment george