import numpy as np
import pandas as pd
import os
import csv

#get working directory
WORKING_DIRECTORY = os.getcwd()
outDir = os.getcwd()

#script parameters
locationList = ['Germany']
nSamples = 200
encoding = 'utf-16'


def create_test_folder(WORKING_DIRECTORY, test_dir = 'test'):
    """
    If path does no exist, then create one

    :param WORKING_DIRECTORY: working directory
    :param test_dir: folder for the test
    :return: returb full path to the test directory
    """
    path = os.path.join(WORKING_DIRECTORY, test_dir)
    if not os.path.exists(path):
        os.mkdir(path)
    return path

#create data frame for test1

def get_street_name(string):
    stringSplit = string.rsplit(sep=' ', maxsplit = 1)
    return stringSplit[0]
    
def get_street_number(string):
    stringSplit = string.rsplit(sep=' ', maxsplit = 1)
    if(len(stringSplit) == 1):
        return 'NA'
    return stringSplit[1]

#match of exctly identical files
dfList = []

#convert old imputs to new inputs and create a test data frame

def read_abi_df(workDir, location, nSamples = 200):

    """
    Read abi data set
    :param workDir: Working directory for the ABI data sets
    :param location: Geographic location
    :param nSamples: number of samples
    :return: Pandas Data Frame
    """

    #assign input file
    inputFile = os.path.join(workDir, location, 'ABI.csv')
    df = pd.read_csv(inputFile)

    #split adress into the number and name
    df['ABI_street_name']  = df['ABI_address'].apply(get_street_name)
    df['ABI_street_number'] = df['ABI_address'].apply(get_street_number)

    #select variables that will be returned by function
    useVariables = ['ABI_ID', 'ABI_name', 'ABI_street_number', 'ABI_street_name',
                    'ABI_city', 'ABI_state', 'ABI_zipcode', 'ABI_country', 
                    'ABI_latitude', 'ABI_longitude', 'ABI_mainstream_vol']

    #selects relevat arguments from the data frame and create a subset of first elements
    if (nSamples >= len(df.index)):
        nSamples = len(df.index)

    dfTest = df[useVariables]
    dfTest = dfTest.iloc[0:nSamples]

    #create a copy of the test data set, this data set is used mor mutation
    dfTest2 = dfTest.copy()
    useVariables2 = [string +'_2' for string in useVariables]
    colNamesDict = dict(zip(useVariables, useVariables2))
    dfTest2.rename(columns=colNamesDict, inplace=True)

    return dfTest, dfTest2

#########################################################################
# Create Input for test 1
#########################################################################

dfTest1, dfTest2 = read_abi_df(WORKING_DIRECTORY, 'Germany', nSamples)

#create subsample of the dataframe
testDirectory = create_test_folder(WORKING_DIRECTORY, 'test1')
outTestDirectory = create_test_folder(outDir, 'test1')

#write test outputs
dfTest1.to_csv(os.path.join(testDirectory, 'input1.csv'), index=False)
dfTest2.to_csv(os.path.join(testDirectory, 'input2.csv'), index=False)

dfTest1.to_csv(os.path.join(outTestDirectory, 'input1.csv'), index=False)
dfTest2.to_csv(os.path.join(outTestDirectory, 'input2.csv'), index=False)

#create test output
inputReqColumns = dfTest1.columns[0:10]

#define output dataset
dfOutput = dfTest1[inputReqColumns]

newReqColNames = ['consolidated_id', 'consolidated_name', 
                    'consolidated_street_number', 'consolidated_street_name',
                    'consolidated_city', 'consolidated_state', 
                    'consolidated_postal_code', 'consolidated_country',
                    'consolidated_latitude', 'consolidated_longitude']  
colNamesDict = dict(zip(inputReqColumns, newReqColNames))
dfOutput.rename(columns=colNamesDict, inplace=True)
                
peices = [dfOutput, dfTest1.iloc[:,0], dfTest1[dfTest1.columns[10:]], dfTest2.iloc[:,0], dfTest2[dfTest2.columns[10:]]]
dfOutput = pd.concat(peices, axis=1)

dfOutput['consolidated_id'] = dfTest1.iloc[:,0] + ' ' + dfTest2.iloc[:,0]

dfOutput.to_csv(os.path.join(testDirectory, 'output.csv'), 
                              sep=',', na_rep='NA', header=True, 
                              index=False, index_label=False, encoding='utf-8',
                              quoting=csv.QUOTE_NONNUMERIC)

dfOutput.to_csv(os.path.join(outTestDirectory, 'output.csv'), 
                              sep=',', na_rep='NA', header=True, 
                              index=False, index_label=False, encoding='utf-8',
                              quoting=csv.QUOTE_NONNUMERIC)
                              
dfOutput.to_excel(os.path.join(testDirectory, 'output.xlsx'))
                              
#########################################################################
# Create Input for test 2
#########################################################################

# #Replace ZIP codes with fake zip codes
dfTest1, dfTest2 = read_abi_df(WORKING_DIRECTORY, 'Germany', nSamples)
dfTest2['ABI_zipcode_2'] = 'NA'
testDirectory = create_test_folder(WORKING_DIRECTORY, 'test2')
outTestDirectory = create_test_folder(outDir, 'test2')

#write test outputs
dfTest1.to_csv(os.path.join(testDirectory, 'input1.csv'), index=False)
dfTest2.to_csv(os.path.join(testDirectory, 'input2.csv'), index=False)

dfTest1.to_csv(os.path.join(outTestDirectory, 'input1.csv'), index=False)
dfTest2.to_csv(os.path.join(outTestDirectory, 'input2.csv'), index=False)


inputReqColumns = dfTest1.columns[0:10]

#define output dataset
dfOutput = dfTest1[inputReqColumns]

newReqColNames = ['consolidated_id', 'consolidated_name', 
                    'consolidated_street_number', 'consolidated_street_name',
                    'consolidated_city', 'consolidated_state', 
                    'consolidated_postal_code', 'consolidated_country',
                    'consolidated_latitude', 'consolidated_longitude']  
colNamesDict = dict(zip(inputReqColumns, newReqColNames))
dfOutput.rename(columns=colNamesDict, inplace=True)
                
peices = [dfOutput, dfTest1.iloc[:,0], dfTest1[dfTest1.columns[10:]], dfTest2.iloc[:,0], dfTest2[dfTest2.columns[10:]]]
dfOutput = pd.concat(peices, axis=1)
dfOutput['consolidated_id'] = dfTest1.iloc[:,0] + ' ' + dfTest2.iloc[:,0]

dfOutput.to_csv(os.path.join(testDirectory, 'output.csv'), 
                              sep=',', na_rep='NA', header=True, 
                              index=False, index_label=False, encoding='utf-8',
                              quoting=csv.QUOTE_NONNUMERIC)
                              
dfOutput.to_csv(os.path.join(outTestDirectory, 'output.csv'), 
                              sep=',', na_rep='NA', header=True, 
                              index=False, index_label=False, encoding='utf-8',
                              quoting=csv.QUOTE_NONNUMERIC)
                              
result = dfTest2.apply(pd.value_counts).fillna(0); result
