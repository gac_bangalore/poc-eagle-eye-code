import pandas as pd
import re
import os.path

#TODO remove from the repo


def read_either(file_type,filepath):
    if file_type == ".xlsx":
        book = pd.ExcelFile(filepath)
        df = book.parse()
        return df

    elif file_type == ".csv":
        df = pd.read_csv(filepath, sep=',')
        return df



def check_file(filepath):
    """

    :param filepath:
    :return: basic inout checking
    """

    return True

    file_type = os.path.splitext(filepath)[1]

    df = read_either(file_type, filepath)

    names = df.columns.values.tolist()
    lat = names[8]
    long = names[9]
    lat_pattern = re.search('_[lL][aA][tT]', lat)
    long_pattern = re.search('[_][lL]', long)

    if lat_pattern is not None:
        if long_pattern is not None:
            return True
        else:
            return False
    else:
        return False


def check_all_files(filepath):
    """

    :param filepath:
    :return:
    """
    results = []

    for file in filepath:
        result = check_file(file)
        results.append(result)

    return results

# if all(check_all_files(["/Users/George/PycharmProjects/poc-eagle-eye-python/test_data/chi_sample_Google.csv",
#                            "/Users/George/PycharmProjects/poc-eagle-eye-python/test_data/chi_sample_Google_copy.xlsx"])):
#     print("YA")


