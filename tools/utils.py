import sys
import csv
from datetime import datetime
from unidecode import unidecode
import time
import mmap
from socket import timeout
import xlrd
import ratelim
import codecs
import logging
from tools.clean_data import clean_special_characters, standardize_abbreviations
    
@ratelim.greedy(50, 1)
def geocode_google(client, inputAddress, language):
    return client.geocode(inputAddress, language=language)

def store_results(result, geoData, googleResultFields, resultKeyConversions, outputFieldNames):
    for key in googleResultFields:
        for i in range(len(geoData[0]['address_components'])):
            if key in geoData[0]['address_components'][i]['types']:
                result[resultKeyConversions[key]] = geoData[0]['address_components'][i]['long_name']
    result[outputFieldNames[8]] = geoData[0]['geometry']['location']['lat']
    result[outputFieldNames[9]] = geoData[0]['geometry']['location']['lng']

def fill_missing_results(result, inputLine, resultFields, origFields):
    result[resultFields[0]] = inputLine[origFields[0]].title()     
    result[resultFields[1]] = standardize_name(inputLine[origFields[1]]).title()      
    
    for index, key in enumerate(resultFields[2:10]):
        if key not in result:
            #insert original data into result; key[4:] drops "geo_" from key
            try:                    
                result[key] =  inputLine[origFields[index+2]].title()
                result[resultFields[10]].append(key)
            except:
                result[key] = ''
    
def check_input(inputDict, requiredColumns):
    for column in requiredColumns:
        try:
            inputDict[column] = inputDict[column].strip()
        except AttributeError:
            inputDict[column] = str(inputDict[column])

def append_headers(path, fieldnames, encode):
    with open(path, 'r', encoding=encode) as f:
        reader = csv.reader(f)
        headers = next(reader)
        for columnName in headers[10:]:
            fieldnames.append(columnName.strip())
        #fieldnames.extend(headers)

def append_headers_xls(path, fieldnames, encode, sheet_index=0):
    stream  = open(path, 'r', encoding=encode)
    data    = mmap.mmap(stream.fileno(), 0, access=mmap.ACCESS_READ)
    book    = xlrd.open_workbook(file_contents=data)
    sheet   = book.sheet_by_index(sheet_index)
    headers = []
    for i in range(sheet.ncols):
        headers.append(sheet.cell_value(0,i))
    fieldnames.extend(headers)

def xls_dict_reader(stream, sheet_index=0):
    '''https://gist.github.com/mdellavo/639082'''
    data    = mmap.mmap(stream.fileno(), 0, access=mmap.ACCESS_READ)
    book    = xlrd.open_workbook(file_contents=data)
    sheet   = book.sheet_by_index(sheet_index)
    def item(i, j):
        return (sheet.cell_value(0,j), sheet.cell_value(i,j))

    return ( dict(item(i,j) for j in range(sheet.ncols)) \
                 for i in range(1, sheet.nrows) )

def progress_bar(amtDone):
    '''
    Displays a progress bar in the console. amtDone must be between 0 and 1
    '''
    sys.stdout.write("\rProgress: [{0:50s}] {1:.1f}%".format('#' * int(amtDone * 50), amtDone * 100))

def update_progress(progress):
    #Did not refactor this as it is not important
    '''
    Displays a progress bar in the console. amtDone must be between 0 and 100
    '''
    print('\r[{0}] {1}%'.format('#'*(int(progress/10)), progress), end="", flush=True)
    
def standardize(text):
    try:
        text = unidecode(text)
    except AttributeError:
        logger.expcetion("Could not standardize text using unidecode.")
    return clean_special_characters(text).upper().strip()
        
def standardize_name(text):
    try:
        text = unidecode(text)
    except AttributeError:
        logger.expcetion("Could not standardize Name using unidecode.")
    return clean_special_characters(text).upper().strip()

def standardize_addr(text):
    try:
        text = unidecode(text)
    except AttributeError:
        logger.expcetion("Could not standardize Address using unidecode.")
    text = clean_special_characters(text).title().strip()
    return standardize_abbreviations(text)
        
        
def find_encoding(filepath):
    encodings = ['utf-8', 'iso-8859-15', 'iso-8859-1', 'iso-8859-2', 
                 'iso-8859-9', 'chinese', 'windows-1251', 'windows-1252', 
                 'windows-1250', 'windows-1256', 'shiftjis']
    for encode in encodings:
        try:
            file = codecs.open(filepath, 'r', encoding=encode)
            file.readlines()
            file.seek(0)
        except UnicodeDecodeError:
            logger.exception('Could not open file using {}.' .format(encode))
        else:
            logger.info('Opening file using {}.' .format(encode))
            return encode

def time_stamp_path(path):
    return ".".join(path.split(".")[:-1])+"_result_"+str(time_now())+'.csv'

def write_headers(newPath, fieldnames):
    if count_lines(newPath, 'utf8') == 0:
        with open(newPath, 'w') as f:
            f.write('\ufeff') # BOM (optional...Excel needs it to open UTF-8 file properly)
            writer = csv.DictWriter(f, fieldnames=fieldnames, extrasaction='ignore')
            writer.writeheader()
    
def write_batch(rows,fieldnames,newPath):
    with open(newPath, 'ab') as f:
        writer = csv.DictWriter(f, fieldnames=fieldnames, extrasaction='ignore')
        writer.writerows(rows)

def count_lines(path, encode):
    try:
        with open(path, 'r', encoding=encode) as f:
            return sum([1 for line in f])-1
    except IOError:
        return 0
    
def time_now():
    s = datetime.now()
    return s.strftime('%m-%d-%y_%H.%M.%S')

def speed(startTime, records):
    n = datetime.now()
    mins = (n-startTime).total_seconds()/60
    return round(records/mins,2)
    
def onerror_retry(exceptions, callback, timeout=120, timedelta=2):
    ''' will catch errors when they are raised as exceptions and retry '''
    end_time = time.time() + timeout
    while True:
        try:
            #yield callback
            return callback    
            break
        except exceptions:
            print("caught", str(exceptions))
            if time.time() > end_time:
                raise
            elif timedelta > 0:
                time.sleep(timedelta)
                
def perform(fun, *args):
    return fun(*args)

def onerror_retry_no_faults(callback, args, attempts=5, timedelta=30, timedOutCount=0, timedOutCountLimit=2):
    ''' catches errors as first item of a tuple in the callback response. 
    If the error is 200, then returns result of callback. Otherwise it attempts
    to repeat the callback attempts number of times, with timedelta pauses 
    that increase with each attempt by attempt*timedelta
    if a timeout error is raised, the whole function is called timedOutCountLimit times
    '''
    try:
        for a in range(attempts):
            errorCode, response = perform(callback, args)
            if errorCode == 200:
                return (errorCode, response)
                break
            else:
                delta = timedelta + a*timedelta
                print("Caught error {}, server response {}, retrying in {} seconds...".format(errorCode, response, delta))
                if delta > 0:
                    time.sleep(delta)
    except timeout:
        print(str(timeout), "retrying {} more times after timeout...".format(timedOutCountLimit-timedOutCount))
        if timedOutCount < timedOutCountLimit:
            onerror_retry_noFaults(callback=callback, args=args, timedOutCount=timedOutCount+1)
        raise
    else:
        raise Exception("It took more than {} attempts to check VAT, aborting :(".format(attempts))

def create_block_key(block):
    group_key = block[0]
    grouped_df = block[1]
    df_idx_set = block[2]
    group_idx_set = set(grouped_df['source'])
    if group_idx_set == df_idx_set:
        return group_key
    else:
        return None

        
logger = logging.getLogger('POC.logger')    
