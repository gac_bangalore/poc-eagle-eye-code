import numpy as np
import pandas as pd
import multiprocessing as mp
import logging
import csv
import os
import psutil
from tools.utils import standardize, time_now, update_progress, progress_bar, find_encoding
from tools.reports import report
from tools import config
from tools.core import BudLink

import affinegap

CURRENT_WORKING_DIRECTORY = os.getcwd()
DEBUG = config.DEBUG

def link_records(fileList, matchColumns=None):
    """ Removes duplicated records and outputs a file with all combined records.
        Records are matched in hierarchical order.
        :param fileList: List of filepaths to link
        :param matchColumns: List of columns to match. Default: postal code, address, name.
        :type fileList: List
        :type matchColumns: List
        
        Example:
        
        link_records(~/Documents/MyRecords.csv, [postal_code, address, name])
    """

    ##Adding file list to report##
    report.input_files = fileList
        ##
    if config.DEDUPE:
        print ("Using BudLink for deduplication and record linkage")
        print (fileList)
        record_linker = BudLink(fileList)

        record_linker.linker_settings['settings_file']= os.path.join('model', 'brazil_linker_learned_setting')
        record_linker.deduper_settings['settings_file']= os.path.join('model', 'brazil_deduper_learned_setting')

        record_linker.linker_settings['training_file'] = os.path.join('model', 'brazil_linker_training_data')
        record_linker.deduper_settings['training_file'] = os.path.join('model', 'brazil_deduper_training_data')

        record_linker.linker_settings['load_settings'] = True
        record_linker.deduper_settings['load_settings'] = True

        record_linker.linker_settings['load_json'] = False
        record_linker.deduper_settings['load_json'] = False

        record_linker.linker_settings['active_learning'] = False
        record_linker.deduper_settings['active_learning'] = False

        record_linker.linker_settings['blocking'] = False
        record_linker.deduper_settings['blocking'] = False

        record_linker.run()

    else:
        update_progress(0)
        if len(fileList) == 1:
            dirtyDataFrames = [input_to_dataframe(fileList[0])]
            update_progress(5)
            find_remove_self_duplicate(dirtyDataFrames)
            df_union_table = dirtyDataFrames[0]
        else:
            pool = multiple_parallelize()
            dirtyDataFrames = [pool.apply_async(input_to_dataframe,
                                                args=(file, matchColumns,))
                                                for file in fileList]
            dirtyDataFrames = [result.get() for result in dirtyDataFrames]
            update_progress(5)
            pool.close()

            # find self duplicates and then remove them
            # df_deduplicated is a list of data frames
            df_list_deduplicated = find_remove_self_duplicate(dirtyDataFrames)
            #df_list_deduplicated = dirtyDataFrames

            #find_
            df_union_table = df_list_deduplicated[0]
            for i, df in enumerate(df_list_deduplicated[1:]):

                # df_pair is a list to be compared
                # df_pair[0] should be always a uniontable
                # df_pair[1] is joined with uniontable
                df_pair = [df_union_table, df]
                duplicate_list = find_cross_duplicates(df_pair)

                print("\nFound {0} duplicate pairs between union table and document {1}".format( len(duplicate_list[0]), i+1))
                logger.debug("Found {0} duplicate pairs between union table and document {1}".format( len(duplicate_list[0]), i+1))

                #pick dulicates with highest scores
                top_duplicates = duplicates_select_top(duplicate_list)

                # Record the first file only once
                if not report.first:

                    report.first = True
                    report.count = report.count + 1

                report.cross_matches.loc[report.count] = [ len(top_duplicates[0]) ]
                report.count = report.count + 1

                print("Selected {0} duplicate pairs between union table and document {1}".format( len(top_duplicates[0]), i+1))
                logger.debug("Selected {0} duplicate pairs between union table and document {1}".format( len(top_duplicates[0]), i+1))

                #construct output tables
                df_union_table = construct_union_table(df_pair, top_duplicates)


        if len(df_union_table) != 0:
            df_to_uniontable_csv(df_union_table)
        logger.info("Record linkage complete.")
        report.final_count.loc[1]=len(df_union_table)

        print("\n")

def construct_union_table(df_pair, duplicates_list):

    #TODO fix concat_df
    df_union_table = concat_df(df_pair, duplicates_list)

    return df_union_table


def concat_df(df_pair, duplicates_list):
    # construct row indices for slicing

    # TODO refactor structure of duplicates_list
    # remove scores from duplicaes_lsit

    matches_list = []

    for i, dl in enumerate(duplicates_list[:-1]):
        dl = [int(x) for x in dl]
        idx_list = list(df_pair[i].index[dl])
        matches_list.append(idx_list)
    df_concat_col_list = list()
    # append header
    # construct list of non matched indices for each df
    non_matches_list = list()
    for i, matches in enumerate(matches_list):
        non_matches_set = set(df_pair[i].index) - set(matches_list[i])
        non_matches_list.append(list(non_matches_set))

    # make header df
    df_header_list = list()
    # column 10 limit the identifier columns
    index_start_data = 10
    df_header = df_pair[0].iloc[duplicates_list[0], :index_start_data]
    df_header.reset_index(inplace=True, drop=True)

    empty_str_list = ['' for x in matches_list[0]]
    consolidated_id = pd.Series(empty_str_list)
    for df in df_pair:
        id = df.iloc[:, 0]
        id.reset_index(inplace=True, drop=True)
        consolidated_id += ' ' + id.apply(str)

    df_header.iloc[:, 0] = consolidated_id
    df_header.reset_index(inplace=True, drop=True)

    # rename columns in df_header
    old_column_names = df_header.columns[:index_start_data]
    # TODO: move from config to class?
    rename_dict = {key: val for key, val in zip(old_column_names, config.UNION_TABLE_COLUMNS)}
    df_header.rename(columns=rename_dict, inplace=True)
    df_header_list.append(df_header)

    # construct master slice list
    slice_list = matches_list
    for j, non_matches in enumerate(non_matches_list):

        for i, slice_i in enumerate(slice_list):
            if i != j:
                slice = [-1 for i in range(len(non_matches))]  # form a list of invalid indices, see comment below
            else:
                slice = non_matches
            slice_i += slice

    # append matched entries df
    # pandas slicing with invalid index using pandas.DataFrame.ix(....) returns  np.nan for the rows with invalid index
    # This allows us to construct rows filled with NAs easier
    for i, df in enumerate(df_pair):
        row_slice = slice_list[i]

        # TODO refactor slicing
        index_start_data = 10
        shape = df.shape
        col_slice = [0] + list(range(index_start_data, shape[1]))

        df_slice = df.ix[row_slice, col_slice]
        df_slice.reset_index(inplace=True, drop=True)
        df_concat_col_list.append(df_slice)

    # concatenate columns and append to a list for row concatenation
    df_concat_col = pd.concat(df_concat_col_list, axis=1)

    for i, non_matches in enumerate(non_matches_list):
        df_header = df_pair[i].ix[non_matches, :index_start_data]
        df_header.reset_index(inplace=True, drop=True)

        # rename columns in df_header
        old_column_names = df_header.columns[:index_start_data]
        # TODO: move from config to class?
        rename_dict = {key: val for key, val in zip(old_column_names, config.UNION_TABLE_COLUMNS)}
        df_header.rename(columns=rename_dict, inplace=True)
        df_header_list.append(df_header)

    df_header = pd.concat(df_header_list, axis=0)
    df_header.reset_index(inplace=True, drop=True)

    df_result = pd.concat([df_header, df_concat_col], axis=1)

    return df_result

def construct_matches_df(df_pair, duplicates_list):
    """

    :param df_pair: list of 2 pandas data framce
    :param duplicates_list: list of entries to concat
    :return: concatenated data frame
    """

    #
    #index of the column that limits the identifier features
    index_start_data = 10

    shape_1 = df_pair[0].shape
    shape_2 = df_pair[1].shape

    data_indices_1 = list(range(index_start_data, shape_1[1]))
    data_indices_2 = list(range(index_start_data, shape_2[1]))

    table_indices_1 = [0] + data_indices_1
    table_indices_2 = [0] + data_indices_2

    df_first = df_pair[0].iloc[duplicates_list[0], table_indices_1]
    df_second = df_pair[1].iloc[duplicates_list[1], table_indices_2]


    # reset index obhect, otherwise concat fails due to duplication
    df_first.reset_index(inplace=True, drop=True)
    df_second.reset_index(inplace=True, drop=True)

    # column 10 limit the identifier columns
    df_header = df_pair[0].iloc[duplicates_list[0], :index_start_data]
    df_header.reset_index(inplace=True, drop=True)
    df_header.iloc[:, 0] = df_first.iloc[:, 0].map(str) + ' ' + df_second.iloc[:, 0].map(str)

    # rename columns in df_header
    old_column_names = df_header.columns[:index_start_data]
    rename_dict = {key: val for key, val in zip(old_column_names, config.UNION_TABLE_COLUMNS)}
    df_header.rename(columns=rename_dict, inplace=True)

    df_matches = pd.concat([df_header, df_first, df_second], axis=1)

    return df_matches

def construct_non_matched_df(df_pair, duplicate_list, df_index):
    """

    :param df_pair: list of 2 pandas data framce
    :param duplicates_list: list of entries to concat
    :return: concatenated data frame
    """

    #
    # index of the column that limits the identifier features
    index_start_data = 10
    unique_diplicates = np.unique(duplicate_list[df_index])

    # construct the lists of on matched rows using set opearations
    full_list = range(len(df_pair[df_index]))

    non_matched_rows = list(set(full_list) - set(unique_diplicates))

    shape = df_pair[df_index].shape
    table_indices = [0] + list(range(index_start_data, shape[1]))

    # slice dataframes of non matched records
    df_nonmatched = df_pair[df_index].iloc[non_matched_rows, table_indices]
    df_nonmatched.reset_index(inplace=True, drop=True)

    # construct union table 1
    df_header = df_pair[df_index].iloc[non_matched_rows, :index_start_data]
    df_header.reset_index(inplace=True, drop=True)
    df_header.iloc[:, 0] = df_header.iloc[:, 0].map(str)

    old_column_names = df_header.columns[:index_start_data]
    rename_dict = {key: val for key, val in zip(old_column_names, config.UNION_TABLE_COLUMNS)}
    df_header.rename(columns=rename_dict, inplace=True)

    if (df_index == 0):
        df_index_empty = 1
    else:
        df_index_empty = 0

    shape_empty = df_pair[df_index_empty].shape
    empty_table_indices = [0] + list(range(index_start_data, shape_empty[1]))
    empty_columns_names = df_pair[df_index_empty].columns[empty_table_indices]

    df_empty = pd.DataFrame(columns=empty_columns_names)

    if (df_index == 0):
        concat_list = [df_header, df_nonmatched, df_empty]
    else:
        concat_list = [df_header, df_empty, df_nonmatched]

    df_non_matches = pd.concat(concat_list, axis=1)

    return df_non_matches

def construct_non_matched_df_2(df_pair, duplicate_list, df_index):
    """

    :param df_pair: list of 2 dataframes
    :param duplicate_list: list of matched pairs
    :param df_index:
    :return:
    """

    index_start_data = 10
    unique_diplicates = np.unique(duplicate_list[df_index])

    # construct the lists of on matched rows using set opearations
    full_list = range(len(df_pair[df_index]))

    non_matched_rows = list(set(full_list) - set(unique_diplicates))

    shape = df_pair[df_index].shape
    table_indices = [0] + list(range(index_start_data, shape[1]))

    # slice dataframes of non matched records
    df_nonmatched = df_pair[df_index].iloc[non_matched_rows, table_indices]
    df_nonmatched.reset_index(inplace=True, drop=True)

    # construct union table 1
    df_header = df_pair[df_index].iloc[non_matched_rows, :index_start_data]
    df_header.reset_index(inplace=True, drop=True)
    df_header.iloc[:, 0] = df_header.iloc[:, 0].map(str)

    old_column_names = df_header.columns[:index_start_data]
    rename_dict = {key: val for key, val in zip(old_column_names, config.UNION_TABLE_COLUMNS)}
    df_header.rename(columns=rename_dict, inplace=True)

    df_nonmatched_complete = pd.concat([df_header, df_nonmatched], axis=1)

    return df_nonmatched_complete


#TODO add logging output and report generation
def find_remove_self_duplicate(df_list):
    """
    Iterates through list of the data frames and removes self duplicates in each data frame
    :param df_list: list of hte data frames, operand
    :return:
    """

    for i, df in enumerate(df_list):
        duplicate_list = find_self_duplicates(df)
        #duplicate_list_clean, saving = clean_duplicate_records(duplicate_list, 1, len(df))
        #remove columns as indices from array 1, this way we use sorted list
        top_duplicates = duplicates_select_top(duplicate_list)
        remove_columns = top_duplicates[0]

        print("\n")
        print("{0} Records in doc {1}.".format(len(df), i ))
        print("Removed {0} duplicate records from doc {1}." .format( len( np.unique(remove_columns) ), i))
        logger.debug('Removed {0} duplicate records from doc {1}.' .format( len( np.unique(remove_columns) ), i) )

        report.num_rows.loc[i] = [len(df)]
        report.self_duplicates.loc[i] = len(remove_columns)

        if (len(remove_columns) > 0):
            df.drop(df.index[remove_columns], inplace=True)

    return df_list

#TODO either remove pool from parameter or check if pool exists
def find_self_duplicates(df, pool = None):
    """
    Return list of the duplicates inside a single pandas data frame
    :param df: pandas dataframe
    :param pool: Multiprocessing pool object for doing search in parallel
    :return: List of the duplicates
    """

    # TODO remove GPU support
    GPU = False

    # Finding and removing duplicates from document 1
    if DEBUG:
        filepath = create_duplicates_file()
    else:
        filepath = ''

    df_matching_features = df.iloc[:, config.FEATURE_MATCHING_COLUMNS] #
    df_matching_features.iloc[:,2] = df_matching_features.iloc[:,1].apply(str) + ' ' + df_matching_features.iloc[:,2].apply(str)
    df_matching_features.iloc[:,0] = df_matching_features.iloc[:, 0].apply(str).apply(standardize)
    df_matching_features = df_matching_features.values

    if pool == None:
        cpu_count = mp.cpu_count()
        pool = mp.Pool(processes=cpu_count)
        create_pool = True
    else:
        create_pool = False

    duplicate_records_pool = [pool.apply_async(match_algo,
                                    args=(index, record, df_matching_features, 1, filepath, len(df_matching_features),
                                          config.DUPLICATE_THRESHOLD,))
                                for index, record in enumerate(df_matching_features)]

    duplicate_records = [list(), list(), list()]
    for result in duplicate_records_pool:
        duplicate_record = result.get()
        if duplicate_record:
            duplicate_records[0] = duplicate_records[0] + duplicate_record[0]
            duplicate_records[1] = duplicate_records[1] + duplicate_record[1]
            duplicate_records[2] = duplicate_records[2] + duplicate_record[2]

    if create_pool:
        pool.close()

    return duplicate_records


def find_cross_duplicates(df_list, pool=None):
    """ Removes exact and approximate duplicates/matches across all files.
        :param df_list: List of DataFrame documents.
        :param pool: Multiprocessing pool object for doing search in parallel
        :type df_list: List
        :type pool: Pool Object
        :return: Returns a DataFrame with combined docs without duplicates/matches.
        :rtype: List
    """

    cross_duplicate_records = list()
    # Finding and removing duplicates from document 1
    if DEBUG:
        filepath = create_duplicates_file()
    else:
        filepath = ''

    if(len(df_list) == 2):
    # Finding and removing matches across docs
        logger.info("Running a Matching Algorithm against both docs...")

        if pool == None:
            cpu_count = mp.cpu_count()
            pool = mp.Pool(processes=cpu_count)
            create_pool = True
        else:
            create_pool = False

        column_names = [df.columns.values for df in df_list]

        #get matchin features for data frame 1
        df_matching_features_1 = df_list[0].iloc[:, config.FEATURE_MATCHING_COLUMNS]  #
        df_matching_features_1.iloc[:, 2] =   df_matching_features_1.iloc[:, 1].apply(str) + ' ' \
                                            + df_matching_features_1.iloc[:, 2].apply(str)
        df_matching_features_1 = df_matching_features_1.values

        # get matchin features for data frame 2
        df_matching_features_2 = df_list[1].iloc[:, config.FEATURE_MATCHING_COLUMNS]  #
        df_matching_features_2.iloc[:, 2] = df_matching_features_2.iloc[:, 1].apply(str) + ' ' \
                                            + df_matching_features_2.iloc[:, 2].apply(str)
        df_matching_features_2 = df_matching_features_2.values

        duplicate_records_pool = [pool.apply_async(match_algo,
                                       args=(index, record, df_matching_features_2, 2, filepath, len(df_matching_features_1),
                                             config.LINKING_THRESHOLD))
                        for index, record in enumerate(df_matching_features_1)]
        logger.debug("Current memory usage: " + check_memory_usage())

        duplicate_records = [list(), list(), list()]
        for result in duplicate_records_pool:
            duplicate_record = result.get()
            if duplicate_record:
                duplicate_records[0] = duplicate_records[0] + duplicate_record[0]
                duplicate_records[1] = duplicate_records[1] + duplicate_record[1]
                duplicate_records[2] = duplicate_records[2] + duplicate_record[2]

        if create_pool:
            pool.close()
    else:
        logger.info("Linking algorithm is not executed, len(df_list) = {0}".format(len(df_list)))

    return duplicate_records

#TODO remove legacy function
def find_cross_duplicates_legacy(docList, pool=None):
    """ Removes exact and approximate duplicates/matches across all files.
        :param docList: List of DataFrame documents.
        :param pool: Multiprocessing pool object for doing search in parallel
        :type docList: List
        :type pool: Pool Object
        :return: Returns a DataFrame with combined docs without duplicates/matches.
        :rtype: List
    """

    #TODO remove GPU support
    GPU = False
        
    #Finding and removing duplicates from document 1
    if DEBUG:
        filepath = create_duplicates_file()
    else:
        filepath = ''
    columnNames = [frame.columns.values for frame in docList]
    duplicatesDoc1 = []
    firstDoc = docList[0].iloc[:,[1,2,3,4,6,8,9]]
    docTypes = firstDoc.dtypes
    docList[0] = docList[0].as_matrix(columns=None)
    firstDoc = firstDoc.as_matrix(columns=None)
    logger.info("Running a Matching Algorithm on Doc 1 ...")
    update_progress(10)    
    if not GPU:
        dup_threshold = 0.94
        numberOfCPUs = mp.cpu_count()
        pool = mp.Pool(processes=numberOfCPUs)

        # combine building number and street name into single address field
        #TODO vectorize string addition
        for rowIndex, row in enumerate(firstDoc):
            row[2] = str(row[1]) + ' ' + str(row[2])

        # TODO: add bi-grams to firstDoc before calling matching/deduplication
        # TODO rename dupRecords1 to listOfGets and refactor duplicatesDoc1.append
        dupRecords1 = [pool.apply_async(match_algo,
                                        args=(index, record, firstDoc, 1, filepath,
                                             dup_threshold,)) 
                                             for index, record in enumerate(firstDoc)]
        [duplicatesDoc1.append(result.get()) for result in dupRecords1 if result.get()]




    else:
        duplicatesDoc1 = cudaRL.match_algo(firstDoc, docTypes)
    duplicatesDoc1, saving = clean_duplicate_records(duplicatesDoc1, 1,
                                                     len(docList[0]))
    firstDoc = np.delete(firstDoc, duplicatesDoc1, axis=0)
    docList[0] = np.delete(docList[0], duplicatesDoc1, axis=0)
    count_doc1 = len(duplicatesDoc1)

    report.self_duplicates.loc[report.count] = [count_doc1]
    report.num_rows.loc[report.count] = [len(firstDoc)]

    if len(docList) > 1:    
        update_progress(25)
        #Finding and removing duplicates from document 2
        duplicatesDoc2 = []
        duplicatesCross = []
        secondDoc = docList[1].iloc[:,[1,2,3,4,6,8,9]]
        secondDocTypes = secondDoc.dtypes
        docList[1] = docList[1].as_matrix(columns=None)
        secondDoc = secondDoc.as_matrix(columns=None)    
        logger.info("Running a Matching Algorithm on Doc 2 ...")
        #Iterating through matrix and calculating bigrams (name and address) and standardizing address
        if not GPU:
            # combine building number and street name into single address field
            for rowIndex, row in enumerate(secondDoc):
                row[2] = str(row[1]) + ' ' + str(row[2])
            # TODO: add bi-grams to firstDoc before calling matching/deduplication
            dupRecords2 = [pool.apply_async(match_algo,
                                            args=(index, record, secondDoc, 1, filepath,
                                                 dup_threshold,)) 
                                                 for index, record in enumerate(secondDoc)]                                         
            [duplicatesDoc2.append(result.get()) for result in dupRecords2 if result.get()]    
        else:
            duplicatesDoc2 = cudaRL.match_algo(secondDoc, secondDocTypes)
        duplicatesDoc2, saving = clean_duplicate_records(duplicatesDoc2, 1,
                                                         len(docList[1]))
        secondDoc = np.delete(secondDoc, duplicatesDoc2, axis=0)
        docList[1] = np.delete(docList[1], duplicatesDoc2, axis=0)
        count_doc2 = len(duplicatesDoc2)
        
        update_progress(50)
        #Finding and removing matches across docs
        logger.info("Running a Matching Algorithm against both docs...")
        if not GPU:
            dupRecords = [pool.apply_async(match_algo,
                                           args=(index, record, secondDoc, 2, filepath,)) 
                                                 for index, record in enumerate(firstDoc)]   
            update_progress(75)
            logger.debug("Current memory usage: " + check_memory_usage())
            [duplicatesCross.append(result.get()) for result in dupRecords if result.get()]
            pool.close()
        else:
            duplicatesCross = cudaRL.match_algo(firstDoc, docTypes, secondDoc, secondDocTypes)
        duplicatesCross, saving = clean_duplicate_records(duplicatesCross, 2,
                                                          max(len(docList[0]),
                                                               len(docList[1])))
        docList = remove_cross_matches(docList, duplicatesCross, saving, columnNames)
        count = len(duplicatesCross)
        print("\n")
        print("{} Records in doc 1.".format(len(firstDoc)))
        print("{} Records in doc 2.".format(len(secondDoc)))


        print("Removed {} duplicate records from doc 1." .format(count_doc1))
        print("Removed {} duplicate records from doc 2." .format(count_doc2))
        print("Matched {} records between doc 1 and doc 2." .format(count))
        logger.debug('Removed {} duplicate records from doc 1.' .format(count_doc1))
        logger.debug('Removed {} duplicate records from doc 2.' .format(count_doc2))
        logger.debug('Matched {} records between doc 1 and doc 2.' .format(count))

        #Record the first file only once
        if not report.first:
            report.self_duplicates.loc[report.count] = [count_doc1]
            report.num_rows.loc[report.count] = [len(firstDoc)]
            report.first = True
            report.count = report.count + 1

        report.self_duplicates.loc[report.count] = [count_doc2]
        report.num_rows.loc[report.count] = [len(secondDoc)]
        report.cross_matches.loc[report.count] = [count]
        report.count = report.count + 1



    else:
        if not GPU:
            pool.close()
        docList = pd.DataFrame(data=docList[0], columns=columnNames)
        print("\n")
        print("Removed {} duplicate records from doc 1." .format(count_doc1))
        logger.debug('Removed {} duplicate records from doc 1.' .format(count_doc1))
    return docList

#def

def duplicates_select_top(duplicates):

    """
    Take a [list() list() list()] structure and selects top duplicate pair for each entry
    :param duplicates:
    :return:
    """

    #get indices from the
    indices_1 = duplicates[0]
    indices_2 = duplicates[1]
    scores = duplicates[2]

    swap = False

    unique_indices_1 = np.unique(indices_1)
    unique_indices_2 = np.unique(indices_2)

    #TO DO refactor
    #if(len(unique_indices_2) < len(unique_indices_1)):
    #    resort = [[v1, v2, v3] for v1, v2, v3 in zip(indices_1, indices_2, scores)]
    #    resort.sort(key=lambda x: x[1])

        #indices_1 = [x[1] for x in resort]
        #indices_2 = [x[0] for x in resort]
        #scores = [x[2] for x in resort]

        #unique_indices_1 = np.unique(indices_1)
        #unique_indices_2 = np.unique(indices_2)

        #swap = True

    top_indeces_1 = unique_indices_1
    top_indeces_2 = np.zeros(len(top_indeces_1))
    top_scores = np.zeros(len(top_indeces_1))


    i = 0
    for j, index_1 in enumerate(unique_indices_1):
        score_max = 0
        index_2 = -1
        while(i < len(indices_1) and index_1 == indices_1[i]):
            if score_max < scores[i]:
                score_max = scores[i]
                index_2 = indices_2[i]
            i += 1
        top_indeces_2[j] = index_2
        top_scores[j] = score_max

    if not swap:
        result = [top_indeces_1, top_indeces_2, top_scores]
    else:
        result = [top_indeces_2, top_indeces_1, top_scores]

    return result




# TODO Remove hard coded value
def match_algo(recordIndex, record, docShared, filesNum, filepath, progress_bar_scale, threshold=0.63):
    """ Runs the matching algorithm on a given record against a List of documents.
        Algoithm heihierarchically checks 'geo_postal_code', 'geo_street_number',
        'geo_street_name', and 'name' and checks for exact matches.
        Algorithm continues to check for approximate matches if no exact matches
        are found.
        :param record: Current record row from a DataFrame document.
        :param recordIndex: Index of current record.
        :param docShared: Document to run matching algorithm
        :param filesNum: Number of files to run matching algorithm (1 or 2)
        :param filepath: Path of duplicates file.
        :type recordIndex: Int
        :type record: List
        :type docShared: List
        :type filesNum: Int
        :type filepath: String
        :return: Returns of List of duplicates found across given record and documents.
        :rtype: List
    """
    #TODO len(docShared) should be replace with len(doc1)
    progress_bar(recordIndex/progress_bar_scale)



    duplicates = [list(), list(), list()]

    # Save full street address is single field
#    record[2] = str(record[1]) +  ' ' + str(record[2])
#    for rowIndex, row in enumerate(docShared):
#        row[2] = str(row[1]) + ' ' + str(row[2])

    for rowIndex, row in enumerate(docShared):
    # TODO What is the use of this if statement?
        if filesNum == 1:
            if rowIndex > recordIndex:
                compare_records(record, row, docShared, filepath, duplicates, recordIndex, rowIndex, threshold)
        else:
            compare_records(record, row, docShared, filepath, duplicates, recordIndex, rowIndex,threshold)
    return duplicates
    
def remove_cross_matches(docList, duplicates, saving, columnNames):
    """ Removes matches from the second document that were found in the first document.
        :param docList: List of documents as DataFrames.
        :param duplicates: Dict with keys as index and values as record index to delete.
        :param saving: Record indices to transfer data from doc 2 to doc 1.
        :param columnNames: List of column names
        :type docList: List
        :type duplicates: List
        :type saving: Dict
        :type columnNames: List
    """
    origIDNameCol = [columnNames[0][0]]
    newReqColNames = ['consolidated_id', 'consolidated_name', 
                        'consolidated_street_number', 'consolidated_street_name',
                        'consolidated_city', 'consolidated_state', 
                        'consolidated_postal_code', 'consolidated_country',
                        'consolidated_latitude', 'consolidated_longitude']  
    columnNames[0][0:10] = newReqColNames
 
    for record1, record2 in saving.items():
        saving[record1] = docList[1][record2]
    
    #Setup final document column layout  
    docData = pd.DataFrame(data=docList[0][:,0:10], columns=columnNames[0][0:10])
    docData_part2 = pd.DataFrame(data=docList[0][:,0], columns=origIDNameCol)
    docData_part3 = pd.DataFrame(data=docList[0][:,10:], columns=columnNames[0][10:])
    docData_part4 = pd.DataFrame(columns=[columnNames[1][0]])
    docData_part5 = pd.DataFrame(columns=columnNames[1][10:])
    docData = pd.concat([docData, docData_part2, docData_part3, 
                         docData_part4, docData_part5], axis=1)

    #Removing cross matches from document 2
    docList[1] = np.delete(docList[1], duplicates, axis=0)    

    #Setup initial doocument column layout
    doc2Data = pd.DataFrame(data=docList[1][:,0:10], columns=columnNames[0][0:10])
    doc2Data_part2 = pd.DataFrame(columns=origIDNameCol)
    doc2Data_part3 = pd.DataFrame(columns=columnNames[0][10:])
    doc2Data_part4 = pd.DataFrame(data=docList[1][:,0], columns=[columnNames[1][0]])
    doc2Data_part5 = pd.DataFrame(data=docList[1][:,10:], columns=columnNames[1][10:])
    doc2Data = pd.concat([doc2Data, doc2Data_part2, doc2Data_part3, 
                          doc2Data_part4, doc2Data_part5], axis=1)

    #Merge both documents together
    if not doc2Data.empty:
        docData = pd.concat([docData, doc2Data], axis=0)
        
    #Insert matched record data from document 2 to document 1
    numOfAddCols_1 = 9 + 1 + len(columnNames[0][10:]) + 1 + 1
    for index, record in saving.items():
        #Refactored due to following issue with pandas
        #A value is trying to be set on a copy of a slice from a DataFrame
        #See the caveats in the documentation: http://pandas.pydata.org/pandas-docs/stable/indexing.html#indexing-view-versus-copy
        #docData.iloc[index][numOfAddCols_1:] = record[10:]
        #/usr/local/python-3.4.4/lib/python3.4/site-packages/pandas/core/series.py:749: SettingWithCopyWarning:
        #A value is trying to be set on a copy of a slice from a DataFrame

        docData.iloc[index,numOfAddCols_1:] = record[10:]
        docData.iloc[index,0] = str(docData.iloc[index][0]) + ' ' + str(record[0])
        docData.iloc[index,numOfAddCols_1-1] = str(record[0])
    return docData      
    
def clean_duplicate_records(duplicateRecords, filesNum, maxFileLength):
    """ Removes double entries from a List indicating duplicate records discovered.
        Uses a 2D-numpy array grid to link all duplicates discovered to remove 
        duplicate entries in duplicates List and multiple records matched together.
        :param duplicateRecords: Records labeled as duplicates by matching algo
        :param filesNum: Number of files to run matching algo
        :param maxFileLength: Maximum number of entries across all file.
        :type duplicateRecords: List
        :type filesNum: Int
        :type maxFileLength: Int
        :return: List of Lists of records to duplicate records
        :rtype: List
    """

    #Construct a 2D grid from numpy array
    match_grid = np.zeros((filesNum, maxFileLength), dtype=np.ndarray)
    deleteRows = []
    saving = {}
    #Setup a 'symbolic' link between paired matches translated as coordinates in the grid.
    for process in duplicateRecords:
        for processPairs in process:
            link(processPairs[0], processPairs[1],match_grid,filesNum)
    #Retrieve coordinates of only nonzero entries and zero out visited cells.
    x, y = np.nonzero(match_grid)
    for num in range(len(x)):
        i = x[num]
        j = y[num]        
        if match_grid[i][j] != 0:
            for matches in match_grid[i][j]:
                deleteRows.append(matches)
                if filesNum == 1:
                    match_grid[0][matches] = 0
                else:
                    #Save the data from rows we expect to delete.
                    saving.update({j:matches})
                    match_grid[1][matches] = 0
                matches = 0
    return deleteRows, saving

def input_to_dataframe(filename, matchColumns=None):
    """ Converts an input file to a pandas dataframe.
        :param filename: The name of the file to convert.
        :param matchColumns: The custom columns to use in matching.
        :type filename: String
        :type matchColumns: List
        :return: Returns a Pandas dataframe with integer index.
        :rtype: dataframe
    """

    if filename.split('.')[-1] in ['xls', 'xlsx']:
        logger.info("Importing Excel file...")
        if not matchColumns:
            df = pd.read_excel(filename, header=0, index_col=False)
        else:
            df = pd.read_excel(filename, header=0, usecols=matchColumns,
                             index_col=False)

    else:
        logger.info("Importing CSV file...")
        encode = find_encoding(filename)
        if not matchColumns:
            df = pd.read_csv(filename, header=0, index_col=False,
                             sep=None, encoding=encode)
        else:
            df = pd.read_csv(filename, header=0, usecols=matchColumns,
                             index_col=False, sep=None, encoding=encode)

    logger.debug("Current memory usage: " + check_memory_usage())
    return df  

def df_to_uniontable_csv(completeData):
    """
        Writes CSV file 'uniontable.csv' from the given dataframe
        :param completeData: All data in a single DataFrame
        :type completeData: List
    """
    folder_output = os.path.join(CURRENT_WORKING_DIRECTORY, 'output')
    if not os.path.exists(folder_output):
        os.makedirs(folder_output)     
    filepath = os.path.join(folder_output, 'uniontable.csv') #'/record_linkage_output_' + str(time_now())+'.csv'
    logger.info("Exporting union table to CSV file...")
    completeData.to_csv(filepath, sep=',', na_rep='NA', header=True, 
                        index=False, index_label=False, encoding='utf-8',
                        quoting=csv.QUOTE_NONNUMERIC)   
    
def write_to_duplicates(filepath, duplicatePair):   
    """Writes duplicate/matches rows to a file."""     
    with open(filepath, mode='a', newline='', encoding='utf-8') as dupPairs:
        dupPairs.write("".join((str(duplicatePair[0]) + str(duplicatePair[1]))))
        dupPairs.write("\n")
        
def create_duplicates_file():
    """Creates a file to hold duplicate/matching data."""
    folder_output = CURRENT_WORKING_DIRECTORY + '/output'
    if not os.path.exists(folder_output):
        os.makedirs(folder_output)     
    count = 0
    filepath = folder_output + '/duplicates_discovered_' + str(count) + '.csv'
    while os.path.isfile(filepath):    
        count += 1
        filepath = folder_output + '/duplicates_discovered_' + str(count) + '.csv'
    return filepath

def multiple_parallelize():
    """ Create a process pool object to submit jobs to pool of worker processes.
        :return: Returns a process pool with the Computer's current CPU count.
        :rtype: Pool Obejct
    """
    numberOfCPUs = mp.cpu_count()
    logger.debug("MP reporting " + str(numberOfCPUs))
    logger.debug("OS reporting " + str(os.cpu_count()))
    return mp.Pool(processes=numberOfCPUs)
    
def check_memory_usage():
    """ Chekcs the current processes' memory usage.
        :return: Memory Info
        :rtype: String
    """
    process = psutil.Process(os.getpid())
    return str(process.memory_info().rss)

def entry_score(recordA, recordB, column):
    """

    :param recordA:
    :param recordB:
    :param column: integer
    :return: match distance
    """

    if (recordA[column] == recordB[column]):
        return 1.
    else:
        return bigram_distance(recordA[column], recordB[column])

def bigram_distance(stringA, stringB):
    """

    :param stringA: string
    :param stringB: string
    :return: string distance
    """
    stringA = {stringA[i:i + 2] for i in range(len(stringA) - 1)}
    stringB = {stringB[i:i + 2] for i in range(len(stringB) - 1)}
    intersection = len(stringA & stringB)
    minimum = min(len(stringA), len(stringB))

    if intersection >= 0 and minimum == 0:
        result = 1.
    else:
        result = 1. *intersection / minimum
    return result

def match_score(recordA, recordB, threshold = 0.7):

    #first weight is for 0.2
    weight = [0.5, 0.5]

    address_score = entry_score(recordA, recordB, 2)
    name_score = entry_score(recordA, recordB, 0)

    #if(address_score > threshold):

    #    address_score = 1
    #else:
    #    address_score = 0

    score = weight[0] *address_score + weight[1] *name_score
    return score

def compare_records(record, row, docShared, filepath, duplicates, recordIndex, rowIndex,threshold):
    #TODO Convert to one big if statement with and. Or write a new function to match all address parameters

    score = 0
    #Branch 1 - Address Exact Match
    if postal_code_exists(record, row):
        if match_postal_code(record, row) or similar_postal_code(record, row):
            if match_address(record, row) or similar_address(record, row, threshold):
                score = entry_score(record, row, 0)

                if (score > threshold):
                    doc_index_1 = duplicates[0]
                    doc_index_2 = duplicates[1]
                    score_list = duplicates[2]

                    doc_index_1.append(recordIndex)
                    doc_index_2.append(rowIndex)
                    score_list.append(score)

    elif similar_city(record, row):
        if match_address(record, row) or similar_address(record,row,threshold):
            score = entry_score(record, row, 0)

            if (score > threshold):
                doc_index_1 = duplicates[0]
                doc_index_2 = duplicates[1]
                score_list = duplicates[2]

                doc_index_1.append(recordIndex)
                doc_index_2.append(rowIndex)
                score_list.append(score)

    elif similar_geo_position(record, row, 0.0005): # TODO Remove hardcoded value
        if match_address(record, row) or similar_address(record, row, threshold):
            score = entry_score(record, row, 0)

            if (score > threshold):
                doc_index_1 = duplicates[0]
                doc_index_2 = duplicates[1]
                score_list = duplicates[2]

                doc_index_1.append(recordIndex)
                doc_index_2.append(rowIndex)
                score_list.append(score)





############Extracted Functions############
#From match_algo()
def postal_code_exists(recordA, recordB):
    if str(recordA[4]).lower() not in ['na', 'nan', ''] and str(recordB[4]).lower() not in ['na', 'nan', '']:
        return True
    return False

def match_postal_code(recordA, recordB):
    return recordA[4] == recordB[4]

def match_street_number(recordA, recordB):
    return recordA[1] == recordB[1]

def match_street_name(recordA, recordB):
    return recordA[2] == recordB[2]

def match_name(recordA, recordB):
    return recordA[0] == recordB[0]

def similar_city(recordA, recordB):
    return is_bigram(recordA[3], recordB[3])

def is_bigram(stringA, stringB, threshold=0.70):
    """ Check if two strings are similiar by using bigrams.
        :param threshold: The margin from which two strings is/isn't a bigram.
    """
    try:
        stringA = {stringA[i:i + 2] for i in range(len(stringA) - 1)}
        stringB = {stringB[i:i + 2] for i in range(len(stringB) - 1)}
        intersection = len(stringA & stringB)
        minimum = min(len(stringA), len(stringB))
        if intersection >= 0 and minimum == 0:
            return True
        if intersection / minimum > threshold:
            return True
    except:
        return False
    return False


def similar_postal_code(recordA, recordB):
    """ Check if two postal codes are similiar by comparing first three digits of each code.
    """
    codeA = str(recordA[4])[:3].lower()
    codeB = str(recordB[4])[:3].lower()
    if codeA not in ['nan', 'na'] and codeB not in ['nan', 'na']:
        return str(recordA[4])[:3] == str(recordB[4])[:3]
    else:
        return False

#TODO
def match_address(addressA, addressB):
    return addressA[2] == addressB[2]


def similar_address(addressA, addressB, threshold=0.70):
    return is_bigram(addressA[2], addressB[2], threshold)


def similar_name(recordA, recordB, threshold=0.70):
    #TODO Collapse into one function? Have alias?
    #TODO Standardize all inputs in the beginning
    #TODO Move to preprocessing
    nameA = standardize(str(recordA[0]))
    nameB = standardize(str(recordB[0]))
    return is_bigram(nameA, nameB, threshold)


def similar_geo_position(record, row, bound=0.0005):
    if type(record[5]) is float and type(row[5]) is float and type(record[6]) is float and type(row[6]) is float:
        latDiff = float(record[5]) - float(row[5])
        lonDiff = float(record[6]) - float(row[6])
        if ((latDiff > -1.0 * bound and latDiff < bound) and
                (lonDiff > -1.0 * bound and lonDiff < bound)):
            return True
    return False

    # Alternative
    # distMeters = math.sqrt((distLat*distLat) - (distLon*distLon)) * 1.113195e5
    # if distMeters < bound:
    #    return True
    # return False


def match_found(recordIndex, record, docShared, filepath, duplicates, rowIndex):
    # Appends
    # List is mutable type in python. Hence after an append it doesn't have to be returned.
    doc_index_1 = duplicates[0]
    doc_index_2 = duplicates[1]
    doc_index_1.append(recordIndex)
    doc_index_2.append(rowIndex)
    #duplicates.append([recordIndex, rowIndex])
    if DEBUG:
        write_to_duplicates(filepath, [record, docShared[rowIndex]])
        # print(recordIndex, rowIndex, filesNum)


def compare_records_tree(record, row, docShared, filepath, duplicates, recordIndex, rowIndex,threshold):
    #TODO Convert to one big if statement with and. Or write a new function to match all address parameters
    #Branch 1 - Address Exact Match
    if postal_code_exists(record, row):
        if match_postal_code(record, row) or similar_postal_code(record, row):
            if match_address(record, row) or similar_address(record,row,threshold):
                if match_name(record, row) or similar_name(record,row,threshold):
                    match_found(recordIndex, record, docShared, filepath, duplicates, rowIndex)
                    # print('****p1****')

    elif similar_city(record, row):
        if match_address(record, row) or similar_address(record,row,threshold):
            if match_name(record, row) or similar_name(record, row, threshold):
                match_found(recordIndex, record, docShared, filepath, duplicates, rowIndex)

    elif similar_geo_position(record, row, 0.0005): # TODO Remove hardcoded value
        if match_address(record, row) or similar_address(record, row, threshold):
            if match_name(record, row) or similar_name(record, row, threshold):
                match_found(recordIndex, record, docShared, filepath, duplicates, rowIndex)
        if match_name(record, row) or similar_name(record, row, threshold):
            match_found(recordIndex, record, docShared, filepath, duplicates, rowIndex)

#From clean_duplicate_records
def link(recordA, recordB, match_grid,filesNum):
    if filesNum == 1:
        if match_grid[0][recordA] == 0:
            match_grid[0][recordA] = [recordB]
        else:
            if recordB not in match_grid[0][recordA]:
                match_grid[0][recordA].append(recordB)
        if match_grid[0][recordB] == 0:
            match_grid[0][recordB] = [recordA]
        else:
            if recordA not in match_grid[0][recordB]:
                match_grid[0][recordB].append(recordA)
    else:
        if match_grid[0][recordA] == 0:
            match_grid[0][recordA] = [recordB]
        else:
            if recordB not in match_grid[0][recordA]:
                match_grid[0][recordA].append(recordB)
        if match_grid[1][recordB] == 0:
            match_grid[1][recordB] = [recordA]
        else:
            if recordA not in match_grid[1][recordB]:
                match_grid[1][recordB].append(recordA)


logger = logging.getLogger('POC.logger')

print(__name__)
if __name__ == '__main__':
    print('This is not how to run this program')
