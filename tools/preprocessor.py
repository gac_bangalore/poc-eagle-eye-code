import os
import csv
import logging
from tools import utils

CURRENT_WORKING_DIRECTORY = os.getcwd()
        
def preprocessing(filename):
    """ Performs preprocessing tasks in preparation of geocoding.
        :param filename: File path of input file.
        :type filename: String
        :return: Returns the input and output file path and required column 
                 names for the output file. And excel string.
        :rtype: String, String, List, String
    """

    inputFile = filename
    outputFolder = os.path.join(CURRENT_WORKING_DIRECTORY,'output')
    if not os.path.exists(outputFolder):
        os.makedirs(outputFolder)

    fileNameSuffix = utils.time_stamp_path(inputFile.split('/')[-1])
    tempPath = os.path.join(outputFolder,'PreProcessed_'+fileNameSuffix)
    outPath  = os.path.join(outputFolder,'Geocoded_' + fileNameSuffix)


    
    excelExtension = inputFile.strip().split('.')[-1] in ['xls', 'xlsx', 'xlsb'] #Boolean value.
    
    #Get prefix from id column to append in column names in output file
    encode = utils.find_encoding(inputFile)
    with open(filename, 'r', encoding=encode) as file:
        header = file.readline()
        try:
            dialect = csv.Sniffer().sniff(file.read(2048))
            delimiter = dialect.delimiter
        except:
            logger.exception("Could not determine file delimiter, defaulting to ','.")
            delimiter = ','
        columns = header.split(delimiter)
        idColumn = columns[0].split('_')
        if len(idColumn) == 1 or not idColumn:
            prefix = 'source'
        else:
            prefix = idColumn[0]

    # TODO output  field names are hardcoded
    outputFieldNames = ['geo_id', 'geo_name', 
                        'geo_street_number', 'geo_street_name',
                        'geo_city', 'geo_state', 
                        'geo_postal_code', 'geo_country',
                        'geo_latitude', 'geo_longitude', 
                        'geo_missing_data']
                        
    #Append prefix to columns names for output file
    outputFieldNames = [prefix + '_' + columnName for columnName in outputFieldNames]
    if not excelExtension:
        utils.append_headers(inputFile, outputFieldNames, encode)
    else:
        utils.append_headers_xls(inputFile, outputFieldNames, encode)
    print(utils.time_now(), ':', 
          str(utils.count_lines(inputFile, encode)), 'addresses will be processed')
    print('Writing results to',outPath)
    utils.write_headers(tempPath, outputFieldNames)
    utils.write_headers(outPath,outputFieldNames)
    
    with open(filename, 'r', encoding=encode) as inputStream, open(tempPath, 'a', encoding='utf8') as outputStream:
        if excelExtension:
            inputReader = utils.xls_dict_reader(inputStream)
        else:
            try:
                dialect = csv.Sniffer().sniff(inputStream.read(2048))
                inputStream.seek(0)
                inputReader = csv.DictReader(inputStream, dialect=dialect)
            except:
                logger.exception("Could not determine file dialect.")
                inputStream.seek(0)
                inputReader = csv.DictReader(inputStream)
        outputWriter = csv.DictWriter(outputStream, fieldnames=outputFieldNames, 
                                      extrasaction='ignore')
                
        columnNames = inputReader.fieldnames
        requiredColumns = columnNames[:10]
        
        for line in inputReader:
            utils.check_input(line, requiredColumns)
            result = {}
        
            for column in range(len(requiredColumns)):
                if column == 1:
                    result[outputFieldNames[column]] = utils.standardize_name(line[requiredColumns[column]])
                elif column == 3:
                    result[outputFieldNames[column]] = utils.standardize_addr(line[requiredColumns[column]])
                else:
                    result[outputFieldNames[column]] = utils.standardize(line[requiredColumns[column]]).title()

            result[outputFieldNames[0]] = line[requiredColumns[0]].title()     
            result.update(line)
            outputWriter.writerow(result)
    inputFile = tempPath
    return inputFile, outPath, outputFieldNames, encode, excelExtension
    
logger = logging.getLogger('POC.logger')