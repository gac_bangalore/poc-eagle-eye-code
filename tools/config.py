########
# List of matching columns used for reacord linkage
DEDUPE = False
#Threshold for removing matching inside a single document

DUPLICATE_THRESHOLD = 0.94
LINKING_THRESHOLD = 0.63

DEBUG = False
PROFILE = False

FEATURE_MATCHING_COLUMNS = [1, 2, 3, 4, 5, 6, 8, 9] #Being used in find_cross_duplicates

FEATURE_MATCHING_COLUMNS_STRINGS = ['id',
                            'name',
                            'street_number',
                            'street_name',
                            'city',
                            'state',
                            'postal_code',
                            'country',
                            'latitude',
                            'longitude']

FEATURE_MATCHING_COLUMNS_TYPE = ['String',
                                 'String',
                                 'String',
                                 'String',
                                 'String',
                                 'String',
                                 'String',
                                 'String',
                                 'LatLong',
                                 'LatLong']

HAS_MISSING = [False,
               False,
               True,
               True,
               True,
               True,
               True,
               True,
               True,
               True]

UNION_TABLE_COLUMNS = ['consolidated_id',
                       'consolidated_name',
                       'consolidated_street_number',
                       'consolidated_street_name',
                       'consolidated_city',
                       'consolidated_state',
                       'consolidated_postal_code',
                       'consolidated_country',
                       'consolidated_latitude',
                       'consolidated_longitude']

####Settings for report generation####

#File format for report
#List of possible values: ['xlsx','xls','csv']
REPORT_EXTENSION = 'xlsx'

#Report output filename
REPORT_FILE_NAME = 'MetadataReport'
REPORT_OUTPUT_FOLDER = "output" #Always remains the same