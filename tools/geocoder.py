import csv
import time
from datetime import datetime
import googlemaps
import logging
from tools import utils
from tools.regions import find_region_codes
import multiprocessing as mp

#TODO Abstract CLIENT and CLIENT_SECRET to settings.py
CLIENT = 'gme-anheuserbuschinbev'
CLIENT_SECRET = '4YES-hETKP0qYiIcUh9kOX4B_t8='

def geocode(input_path, output_path, output_field_names, encode, excel_extension=None):
    """ Outputs a CSV file with geocoded data provided by the Google Maps API.
        :param input_path: The file path of the input file.
        :param output_path: The file path of the output CSV file.
        :param output_field_names: A list of the names of the required columns for the output file.
        :param excel_extension:
        :type input_path: String
        :type output_path: String
        :type output_field_names: List
        :type excel_extension: String
        :return: Returns the file path of the output CSV file.
        :rtype: String
    
    """
    start_time = datetime.now()
    
    
    with open(input_path, 'r', encoding=encode) as input_stream, open(output_path, 'a', encoding='utf8') as output_stream:
        if excel_extension:
            input_reader = utils.xls_dict_reader(input_stream)
        else:
            try:
                dialect = csv.Sniffer().sniff(input_stream.read(2048))
                input_stream.seek(0)
                input_reader = csv.DictReader(input_stream, dialect=dialect)
            except:
                logger.exception("Could not determine file dialect.")
                input_stream.seek(0)
                input_reader = csv.DictReader(input_stream)
        output_writer = csv.DictWriter(output_stream, fieldnames=output_field_names,
                                      extrasaction='ignore')
        google_result_fields = ['street_number', 'route', 'locality',
                              'administrative_area_level_1', 'postal_code', 
                              'country']
        result_key_conversions = {'street_number':output_field_names[2],
                                'route':output_field_names[3],
                                'locality':output_field_names[4],
                                'administrative_area_level_1':output_field_names[5],
                                'postal_code':output_field_names[6],
                                'country':output_field_names[7]}
        region_codes = find_region_codes()
        column_names = input_reader.fieldnames
        required_columns = column_names[:10]
        #requestLimitPerProcess = 50 / mp.cpu_count()
        start = datetime.now()
        #Run in parallel        
        results = []
        total_processed = 0
        pool = mp.Pool(processes= mp.cpu_count())


        def geocode_process_results(output):
            # No test for this trivial function - AAA
            results.append(output)

        for line in input_reader:
            pool.apply_async(parallel_geo,
                             args=(line, required_columns, output_field_names,
                                   google_result_fields, result_key_conversions,
                                   region_codes, column_names,),
                             callback=geocode_process_results)
            total_processed += 1
        pool.close()
        pool.join()
        [output_writer.writerow(result) for result in results]
        
        print(datetime.now() - start)
    print(utils.time_now(), "Final", 
          str(total_processed), "records processed at a rate of",
          str(utils.speed(start_time, total_processed)), "lines per minute")
    print(str(total_processed),'geocoded addresses written to', output_path, "in",
          str(datetime.now()-start_time).split(".")[0])
    return output_path

def RateLimited(maxPerSecond):
    #Currently not being used. No tests to be written - AAA
    """
    Limits the number of times a function can be executed in 1 second.
    :param maxPerSecond: Maximum times a function is to be executed in 1 second.
    :return:
    """
    minInterval = 1.0 / float(maxPerSecond)
    def decorate(func):
        lastTimeCalled = [0.0]
        def rateLimitedFunc(*args,**kargs):
            elapsed = time.clock() - lastTimeCalled[0]
            leftToWait = minInterval - elapsed
            if leftToWait>0:
                time.sleep(leftToWait)
            ret = func(*args,**kargs)
            lastTimeCalled[0] = time.clock()
            return ret
        return rateLimitedFunc
    return decorate

#@RateLimited(2)     
def parallel_geo(line, required_columns, output_field_names,
                 google_result_fields, result_key_conversions, region_codes,
                 column_names):
    client = googlemaps.Client(client_id=CLIENT, client_secret=CLIENT_SECRET)
    utils.check_input(line, required_columns)
    
    #Determine if will perform geocoding or reverse geocoding
    #TODO Take a look at required columns
    if reverse_geo(line, required_columns):
        geodata = client.reverse_geocode((line[required_columns[8]], line[required_columns[9]]))
    else:
        input_address = line[required_columns[2]] + ' ' + \
                       line[required_columns[3]] + ', ' + \
                       line[required_columns[4]] + ', ' + \
                       line[required_columns[5]] + ', ' + \
                       line[required_columns[6]] + ', ' + \
                       line[required_columns[7]]
        r_code = None
        if line[required_columns[7]]:
            if line[required_columns[7]].lower() in region_codes:
                r_code = region_codes[line[required_columns[7]].lower()]
        geodata = client.geocode(utils.standardize_addr(input_address), region=r_code)
    result = {}
    
    #Parse through returned Google data and update our results for output
    if len(geodata):
        utils.store_results(result, geodata, google_result_fields,
                            result_key_conversions, output_field_names)

    #TODO output_field_name[10] is hardcoded
    result[output_field_names[10]] = []
    utils.fill_missing_results(result, line, output_field_names, column_names)
    result.update(line)            
    
    #outputWriter.writerow(dict((k, v.encode('utf8') if isinstance(v, str) else v) for k, v in result.items()))
    return result
    
def reverse_geo(line, required_columns):
    """ Determine whether to perform reverse geocode if street number and street 
        name are not provided and lat and lon are provided.
        :param line: A single row from the input file.
        :param required_columns: A list of the names of the required columns from the input file.
        :type line: Dict
        :type required_columns: List
        :return: Returns True or False depending on if street address data  is available.
        :rtype: Boolean
    """
    st_num = line[required_columns[2]]
    st_name = line[required_columns[3]]
    lat = line[required_columns[8]]
    lon = line[required_columns[9]]
    if ((not st_num and not st_name) or
        (st_num == 'Nan' and st_name == 'Nan') or 
        (st_num == 'NA' and st_name == 'NA') or 
        (st_num == 'nan' and st_name == 'nan')):
        if (lat and lon and lat != 'NaN' and lon != 'NaN' and lat != 'NA' and lon != 'NA'):
            return True
    return False

def needs_geocoding(in_path, output_field_names, encoding, excel_extension=None):
    """ Determines if a file needs geocoding by geocoding the first ten rows
        and comparing with the original rows. If at least 8 of 10 are the same, 
        then no geocoding is required.
        :param in_path: The file path of the input file.
        :param output_field_names: A list of the names of the required columns for the output file.
        :param encoding:
        :param excel_extension:
        :type in_path: String
        :type output_field_names: List
        :type encoding: String #hopefully
        :type excel_extension: String
        :return: Returns True or False if a file needs geocoding.
        :rtype: Boolean
    """
    result_count = 0
    with open(in_path, 'r', encoding=encoding) as input_stream:
        if excel_extension:
            input_reader = utils.xls_dict_reader(input_stream)
        else:
            try:
                dialect = csv.Sniffer().sniff(input_stream.read(2048))
                input_stream.seek(0)
                input_reader = csv.DictReader(input_stream, dialect=dialect)
            except:
                logger.exception("Could not determine file dialect.")
                input_stream.seek(0)
                input_reader = csv.DictReader(input_stream)
        google_result_fields = ['street_number', 'route', 'locality',
                              'administrative_area_level_1', 'postal_code', 
                              'country']
        result_key_conversions = {'street_number':output_field_names[2],
                                'route':output_field_names[3],
                                'locality':output_field_names[4],
                                'administrative_area_level_1':output_field_names[5],
                                'postal_code':output_field_names[6],
                                'country':output_field_names[7]}
        region_codes = find_region_codes()
        column_names = input_reader.fieldnames
        required_columns = column_names[:10]
        
        for line in input_reader:
            client = googlemaps.Client(client_id=CLIENT, client_secret=CLIENT_SECRET)
            utils.check_input(line, required_columns)
            
            #Determine if will perform geocoding or reverse geocoding
            if reverse_geo(line, required_columns):
                geo_data = client.reverse_geocode((line[required_columns[8]], line[required_columns[9]]))
            else:
                input_address = line[required_columns[2]] + ' ' + \
                                line[required_columns[3]] + ', ' + \
                                line[required_columns[4]] + ', ' + \
                                line[required_columns[5]] + ', ' + \
                                line[required_columns[6]] + ', ' + \
                                line[required_columns[7]]
                r_code = None
                if line[required_columns[7]]:
                    if line[required_columns[7]].lower() in region_codes:
                        r_code = region_codes[line[required_columns[7]].lower()]
                geo_data = client.geocode(utils.standardize_addr(input_address), region=r_code)
            result = {}
            
            #Parse through returned Google data and update our results for output
            if len(geo_data):
                utils.store_results(result, geo_data, google_result_fields,
                                    result_key_conversions, output_field_names)
            result[output_field_names[10]] = []
            utils.fill_missing_results(result, line, output_field_names, column_names)
            result.update(line)                 
            if result == line:
                result_count += 1
        if result_count >= 8:
            return False
    return True

logger = logging.getLogger('POC.logger')
