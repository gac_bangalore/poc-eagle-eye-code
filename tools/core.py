import numpy as np
import pandas as pd
import multiprocessing as mp
import logging
import csv
from unidecode import unidecode
import re
import os
import psutil
from tools.utils import standardize, time_now, update_progress, progress_bar, find_encoding
from tools.reports import report
import dedupe
from tools.reports import report
from tools import config
from tools import utils
import copy
import json
import time
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC


from collections import defaultdict

pd.options.mode.chained_assignment = None
import warnings
warnings.filterwarnings("ignore")

class BudLink:

    MATCHING_FIELDS = [
        {'field': 'name', 'variable name': 'name',  'type': 'String'},
        {'field': 'street_name', 'variable name': 'street_name', 'type': 'String','has missing':True},
        {'field': 'street_number', 'variable name': 'street_number', 'type': 'ShortString', 'has missing':True},
        #{'field': 'state', 'type': 'String', 'has missing': True},
        #{'field': 'country', 'type': 'ShortString', 'has missing': True},
        {'field': 'city', 'type': 'ShortString', 'has missing': True},
        {'field': 'postal_code', 'variable name': 'postal_code', 'type': 'String', 'has missing': True},
        {'field': 'location', 'variable name': 'location', 'type': 'LatLong', 'has missing': True},
        #{'type': 'Interaction', 'interaction variables': ['name', 'location']},
        #{'type': 'Interaction', 'interaction variables': ['street_name', 'street_number']},
        #{'type': 'Interaction', 'interaction variables': ['name', 'street_name']},
        #{'type': 'Interaction', 'interaction variables': ['name', 'postal_code']}
    ]

    DEDUPING_FIELDS = [
        {'field': 'name', 'variable name': 'name', 'type': 'String'},
        {'field': 'street_name', 'variable name': 'street_name', 'type': 'String','has missing':True},
        {'field': 'street_number', 'variable name': 'street_number', 'type': 'ShortString', 'has missing':True},
        {'field': 'state', 'type': 'ShortString', 'has missing': True},
        {'field': 'country', 'type': 'ShortString', 'has missing': True},
        {'field': 'city', 'type': 'Exact', 'has missing': True},
        {'field': 'postal_code', 'variable name': 'postal_code', 'type': 'String', 'has missing': True}
        # {'type': 'Interaction', 'interaction variables': ['name', 'street_name']},
        # {'type': 'Interaction', 'interaction variables': ['name', 'postal_code']}
        #{'field': 'location', 'type': 'LatLong', 'has missing': True}
    ]

    FEATURE_MATCHING_COLUMNS = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

    FEATURE_MATCHING_COLUMNS_STRINGS = ['id',
                                        'name',
                                        'street_number',
                                        'street_name',
                                        'city',
                                        'state',
                                        'postal_code',
                                        'country',
                                        'latitude',
                                        'longitude']

    #dedupe_blocker_index_fields and linker_blocker_index_fields are used to block records
    # We can enforce blocker fields for deduper and linker, usually performs better than dedupe suggested fields
    dedupe_blocker_index_fields = ['name', 'street_name', 'postal_code']
    linker_blocker_index_fields = ['postal_code', 'name', 'street_name', 'street_number']
    #`linker_blocker_index_fields = ['name', 'street_name']

    # linker_training_file = os.path.join('model', 'chicago_linker_training.json')
    # linker_setting_file = os.path.join('model', 'chicago_linker_learned_setting')
    # deduper_training_file = os.path.join('model', 'chicago_deduper_training.json')
    # deduper_setting_file = os.path.join('model', 'chicago_deduper_learned_setting')

    linker_training_file = os.path.join('model', 'brazil_linker_training.json')
    linker_setting_file = os.path.join('model', 'brazil_linker_learned_setting')
    deduper_training_file = os.path.join('model', 'brazil_deduper_training.json')
    deduper_setting_file = os.path.join('model', 'brazil_deduper_learned_setting')

    def __init__(self,file_list,country=0):

        if type(file_list) != list:
            raise Exception("file_list is not of type list")

        if len(file_list) < 1:
            raise Exception("file_list is empty or has not a valid length: {}".format(len(file_list)))

        self.country = country
        self.file_list = file_list

        self.save_report = True
        self.predict_zipcode = True

        self.folder_output = os.path.join(os.getcwd(), 'output')

        if not os.path.exists(self.folder_output):
            os.makedirs(self.folder_output)

        #Settings and switches for the class
        self.deduper_settings = {'load_settings': True,
                                 'settings_file': self.deduper_setting_file,
                                 'save_settings': True,
                                 'load_json': False,
                                 'training_file':self.deduper_training_file,
                                 'active_learning': False,
                                 'loaded':False,
                                 'threshold': 0.5,
                                 'blocking': True,
                                 'enforce_blocking_fields': True,
                                 'rfc_classifier' : True,
                                 'rfc_n_estimators': 100
                                 }

        self.linker_settings = { 'load_settings': True,
                                 'settings_file': self.linker_setting_file,
                                 'save_settings': True,
                                 'load_json': False,
                                 'training_file': self.linker_training_file,
                                 'active_learning': False,
                                 'loaded': False,
                                 'threshold': 0.5,
                                 'blocking': True,
                                 'enforce_blocking_fields': True,
                                 'rfc_classifier': True,
                                 'rfc_n_estimators': 100
                                 }

        self.deduplicate_files = True
        self.link_files = True
        self.linker_threshold = 0.5
        self.deduper_threshold = 0.5
        self.threshold = 0.2
        self.preprocess_zipcode = True
        self.zipcode_length = 3
        self.zipcode_desired_length = 5

        self.df_list = []
        self.dict_list = []
        self.feature_df_list = []

        #Should be the last statement


    def run(self):
        self.read_input_files()
        if self.deduplicate_files:
            self.load_deduper()
            self.deduplicate_all()
            #TODO: Add class parameter to save deduplicated files
            #TODO: Add functionality to save deduplicated files

        if self.link_files:
            self.load_linker()
            self.linker_match_all()
            self.save_union_table_csv()


    def read_input_files(self):
        #construct list of processed data frames and dictionaries
        self.df_list = []
        self.dict_list = []
        self.feature_df_list = []

        starting_index = 0
        for idx, filename in enumerate(self.file_list):
            df = self.input_to_dataframe(idx)
            #use global index
            df.index += starting_index
            processed_df = self.preprocess_dataframe(df)

            #if not self.check_format(processed_df):
            #    raise Exception('Format of the file {0} is wrong'.format(filename))

            #Construct DF list
            self.df_list.append(processed_df)

            starting_index = len(processed_df)

        #create feature df list
        for idx, filename in enumerate(self.file_list):
            #Construct Feature DF List
            self.create_feature_df(idx)

        #create master dict and reset indices in the feature dataframes
        self.create_master_dict()

        for idx, feature_df in enumerate(self.feature_df_list):
            #Construct Dict List
            feature_dict = self.df_to_ddio_dict(feature_df)
            self.dict_list.append(feature_dict)

        if(self.predict_zipcode):
            self.zipcode_model_train()
            self.zipcode_model_predict()




    def load_linker(self):

        if(len(self.df_list) > 1):
            if self.linker_settings['load_settings']:
                self.load_linker_settings()

            if not self.linker_settings['loaded']:
                self.linker = dedupe.RecordLink(self.MATCHING_FIELDS)
                n_estimators = self.linker_settings['rfc_n_estimators']
                self.linker.classifier = RandomForestClassifier(n_estimators=n_estimators,
                                                                max_depth=None,
                                                                min_samples_split=1,
                                                                random_state=0)

                self.linker.sample(self.dict_list[0], self.dict_list[1])
                if self.linker_settings['load_json']:
                    self.linker_load_json()

                if self.linker_settings['active_learning']:
                    self.active_learning_linker()

                self.linker.train()

                if self.linker_settings['save_settings']:
                    with open(self.linker_settings['settings_file'], 'wb') as sf:
                        self.linker.writeSettings(sf)

            #TODO: Add exception handling
            self.linker_settings['loaded'] = True
        else:
            #TODO add sanity checks for number of input files
            print('Linker was not loaded, number of input files={0}'.format(len(self.df_list) > 1))

    def load_deduper(self):
        if self.deduper_settings['load_settings']:
            self.load_deduper_settings()

        if not self.deduper_settings['loaded']:
            self.deduper = dedupe.Dedupe(self.DEDUPING_FIELDS)
            n_estimators = self.deduper_settings['rfc_n_estimators']
            self.deduper.classifier = RandomForestClassifier(n_estimators=n_estimators,
                                                            max_depth=None,
                                                            min_samples_split=1,
                                                            random_state=0)

            #self.deduper.classifier = SVC(probability=True)
            #self.deduper.sample(self.dict_list[1])

            if self.deduper_settings['load_json']:
                self.deduper_load_json()

            for dict in self.dict_list:

                self.deduper.sample(dict, blocked_proportion=0.5)

                if self.deduper_settings['active_learning']:
                    self.active_learning_deduper()

            #train on master dice
            # self.deduper.sample(self.master_dict, blocked_proportion=0.5)
            #
            # if self.deduper_settings['load_json']:
            #     self.deduper_load_json()
            #
            # if self.deduper_settings['active_learning']:
            #     self.active_learning_deduper()

            self.deduper.train()

            if self.deduper_settings['save_settings']:
                with open(self.deduper_settings['settings_file'], 'wb') as sf:
                    self.deduper.writeSettings(sf)

        # TODO: Add exception handling
        self.deduper_settings['loaded'] = True

    def load_linker_settings(self):
        """
        Loads existing deduper settings
        """
        if os.path.exists(self.linker_settings['settings_file']):
            with open(self.linker_settings['settings_file'], 'rb') as lf:
                self.linker = dedupe.StaticRecordLink(lf)
                self.linker_settings['loaded'] = True
        else:
            self.linker_settings['loaded'] = False
            print ('linker settings missing')

    def load_deduper_settings(self):
        """
        Loads existing deduper settings
        :return: Returns boolean and modifies the class
        """
        if os.path.exists(self.deduper_settings['settings_file']):
            with open(self.deduper_settings['settings_file'], 'rb') as df:
                self.deduper = dedupe.StaticDedupe(df)
                self.deduper_settings['loaded'] = True
        else:
            self.deduper_settings['loaded'] = False
            print ('deduper settings missing')

    def deduper_load_json(self):
        if os.path.exists(self.deduper_settings['training_file']):
            print('reading labeled examples from ', self.deduper_settings['training_file'])
            with open(self.deduper_settings['training_file']) as dtf:
                self.deduper.readTraining(dtf)
        else:
            print('deduper labels missing.')

    def linker_load_json(self):
        if os.path.exists(self.linker_settings['training_file']):
            print('reading labeled examples from ', self.linker_settings['training_file'])
            with open(self.linker_settings['training_file']) as ltf:
                self.linker.readTraining(ltf)
        else:
            print('linker labels missing.')

    def active_learning_linker(self):
        print ('Starting active learning for linker')
        dedupe.consoleLabel(self.linker)
        with open(self.linker_settings['training_file'], 'w') as tf:
            dirname = os.path.dirname(self.linker_settings['training_file'])
            if not os.path.exists(dirname):
                os.mkdir(dirname)
            self.linker.writeTraining(tf)

        # Li: Convert json training data to csv files
        self.json_train_data_to_csv(self.linker_settings['training_file'])

    def active_learning_deduper(self):
        print ('Starting active learning for deduper')
        dedupe.consoleLabel(self.deduper)
        with open(self.deduper_settings['training_file'], 'w') as tf:
            dirname = os.path.dirname(self.deduper_settings['training_file'])
            if not os.path.exists(dirname):
                os.mkdir(dirname)
            self.deduper.writeTraining(tf)

        #Li: Convert json training data to csv files
        self.json_train_data_to_csv(self.deduper_settings['training_file'])

    # def save_settings(self):
    #     pass

    def create_master_dict(self):
        #Construct the dictionry for ddio matching
        self.master_df = pd.concat(self.feature_df_list, axis = 0)
        #self.master_df.reset_index(inplace=True, drop=True)
        self.master_dict = self.master_df.to_dict(orient='index')

    def linker_match(self,idx_1,idx_2):
        """
        :param idx_1:
        :param idx_2:
        :return: [(id,obj_1_id,obj_2_id)...]
        """
        return self.linker.match(self.dict_list[idx_1],self.dict_list[idx_2],self.linker_threshold)


    def linker_match_all(self,base_idx=0):
        self.matches = []
        num_files = len(self.file_list)

        if (num_files > 1):
            assert base_idx < num_files, "Base id cannot be larger than num_files"

            #initialize debug union table
            if config.DEBUG:
                self.df_union_table_debug = self.df_list[base_idx].iloc[:, self.FEATURE_MATCHING_COLUMNS]

            filename_base = os.path.basename(self.file_list[base_idx])
            print('\n##############################################################')
            print( 'Record linkage has started\nUsing file #{0} \"{1}\" as base'.format(base_idx, filename_base) )
            for i in range(num_files):
                if i != base_idx:
                    filename = os.path.basename(self.file_list[i])
                    print ('\nStarted record linkage between documents {0} and {1} (files: \"{2}\" and \"{3}\")'.format(base_idx, i, filename_base, filename))

                    self.df_list[base_idx] = self.link_dataframes(base_idx,i)
                    self.df_list[base_idx] = self.preprocess_dataframe(self.df_list[base_idx])

                    self.feature_df_list[base_idx] = self.create_feature_df(base_idx)
                    self.dict_list[base_idx] = self.df_to_ddio_dict(self.feature_df_list[base_idx])
        else:
            self.df_union_table = self.df_list[0]

            if config.DEBUG:
                self.df_union_table_debug = self.df_list[0]

    def input_to_dataframe(self, index):
        """ Converts an input file to a pandas dataframe.
            :param filename: The name of the file to convert.
            :param matchColumns: The custom columns to use in matching.
            :type filename: String
            :type matchColumns: List
            :return: Returns a Pandas dataframe with integer index.
            :rtype: dataframe
        """
        filename = self.file_list[index]

        #Checking if input file is excel
        if filename.split('.')[-1] in ['xls', 'xlsx']:
            logger.info("Importing Excel file...")
            df = pd.read_excel(filename, header=0, index_col=False)
        else:
            logger.info("Importing CSV file...")
            encode = find_encoding(filename)
            df = pd.read_csv(filename, header=0, index_col=False, sep=',',
                             encoding=encode, quoting=csv.QUOTE_ALL, dtype=str)
        return df

    def unidecode_none(self, x):
        if (x != None):
            return unidecode(str(x))
        else:
            return x

    def zipcode_truncate(self,zip):
        # zip = str(zip)
        # length = self.zipcode_length
        # zip[:length]

        zip = str(zip)
        length = self.zipcode_length
        zip = zip[:length]

        try:
            zip = float(zip)
            zip = int(np.round(zip/100))
            result = str(zip)
        except:
            result = None
        return result

    def preprocess_zip_code(self, zip_code):
        if zip_code == None:
            return None
        else:

            zip_string = str(zip_code)

            # print(zip_code)
            len_zip_code = len(zip_string)
            len_left_zeros = len_zip_code - len(zip_string.lstrip('0'))
            len_target = self.zipcode_desired_length
            len_trim = self.zipcode_length
            zero_padding_right ='0' *(len_target - len_trim - len_left_zeros)
            zero_padding_left = '0' *len_left_zeros
            zip_float = float(zip_string)
            zip_round = np.round(zip_float / 10 ** (len_zip_code - len_trim - len_left_zeros))
            zip_int = int(zip_round)
            result = zero_padding_left + str(zip_int) + zero_padding_right
            # print(zip_code, result)
            return result

    def zipcode_model_train(self, idx = -1):
        print("Training zipcode prediction model on location")
        if(idx < 0 or idx >= len(self.df_list)):
            df_train = self.master_df[['latitude', 'longitude', 'postal_code']].dropna()
        else:
            df_train = self.feature_df_list[idx][['latitude', 'longitude', 'postal_code']].dropna()
        x_train = df_train[['latitude', 'longitude']].values
        y_train = df_train[['postal_code']].values
        self.zipcode_rfc = RandomForestClassifier(n_estimators=30, max_depth=None,
                                     min_samples_split=1, random_state=0)
        self.zipcode_rfc.fit(x_train, y_train)

    def zipcode_model_predict(self):
        print("Predciting zipcode based on location")
        for idx, df in enumerate(self.feature_df_list):
            self.feature_df_list[idx] = self.df_predict_zipcode(df, self.df_list[idx])
            self.dict_list[idx] = self.df_to_ddio_dict(self.feature_df_list[idx])

            # create master dict and reset indices in the feature dataframes
        self.create_master_dict()


    def df_predict_zipcode(self, df, df_b = None):
        no_postal_code = df['postal_code'].isnull()
        has_location = ~df['latitude'].isnull()
        idx_slice = df['latitude'][no_postal_code & has_location].index
        if len(idx_slice) > 0:
            x_predict = df.ix[idx_slice, ['latitude', 'longitude']].values
            zipcode_predict = self.zipcode_rfc.predict(x_predict)
            df.ix[idx_slice, 'postal_code'] = zipcode_predict
            if type(df_b) == pd.DataFrame:
                idx_zipcode = self.FEATURE_MATCHING_COLUMNS_STRINGS.index('postal_code')
                df.ix[idx_slice, idx_zipcode] = zipcode_predict
        return df



    def location_tuple(self, x):
        if x[0] == None or x[1] == None:
            return None
        else:
            return tuple( [float(x[0]), float(x[1])] )

    def check_format(self, df):
        """
        Moved from check_file.py

        :param df:
        :return:
        """

        col_names = df.columns
        lat = col_names[8]
        long = col_names[9]
        lat_pattern = re.search('_[lL][aA][tT]', lat)
        long_pattern = re.search('[_][lL]', long)

        if lat_pattern == None:
            return False
        else:
            if long_pattern == None:
                return False
            else:
                return True

    def preprocess_dataframe(self,df):
        #TODO: replace NaN, NA, etc with None
        df = df.where((pd.notnull(df)), None)

        df = df.applymap(self.unidecode_none)

        if self.preprocess_zipcode:
            df.iloc[:,6] = df.iloc[:,6].apply(self.preprocess_zip_code)
            # Zipcode is column 7, hence index 6

        return df

    def write_blocks(self, blocks, filename):
        file = open(filename, 'w')
        for block in blocks:
            file.write('#############################################\nNew block\n#############################################\n')
            for record in block:
                file.write('idx = ' + str(record[0]) + '\n')
                file.write('record = ' + str(record[1]) + '\n')
                file.write('smaller_ids = ' + str(record[2]) + '\n')
        file.close()



    def deduplicate_one(self,idx):
        if self.deduper_settings['loaded']:
            if self.deduper_settings['blocking']:

                result_set = self.dedupe_blocker(idx)
                blocks = list(self.candidates_gen(result_set))

                filename = os.path.basename(self.file_list[idx]) + '_.txt'
                filename = os.path.join(self.folder_output, filename)
                self.write_blocks(blocks, filename)

                threshold = self.deduper.thresholdBlocks(blocks, recall_weight=2)
                clustered_dupes = self.deduper.matchBlocks(blocks, threshold)
            else:
                threshold = self.deduper.threshold(self.dict_list[idx], recall_weight=2.)
                clustered_dupes = self.deduper.match(self.dict_list[idx],threshold)
            filename = os.path.basename(self.file_list[idx])
            print ('Number of duplicate clusters in file \"{0}\" is {1}'.format(filename, len(clustered_dupes)))
            #Deleting values from the dictionary


            #counter for number of duplicate paors
            n_pairs = 0

            #TODO remove entries from dataframe as well
            duplicates_list = list()

            for cluster in clustered_dupes:
                for entry in cluster[0][1:]:
                    del self.dict_list[idx][entry]
                    n_pairs += 1
                    duplicates_list.append(entry)

            #update report data frames
            report.num_rows.loc[idx] = [len(self.df_list[idx])]
            report.self_duplicates.loc[idx] = len(duplicates_list)

            #remove duplicates from dataframe
            if(len(duplicates_list) > 0):
                self.feature_df_list[idx].drop(duplicates_list, inplace=True)
            print('Number of duplicates in file \"{0}\" is {1}'.format(filename, n_pairs))

            #TODO move to a separate function
            # Li : save the duplicated records for validate
            d =  list(clustered_dupes)

            # Change tuple to list
            for i in range(0, len(d)):
                d[i] = list(d[i])
                for j in range(0, len(d[i])):
                    if isinstance(d[i][j], np.ndarray):
                        d[i][j] = list(d[i][j])

            # Create the pandas dataframe for the duplicated records
            df_duplicate_cluster_idx = pd.DataFrame(d,columns=['Id','Confidence_Score'])

            # Convert index to detailed records
            cols = list(self.df_list[idx].columns.values)
            cols.insert(0,'Cluster_ID')
            df_duplicate_cluster = pd.DataFrame(columns = cols)

            cluster_id = 0
            add_index = 0

            for i in df_duplicate_cluster_idx['Id']:
                for j in i:
                    row_to_append = list(self.df_list[idx].ix[j])
                    row_to_append.insert(0,cluster_id)
                    df_duplicate_cluster.loc[add_index] = row_to_append
                    add_index +=1
                cluster_id+=1

            # Save the dataframe to csv file
            filename = os.path.basename(self.file_list[idx])
            filepath = os.path.join(self.folder_output,
                                    'Duplicates_found'+'_'+ filename + '_.csv')  # '/record_linkage_output_' + str(time_now())+'.csv'
            logger.info("Exporting duplicated records found to CSV file...")
            df_duplicate_cluster.to_csv(filepath, sep=',', na_rep='NA', header=True,
                                      index=False, index_label=False, encoding='utf-8')

            #remove duplicates for df_list
            if(len(duplicates_list) > 0):
                self.df_list[idx].drop(duplicates_list, inplace=True)


            #write clean dataframe to csv file
            filename = "deduplicated_" + os.path.basename(self.file_list[idx])
            filepath = os.path.join(self.folder_output, filename)
            self.df_list[idx].to_csv(filepath, sep=',', na_rep='NA', header=True,
                                       index=False, index_label=False, encoding='utf-8',
                                       quoting=csv.QUOTE_NONNUMERIC)

            df_sample = self.df_list[idx].sample(5000)

            filename = "deduplicated_sample_" + os.path.basename(self.file_list[idx])
            filepath = os.path.join(self.folder_output, filename)
            df_sample.to_csv(filepath, sep=',', na_rep='NA', header=True,
                                       index=False, index_label=False, encoding='utf-8',
                                       quoting=csv.QUOTE_NONNUMERIC)

        else:
            raise Exception("Deduper not loaded. Load deduper first")

    def deduplicate_all(self):
        print('\n')
        print('######################################')
        for idx,dict in enumerate(self.dict_list):
            filename = os.path.basename(self.file_list[idx])
            print('\n')
            print ('Deduplicating file #{0}: {1}'.format(idx, filename))
            self.deduplicate_one(idx)

            #save deduplicated dataframe

            #

        #Doesn't return anything. Modifies the existing dictionaries.


    def df_to_feature_df(self,df):
        # Select columns from dataframe and convert to dictionary format for dedupe.io

        # get feature columns from the dataframe (using column indices)
        feature_df = df.iloc[:, self.FEATURE_MATCHING_COLUMNS]

        # rename features of the data frame before converting dataframe to dictionry
        # we need to construct a dictionary for the renaming map in order to rename columns in pandas
        old_column_names = feature_df.columns
        new_column_names = self.FEATURE_MATCHING_COLUMNS_STRINGS
        rename_dict = {key: val for key, val in zip(old_column_names, new_column_names)}

        # renaming the dataframe
        feature_df.rename(columns=rename_dict, inplace=True)

        # Combining Latitude and Longitude column to get location column
        feature_df['location'] = feature_df.loc[:, ['latitude', 'longitude']].apply(self.location_tuple, axis=1)

        return feature_df

    def create_feature_df(self,idx):
        # Select columns from dataframe and convert to dictionary format for dedupe.io

        df = self.df_list[idx]

        # get feature columns from the dataframe (using column indices)
        feature_df = df.iloc[:, self.FEATURE_MATCHING_COLUMNS]

        # rename features of the data frame before converting dataframe to dictionry
        # we need to construct a dictionary for the renaming map in order to rename columns in pandas
        old_column_names = feature_df.columns
        new_column_names = self.FEATURE_MATCHING_COLUMNS_STRINGS
        rename_dict = {key: val for key, val in zip(old_column_names, new_column_names)}

        # renaming the dataframe
        feature_df.rename(columns=rename_dict, inplace=True)

        # Combining Latitude and Longitude column to get location column
        feature_df['location'] = feature_df.loc[:, ['latitude', 'longitude']].apply(self.location_tuple, axis=1)
        feature_df['source'] = idx

        self.feature_df_list.append(feature_df)

        return feature_df

    def df_to_ddio_dict(self,feature_df):

        #Construct the dictionry for ddio matching
        feature_dict = feature_df.to_dict(orient='index')
        return feature_dict

    def link_dataframes(self,idx_1,idx_2):
        if self.linker_settings['blocking']:
            blocked_data = self.linker_blocker(idx_1, idx_2)
            candidates_gen = self.linker_candidates_gen(blocked_data, idx_1, idx_2)
            matches = self.linker.matchBlocks(candidates_gen)
        else:
            matches = self.linker_match(idx_1,idx_2)


        #restructure matches
        matches_1 = []
        matches_2 = []
        scores = []
        #TODO : Change data structure to store matches.
        # Currently we have only two matches. Should be able to handle more than two matches at once.
        for match in matches:
            if match[1] > self.threshold: #Right now threshold is set to 0, doesn't do anything else
                matches_1.append(match[0][0])
                matches_2.append(match[0][1])
                scores.append(match[1])

        match_list = [matches_1, matches_2, scores]

        #####################################
        # Li: create files included matched records
        # Create the pandas dataframe for the duplicated records
        df_matched_found_idx = pd.DataFrame(match_list)
        df_matched_found_idx = df_matched_found_idx.transpose()
        df_matched_found_idx.columns = ['Match_1_idx', 'Match_2_idx','Score']

        # Check if the output folder exist

        # Convert index to detailed records
        cols = copy.deepcopy(self.FEATURE_MATCHING_COLUMNS_STRINGS)
        cols.insert(0, 'Match_ID')
        df_matched_found = pd.DataFrame(columns=cols)

        match_id = 0
        add_index = 0

        for index, row in df_matched_found_idx.iterrows():
            row_to_append1 = list(self.df_list[idx_1].ix[row['Match_1_idx'],0:10])
            row_to_append1.insert(0, match_id)
            df_matched_found.loc[add_index] = row_to_append1
            add_index += 1

            row_to_append2 = list(self.df_list[idx_2].ix[row['Match_2_idx'],0:10])
            row_to_append2.insert(0, match_id)
            df_matched_found.loc[add_index] = row_to_append2
            add_index += 1

            match_id += 1

        # Save the dataframe to csv file
        #
        filename_1 = os.path.basename(self.file_list[idx_1])
        filename_2 = os.path.basename(self.file_list[idx_2])

        filepath = os.path.join(self.folder_output,
                                'Matched_found' + '_' + filename_1 + '_' + filename_2 + '_.csv')
        logger.info("Exporting matched records found to CSV file...")
        df_matched_found.to_csv(filepath, sep=',', na_rep='NA', header=True,
                                    index=False, index_label=False, encoding='utf-8')
        ########################################################


        filename_1 = os.path.basename(self.file_list[idx_1])
        filename_2 = os.path.basename(self.file_list[idx_2])
        print('Found {0} matches bethween files \"{1}\" and \"{2}\"'.format(len(matches_1), filename_1, filename_2))

        # Update report
        # Record the first file only once
        if not report.first:
            report.first = True
            report.count += 1

        report.cross_matches.loc[report.count] = [len(matches_1)]
        report.count += 1


        #self.construct_union_table([self.df_list[idx_1], self.df_list[idx_2]], match_list)
        self.construct_union_table([self.df_list[idx_1], self.df_list[idx_2]], match_list)

        if config.DEBUG:
            self.construct_union_table_debug([self.df_list[idx_1], self.df_list[idx_2]], match_list)

        return self.df_union_table

    def construct_union_table(self,df_pair, duplicates_list):
        # df_union_table = self.construct_matches_df(df_pair, duplicates_list)
        #
        # df_unmatched_1 = self.construct_non_matched_df(df_pair, duplicates_list, 0)
        # df_unmatched_2 = self.construct_non_matched_df(df_pair, duplicates_list, 1)
        #
        #
        # df_union_table = pd.concat([df_union_table, df_unmatched_1, df_unmatched_2], axis=0)
        # df_union_table.reset_index(inplace=True, drop=True)

        #TODO fix concat_df
        df_union_table = self.concat_df(df_pair, duplicates_list)

        self.df_union_table = self.preprocess_dataframe(df_union_table)
        report.final_count.loc[1] = len(self.df_union_table)


        return df_union_table


    def construct_union_table_debug(self,df_pair, duplicates_list):
        df_union_table = self.construct_matches_df_debug(df_pair, duplicates_list)

        df_unmatched_1 = self.construct_non_matched_df_debug(df_pair, duplicates_list, 0)
        df_unmatched_2 = self.construct_non_matched_df_debug(df_pair, duplicates_list, 1)

        df_union_table = pd.concat([df_union_table, df_unmatched_1, df_unmatched_2], axis=0)
        df_union_table.reset_index(inplace=True, drop=True)

        self.df_union_table_debug = self.preprocess_dataframe(df_union_table)

        return df_union_table

    def concat_df(self, df_pair, duplicates_list):


        #construct row indices for slicing

        #TODO refactor structure of duplicates_list
        #remove scores from duplicaes_lsit

        matches_list = duplicates_list[:-1]
        df_concat_col_list = list()
        #append header
        #construct list of non matched indices for each df
        non_matches_list = list()
        for i, matches in enumerate(matches_list):
            non_matches_set = set(df_pair[i].index) - set(matches_list[i])
            non_matches_list.append( list(non_matches_set) )


        #make header df
        df_header_list = list()
        # column 10 limit the identifier columns
        index_start_data = 10
        df_header = df_pair[0].ix[duplicates_list[0], :index_start_data]
        df_header.reset_index(inplace=True, drop=True)

        empty_str_list = ['' for x in matches_list[0]]
        consolidated_id = pd.Series(empty_str_list)
        for idx, df in enumerate(df_pair):
            id = df.ix[matches_list[idx], 0]
            id.reset_index(inplace=True, drop=True)
            consolidated_id += ' ' + id.apply(str)

        df_header.iloc[:,0] = consolidated_id
        df_header.reset_index(inplace=True, drop=True)

        # rename columns in df_header
        old_column_names = df_header.columns[:index_start_data]
        #TODO: move from config to class?
        rename_dict = {key: val for key, val in zip(old_column_names, config.UNION_TABLE_COLUMNS)}
        df_header.rename(columns=rename_dict, inplace=True)
        df_header_list.append(df_header)

        #construct master slice list
        slice_list = matches_list
        for j, non_matches in enumerate(non_matches_list):

            for i, slice_i in enumerate(slice_list):
                if i != j:
                    slice = [-1 for i in range(len(non_matches))] #form a list of invalid indices, see comment below
                else:
                    slice = non_matches
                slice_i += slice

        # append matched entries df
        # pandas slicing with invalid index using pandas.DataFrame.ix(....) returns  np.nan for the rows with invalid index
        # This allows us to construct rows filled with NAs easier
        for i, df in enumerate(df_pair):
            row_slice = slice_list[i]

            #TODO refactor slicing
            index_start_data = 10
            shape = df.shape
            col_slice = [0] + list(range(index_start_data, shape[1]))

            df_slice = df.ix[row_slice, col_slice]
            df_slice.reset_index(inplace=True, drop=True)
            df_concat_col_list.append(df_slice)

        # concatenate columns and append to a list for row concatenation
        df_concat_col = pd.concat(df_concat_col_list, axis=1)

        for i, non_matches in enumerate(non_matches_list):
            df_header = df_pair[i].ix[non_matches, :index_start_data]
            df_header.reset_index(inplace=True, drop=True)

            # rename columns in df_header
            old_column_names = df_header.columns[:index_start_data]
            # TODO: move from config to class?
            rename_dict = {key: val for key, val in zip(old_column_names, config.UNION_TABLE_COLUMNS)}
            df_header.rename(columns=rename_dict, inplace=True)
            df_header_list.append(df_header)

        df_header = pd.concat(df_header_list, axis=0)
        df_header.reset_index(inplace = True, drop = True)

        df_result = pd.concat([df_header, df_concat_col], axis = 1)

        return df_result


    def construct_matches_df(self,df_pair, duplicates_list):
        """

        :param df_pair: list of 2 pandas data framce
        :param duplicates_list: list of entries to concat
        :return: concatenated data frame
        """

        #
        # index of the column that limits the identifier features
        index_start_data = 10

        shape_1 = df_pair[0].shape
        shape_2 = df_pair[1].shape

        data_indices_1 = list(range(index_start_data, shape_1[1]))
        data_indices_2 = list(range(index_start_data, shape_2[1]))

        table_indices_1 = [0] + data_indices_1
        table_indices_2 = [0] + data_indices_2

        df_first = df_pair[0].ix[duplicates_list[0], table_indices_1]
        df_second = df_pair[1].ix[duplicates_list[1], table_indices_2]

        # reset index obhect, otherwise concat fails due to duplication
        df_first.reset_index(inplace=True, drop=True)
        df_second.reset_index(inplace=True, drop=True)

        # column 10 limit the identifier columns
        df_header = df_pair[0].ix[duplicates_list[0], :index_start_data]
        #df_header.iloc[:,0] = df_pair[0].ix[duplicates_list[0], 0] + df_pair[1].ix[duplicates_list[1], 0]
        df_header.reset_index(inplace=True, drop=True)

        # rename columns in df_header
        old_column_names = df_header.columns[:index_start_data]
        #TODO: move from config to class?
        rename_dict = {key: val for key, val in zip(old_column_names, config.UNION_TABLE_COLUMNS)}
        df_header.rename(columns=rename_dict, inplace=True)

        df_matches = pd.concat([df_header, df_first, df_second], axis=1)

        return df_matches

    def construct_non_matched_df(self,df_pair, duplicate_list, df_index):
        """

        :param df_pair: list of 2 pandas data framce
        :param duplicates_list: list of entries to concat
        :return: concatenated data frame
        """

        #
        # index of the column that limits the identifier features
        index_start_data = 10

        index_start_data = 10
        unique_diplicates = np.unique(duplicate_list[df_index])

        # construct the lists of on matched rows using set opearations
        full_list = list(df_pair[df_index].index)

        non_matched_rows = list(set(full_list) - set(unique_diplicates))

        shape = df_pair[df_index].shape
        table_indices = [0] + list(range(index_start_data, shape[1]))

        # slice dataframes of non matched records
        df_nonmatched = df_pair[df_index].ix[non_matched_rows, table_indices]
        df_nonmatched.reset_index(inplace=True, drop=True)

        # construct union table 1
        df_header = df_pair[df_index].ix[non_matched_rows, :index_start_data]
        df_header.reset_index(inplace=True, drop=True)
        df_header.iloc[:, 0] = df_header.iloc[:, 0].map(str)

        old_column_names = df_header.columns[:index_start_data]
        rename_dict = {key: val for key, val in zip(old_column_names, config.UNION_TABLE_COLUMNS)}
        df_header.rename(columns=rename_dict, inplace=True)

        if (df_index == 0):
            df_index_empty = 1
        else:
            df_index_empty = 0

        shape_empty = df_pair[df_index_empty].shape
        empty_table_indices = [0] + list(range(index_start_data, shape_empty[1]))
        empty_columns_names = df_pair[df_index_empty].columns[empty_table_indices]

        df_empty = pd.DataFrame(columns=empty_columns_names)

        if (df_index == 0):
            concat_list = [df_header, df_nonmatched, df_empty]
        else:
            concat_list = [df_header, df_empty, df_nonmatched]

        df_non_matches = pd.concat(concat_list, axis=1)

        return df_non_matches

    #TODO _debug functions were a quick fix, remove later
    def construct_matches_df_debug(self,df_pair, duplicates_list):
        """

        :param df_pair: list of 2 pandas data framce
        :param duplicates_list: list of entries to concat
        :return: concatenated data frame
        """
        # if self.debug_df_first:
        #     df_first = df_pair[0].ix[duplicates_list[0], self.FEATURE_MATCHING_COLUMNS]
        # else:
        #     df_first = df_pair[0].ix[duplicates_list[0], :]

        df_first = self.df_union_table_debug.ix[duplicates_list[0], :]
        df_second = df_pair[1].ix[duplicates_list[1], self.FEATURE_MATCHING_COLUMNS]

        # reset index obhect, otherwise concat fails due to duplication
        df_first.reset_index(inplace=True, drop=True)
        df_second.reset_index(inplace=True, drop=True)

        # rename columns in df_header
        df_matches = pd.concat([df_first, df_second], axis=1)

        return df_matches

    # TODO _debug functions were a quick fix, remove later
    def construct_non_matched_df_debug(self, df_pair, duplicate_list, df_index):
        """

        :param df_pair: list of 2 pandas data framce
        :param duplicates_list: list of entries to concat
        :return: concatenated data frame
        """

        # if self.debug_df_first:
        #     df_first = df_pair[0].ix[duplicates_list[0], self.FEATURE_MATCHING_COLUMNS]
        # else:
        #     df_first = df_pair[0].ix[duplicates_list[0], :]



        # construct the lists of on matched rows using set opearations
        if(df_index == 0):
            df_pair[0] = self.df_union_table_debug

        unique_diplicates = np.unique(duplicate_list[df_index])

        full_list = list(df_pair[df_index].index)
        non_matched_rows = list(set(full_list) - set(unique_diplicates))

        # slice dataframes of non matched records
        if (df_index == 0):
            df_nonmatched = df_pair[df_index].ix[non_matched_rows, :]

            df_index_empty = 1
            empty_columns_names = df_pair[df_index_empty].columns[self.FEATURE_MATCHING_COLUMNS]
        else:
            df_nonmatched = df_pair[df_index].ix[non_matched_rows, self.FEATURE_MATCHING_COLUMNS]

            df_index_empty = 0
            empty_columns_names = df_pair[df_index_empty].columns[:]

        df_nonmatched.reset_index(inplace=True, drop=True)

        df_empty = pd.DataFrame(columns=empty_columns_names)

        if (df_index == 0):
            concat_list = [df_nonmatched, df_empty]
        else:
            concat_list = [df_empty, df_nonmatched]

        df_non_matches = pd.concat(concat_list, axis=1)

        return df_non_matches

    def save_union_table_csv(self):
        """
            Writes CSV file 'uniontable.csv' from the given dataframe
            :param completeData: All data in a single DataFrame
            :type completeData: List
        """
        #TODO change the path below
        filepath = os.path.join(self.folder_output,
                                'uniontable.csv')  # '/record_linkage_output_' + str(time_now())+'.csv'
        logger.info("Exporting union table to CSV file...")
        self.df_union_table.to_csv(filepath, sep=',', na_rep='NA', header=True,
                            index=False, index_label=False, encoding='utf-8',
                            quoting=csv.QUOTE_NONNUMERIC)

        if config.DEBUG:
            filepath = os.path.join(self.folder_output,
                                    'uniontable_debug.csv')  # '/record_linkage_output_' + str(time_now())+'.csv'
            logger.info("Exporting debug table to CSV file...")
            self.df_union_table_debug.to_csv(filepath, sep=',', na_rep='NA', header=True,
                                       index=False, index_label=False, encoding='utf-8',
                                       quoting=csv.QUOTE_NONNUMERIC)

    def create_block_key(self, grouped_df, df_idx_set):
        # group_key = block['bl']
        group_idx_set = set(grouped_df['source'])
        if group_idx_set == df_idx_set:
            #print(grouped_df)
            block_key = grouped_df['block_key'].values[0]
            #print(block_key)
            return block_key
        else:
            return None

    def linker_valid_block(self, grouped_df, df_idx_set):
        # group_key = block['bl']
        group_idx_set = set(grouped_df['source'])
        if group_idx_set == df_idx_set:
            block_key = grouped_df['block_key'].values[0]
            return True
        else:
            return None

    def linker_blocker(self,idx_1,idx_2):

        time_start = time.time()

        df_1 = self.feature_df_list[idx_1]
        df_2 = self.feature_df_list[idx_2]

        #use combined data structures to build accurate blocks
        df_combined = pd.concat([df_1, df_2], axis=0)
        dict_combined = self.df_to_ddio_dict(df_combined)
        tuple_combined = ((key, val) for key, val in dict_combined.items())

        ####################################################################################
        #create inverted index for unique elmenents in both dataframes
        ####################################################################################
        if self.linker_settings['enforce_blocking_fields']:
            index_fields = self.linker_blocker_index_fields
            print('Blocking fields are enforced. Following fields are used for blocking:', index_fields)
        else:
            index_fields = list(self.linker.blocker.index_fields)
            print('Using suggested blocking fields. Following fields are used for blocking:', index_fields)

        index_fields = list(self.linker.blocker.index_fields)
        print('Enforcement of blocking fields for linker is not available. '
              'Following fields are used for blocking:', index_fields)
        for field in index_fields:
            unique_data = df_combined[field].unique()
            self.linker.blocker.index(tuple(unique_data), field)

        ####################################################################################
        # Get blocking map
        ####################################################################################

        print("Create blocking map {0:.2f}".format(time.time() - time_start))
        block_data = self.linker.blocker(tuple_combined)
        blocking_map = pd.DataFrame(block_data, columns=['block_key', 'index'])
        # blocking_map.drop_duplicates(subset=['block_key', 'index'], inplace=True)
        blocking_map = blocking_map.drop_duplicates(subset=['block_key', 'index'])

        print("Add source to blocking map {0:.1f}".format(time.time() - time_start))

        #add source to blocking map
        index_slice = blocking_map['index'].values
        blocking_map['source'] = df_combined.ix[index_slice, 'source'].values

        ####################################################################################
        # Create plural key
        ####################################################################################

        df_idx_set = set([idx_1, idx_2])
        # print('Test count plural key count, {0:.2f}'.format(time.time() - time_start))
        # blocking_map_plural = blocking_map.groupby(['block_key'], as_index=False, group_keys=True)['index'].count()
        # blocking_map_plural = blocking_map_plural[blocking_map_plural['index'] > 1]
        # print(len(blocking_map_plural))

        print('Test count plural key mean source, {0:.2f}'.format(time.time() - time_start))
        # dirty but it works and it is fast
        # source is always either idx_1 or idx_2,
        # and if entries from source=idx_1 and source=idx_2 are present is the grouped dataframe
        # then mean always belongs to (idx_min, idx_max)
        blocking_map_plural = blocking_map.groupby(['block_key'], as_index=False, group_keys=True)['source'].mean()
        min_idx = min(idx_1, idx_2)
        max_idx = max(idx_1, idx_2)
        plural_key = blocking_map_plural[ (blocking_map_plural['source'] > min_idx) & (blocking_map_plural['source'] < max_idx) ]
        plural_key.reset_index(inplace=True, drop=True)
        plural_key['block_id'] = plural_key.index
        # print(plural_key)

        # print(len(blocking_map_plural))
        #
        #
        # print('Test count plural key count, {0:.2f}'.format(time.time() - time_start))
        # blocking_map_temp = blocking_map[['block_key', 'source']]
        # blocking_map_plural = blocking_map_temp.groupby(['block_key'], as_index=False, group_keys=True).agg(lambda x: set(x))
        # blocking_map_plural = blocking_map_plural[blocking_map_plural['source'] == df_idx_set]
        # print(len(blocking_map_plural))
        #
        # # print(blocking_map_plural)
        # # print('Test count plural key start filter, {0:.2f}'.format(time.time() - time_start))
        # # df_idx_set = set([idx_1, idx_2])
        # # blocking_map_plural = blocking_map.groupby(['block_key'], as_index=False).filter(lambda x: self.linker_valid_block(x,df_idx_set))
        # # print(blocking_map_plural.keys())
        #
        # print('Create plural key groupby, {0:.2f}'.format(time.time() - time_start))
        # block_group = blocking_map.groupby(['block_key'], as_index=False)
        #
        # df_idx_set = set([idx_1, idx_2])
        # block_groups = ([key, df, df_idx_set] for key, df in block_group)
        #
        # print('Create plural key start apply, {0:.2f}'.format(time.time() - time_start))
        #
        # block_keys = block_group.apply(lambda x: self.create_block_key(x, df_idx_set))
        # # print(block_keys.values)
        #
        # block_keys = block_keys.values
        # block_keys = list(filter(lambda x: x!=None, block_keys))
        # block_id = range(len(block_keys))

        # print('Create plural key grouped_df, {0:.2f}'.format(time.time() - time_start))
        # #multiprocessing block key creation
        # cpu_count = mp.cpu_count()
        # pool = mp.Pool(processes=cpu_count)
        # block_keys_pool = pool.map(utils.create_block_key, block_groups)
        #
        # id = 0
        # block_keys = []
        # block_id = []
        # for key in block_keys_pool:
        #     if(key != None):
        #         block_keys.append(key)
        #         block_id.append(id)
        #         id += 1
        # pool.close()

        # lset = set
        # df_idx_set = set([idx_1, idx_2])
        # id = int(0)
        # n_blocks = len(block_group)
        # block_keys = np.empty(n_blocks, str)
        # block_id = np.empty(n_blocks, str)
        # for group_key, grouped_df in block_group:
        #     # idx_unique = sorted(grouped_df['source'].unique())
        #     # if(idx_unique == sorted([idx_1, idx_2])):
        #     group_idx_set = lset(grouped_df['source'])
        #     if group_idx_set == df_idx_set:
        #         grouped_df['block_id'] = id
        #
        #         #plural_block = plural_block.append(grouped_df)
        #
        #         block_keys[id] = group_key
        #         block_id[id] = id
        #         id += 1
        #
        # print('Create plural key, {0:.2f}'.format(time.time() - time_start))
        #
        # block_keys = block_keys[:id]
        # block_id = block_id[:id]

        # plural_key = pd.DataFrame({'block_key': block_keys, 'block_id': block_id})

        print('Create plural_block, {0:.2f}'.format(time.time() - time_start))
        plural_block = pd.merge(left=plural_key, right=blocking_map, on='block_key')



        ####################################################################################
        # Create covered blocks
        # Iterate through each row of plural block and
        ####################################################################################

        print('Create covered blocks df, {0:.2f}'.format(time.time() - time_start))
        covered_blocks_df = plural_block[['index', 'block_id']]
        covered_blocks_df = covered_blocks_df.groupby(['index'], as_index=False, group_keys=True).agg(lambda x: list(x))

        def create_smaller_ids(row):
            index = row.name
            dedupe_index = row['index']
            block_ids = row['block_id']

            result_list = []

            for block_id in block_ids:
                smaller_ids_list = filter(lambda x: x < block_id, block_ids)
                smaller_ids = ','.join(str(e) for e in smaller_ids_list)
                entry = (dedupe_index, block_id, smaller_ids)
                # yield (dedupe_index, block_id, smaller_ids)
                result_list.append(entry)
            return result_list

        print('Smaller_coverage new, {0:.2f}'.format(time.time() - time_start))
        smaller_coverage_rows = covered_blocks_df.apply(create_smaller_ids, axis=1, reduce = True)
        # print(res)
        smaller_coverage = []
        for smaller_coverage_row in smaller_coverage_rows:
            for x in smaller_coverage_row:
                smaller_coverage.append(x)

        # print(smaller_coverage)
        smaller_coverage_df = pd.DataFrame(smaller_coverage,
                                           columns=['index', 'block_id', 'smaller_ids'])
        # #legacy smaller coverage
        # print('Smaller_coverage, {0:.2f}'.format(time.time() - time_start))
        # smaller_coverage = []
        # for row in covered_blocks_df.iterrows():
        #     dedupe_index = row[1]['index']
        #     block_ids =  row[1]['block_id']
        #     for block_id in block_ids:
        #         smaller_ids_list = filter(lambda x: x < block_id, block_ids)
        #         smaller_ids = ','.join(str(e) for e in smaller_ids_list)
        #         myTuple = (dedupe_index, block_id, smaller_ids)
        #         smaller_coverage.append(myTuple)
        #
        # smaller_coverage_df = pd.DataFrame(smaller_coverage,
        #                                    columns=['index', 'block_id', 'smaller_ids'])

        #print(smaller_coverage_df)



        #
        # res = covered_blocks_df.apply(create_smaller_ids, axis=1)
        # print(res)
        # print(len(res))



        # print('Create covered blocks, {0:.2f}'.format(time.time() - time_start))
        # covered_blocks = {}
        # for row in plural_block.iterrows():
        #     dedupe_index = row[1]['index']
        #     block_id = int(row[1]['block_id'])
        #     if dedupe_index in covered_blocks:
        #         covered_blocks[dedupe_index].append(block_id)
        #     else:
        #         covered_blocks[dedupe_index] = [block_id]

        ####################################################################################
        # Smaller_coverage
        ####################################################################################

        # print('Smaller_coverage, {0:.2f}'.format(time.time() - time_start))
        # smaller_coverage = []
        # for dedupe_index, block_ids in covered_blocks.items():
        #     for block_id in block_ids:
        #         smaller_ids_list = filter(lambda x: x < block_id, block_ids)
        #         smaller_ids = ','.join(str(e) for e in smaller_ids_list)
        #         myTuple = (dedupe_index, block_id, smaller_ids)
        #         smaller_coverage.append(myTuple)
        #
        # smaller_coverage_df = pd.DataFrame(smaller_coverage,
        #                                    columns=['index', 'block_id', 'smaller_ids'])

        # print(len(smaller_coverage_df))

        ####################################################################################
        # Create result set
        ####################################################################################

        print('Create result set, {0:.2f}'.format(time.time() - time_start))
        df_combined['index'] = df_combined.index
        result_set = pd.merge(left=df_combined, right=smaller_coverage_df, left_on='index', right_on='index',
                              how='right')
        del df_combined['index']

        result_set.sort(columns='block_id', inplace=True)

        print('Blocking is finished, {0:.2f}'.format(time.time() - time_start))
        return result_set


        #return blocked_data


    def dedupe_blocker(self,idx):
        #TODO: Make this pretty and useable by Deduper and Linker
        test_dict = self.dict_list[idx]
        test_df = self.feature_df_list[idx]

        print ('creating inverted index')

        #either force blocking fields or use dedupe suggested blocking rules

        if self.deduper_settings['enforce_blocking_fields']:
            index_fields = self.dedupe_blocker_index_fields
            print('Deduper blocking fields are enforced. Following fields are used for blocking:', index_fields)
        else:
            index_fields = list(self.deduper.blocker.index_fields)
            print('Using suggested blocking fields for deduper. Following fields are used for blocking:', index_fields)

        for field in index_fields:
            unique_data = test_df[field].unique()
            self.deduper.blocker.index(tuple(unique_data), field)

        full_data = ((key, val) for key, val in test_dict.items())
        # full_data = ((val['id'], val) for key, val in test_dict.items())
        # We do not use id as the index/key over here. In some of the Dedupe examples they use the Id as an identifier.

        ####################################################################################
        # Get blocking map
        ####################################################################################


        print('Create blocking map')
        b_data = self.deduper.blocker(full_data)
        blocking_map = pd.DataFrame(b_data, columns=['block_key', 'dedupe_index'])
        #deduplicate deduplication blocking map
        blocking_map.drop_duplicates(subset=['block_key', 'dedupe_index'], inplace =True)

        # blocking_map.set_index(['block_key'],inplace=True,drop=False)

        ####################################################################################
        # Create plural key
        ####################################################################################

        print('Create plural key')
        plural_key = blocking_map.groupby(['block_key'], as_index=False)['dedupe_index'].count()
        plural_key = plural_key[plural_key['dedupe_index'] > 1]
        del plural_key['dedupe_index'] # Deleting dedupe index because aggregating by count causes the dedupe_index to now have counts rather than the index values
        #Removing the column to avoid confusion.
        # del blocking_map['block_key']

        # Resetting index to generate id's for block keys. These will
        # be used later to to join.
        plural_key.reset_index(inplace=True, drop=True)
        plural_key['block_id'] = plural_key.index

        #Executing SQL like joins using merge.
        plural_block = pd.merge(left=plural_key, right=blocking_map, on='block_key')

        ####################################################################################
        # Create covered blocks
        # Iterate through each row of plural block and
        ####################################################################################

        print('Create covered blocks')
        covered_blocks = {}
        for row in plural_block.iterrows():
            dedupe_index = row[1]['dedupe_index']
            block_id = int(row[1]['block_id'])
            if dedupe_index in covered_blocks:
                covered_blocks[dedupe_index].append(block_id)
            else:
                covered_blocks[dedupe_index] = [block_id]

        ####################################################################################
        # Smaller_coverage
        ####################################################################################

        print('Create small coverage')
        smaller_coverage = []
        for dedupe_index, block_ids in covered_blocks.items():
            for block_id in block_ids:
                smaller_ids_list = filter(lambda x: x < block_id, block_ids)
                smaller_ids = ','.join(str(e) for e in smaller_ids_list)
                myTuple = (dedupe_index, block_id, smaller_ids)
                smaller_coverage.append(myTuple)

        smaller_coverage_df = pd.DataFrame(smaller_coverage, columns=['dedupe_index', 'block_id', 'smaller_ids'])

        ####################################################################################
        # Create result set
        ####################################################################################

        print("Create result set")
        test_df['dedupe_index'] = test_df.index
        result_set = pd.merge(left=test_df, right=smaller_coverage_df, left_on='dedupe_index', right_on='dedupe_index',
                              how='right')
        del test_df['dedupe_index']

        result_set.sort(columns='block_id', inplace=True)

        return result_set
        # blocks = list(candidates_gen(result_set))
        # print (blocks)

    def linker_candidates_gen(self,result_set,idx_1,idx_2):
        lset = set

        block_id = None
        records = [[],[]]
        start_time = time.time()

        i = 0
        for row in result_set.iterrows():
            row = row[1]
            if row['block_id'] != block_id:
                if records[0] and records[1]:  # Doesn't execute if list is empty
                    yield records

                block_id = row['block_id'] #If the block id changes, create a new list of records
                records = [[],[]]
                i += 1

                if i % 10000 == 0:
                    print (i, "blocks")
                    print (time.time() - start_time, "seconds")

            smaller_ids = row['smaller_ids']

            if smaller_ids:
                smaller_ids = lset(smaller_ids.split(','))
            else:
                smaller_ids = lset([])

            if row['source'] == idx_1:
                records[0].append((row['index'], row, smaller_ids))

            if row['source'] == idx_2:
                records[1].append((row['index'], row, smaller_ids))

        if records:
            yield records


    def candidates_gen(self,result_set):
        lset = set

        block_id = None
        records = []
        start_time = time.time()
        i = 0
        for row in result_set.iterrows():
            row = row[1]
            if row['block_id'] != block_id:
                if records:  # Doesn't execute if list is empty
                    yield records

                block_id = row['block_id'] #If the block id changes, create a new list of records
                records = []
                i += 1

                if i % 10000 == 0:
                    print (i, "blocks")
                    print (time.time() - start_time, "seconds")

            smaller_ids = row['smaller_ids']

            if smaller_ids:
                smaller_ids = lset(smaller_ids.split(','))
            else:
                smaller_ids = lset([])
                #print(block_id, smaller_ids)

            records.append((row['dedupe_index'], row, smaller_ids))

        if records:
            yield records

    def json_train_data_to_csv(self,filepath):
        # Read in Json training data
        with open(filepath) as json_data:
            data = json.load(json_data)
        df = pd.DataFrame.from_dict(data, orient='index')

        # Create empty dataframes for output
        output = pd.DataFrame('miss', index=range(0, sum(df.count())),
                                  columns=['Record_1', 'Record_2', 'Match_or_Distinct'])
        output_index = 0
        for index, row in df.iterrows():
            for cell in row:
                if cell != None:
                    record1_dict = cell['__value__'][0].copy()
                    record2_dict = cell['__value__'][1].copy()

                    del record1_dict['location']
                    del record2_dict['location']

                    record1_str = (
                        ', '.join('{0}: {1}'.format(key, val) for key, val in sorted(record1_dict.items())))
                    record2_str = (
                        ', '.join('{0}: {1}'.format(key, val) for key, val in sorted(record2_dict.items())))

                    output.iloc[output_index, :]['Record_1'] = record1_str
                    output.iloc[output_index, :]['Record_2'] = record2_str
                    output.iloc[output_index, :]['Match_or_Distinct'] = str(index)
                    output_index += 1

        out_path = os.path.join('model',filepath.split('/')[-1].split('.')[0]+'.csv')
        output.to_csv(out_path,sep=',')


logger = logging.getLogger('BudLink.logger')
dedupe_logger = logging.getLogger('dedupe')
dedupe_logger.setLevel(logging.CRITICAL)


if __name__ == "__main__":
    #input_file = ['../sample_data/merida_input_inegi.csv','../sample_data/merida_input_geoloc.csv','../sample_data/merida_input_salesforce.csv']
    input_file = ['../test_data/chi_sample_ABI.csv','../test_data/chi_sample_Google.csv']
    testLinker = BudLink(input_file)
    testLinker.deduper_settings['blocking'] = True
    testLinker.deduper_settings['enforce_blocking_fields'] = False
    testLinker.linker_settings['blocking'] = True
    testLinker.read_input_files()
    linker_setting_file = os.path.join('..','model', 'chicago_linker_learned_setting')
    testLinker.linker_settings['settings_file'] = linker_setting_file
    # testLinker.load_deduper()
    # testLinker.deduplicate_all()
    testLinker.load_linker()
    testLinker.linker_match_all()

    #print (testLinker.linker.blocker.index_fields)
    # blocked_data = testLinker.linker_blocker(0,1)
    # candidates_gen = testLinker.linker_candidates_gen(blocked_data,0,1)
    # linked_list = testLinker.linker.matchBlocks(candidates_gen)
    # print (linked_list)
    # print (len(linked_list))

###
##