import pandas as pd
from random import randint
import os
from tools import config
from tools import utils
### For PDF ###
import time
from reportlab.lib.enums import TA_JUSTIFY, TA_LEFT, TA_CENTER
from reportlab.lib.pagesizes import letter
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Image, Flowable
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.units import inch

import matplotlib
# Force matplotlib to not use any Xwindows backend.
matplotlib.use('Agg')

import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import matplotlib.pyplot as plt
###############


class MCLine(Flowable):
    """
    Line flowable --- draws a line in a flowable
    http://two.pairlist.net/pipermail/reportlab-users/2005-February/003695.html
    """

    # ----------------------------------------------------------------------
    def __init__(self, width, height=0):
        Flowable.__init__(self)
        self.width = width
        self.height = height

    # ----------------------------------------------------------------------
    def __repr__(self):
        return "Line(w=%s)" % self.width

    # ----------------------------------------------------------------------
    def draw(self):
        """
        draw the line
        """
        self.canv.line(0, self.height, self.width, self.height)


class Report:

    def __init__(self,output_path, save_report=False):
        self.first = False
        self.count = 1
        self.output_path_xl= output_path + '.' + config.REPORT_EXTENSION
        self.output_path_pdf = output_path + '.pdf'
        self.input_files = [] #List of files given as input
        self.num_rows = pd.DataFrame(columns=['Num Records']) #Number of rows in each input file
        self.self_duplicates = pd.DataFrame(columns=['Num Duplicates'])
        self.cross_matches = pd.DataFrame(columns=['Num Matches'])
        self.final_count = pd.DataFrame(columns=['Final Count'])
        self.sheet_name = 'Summary Stats'

        self.save_report = save_report

        #self plot dimensions
        self.plot_size = (6, 4)

        # analysis info
        self.num_buyers = 0
        self.num_non_buyers = 0
        self.num_sellers = 0
        self.num_non_sellers = 0
        self.num_maybe_sellers = 0
        self.num_known = 0
        self.num_unknown = 0

        self.sellers = pd.DataFrame(columns=['Yes', 'No', 'Maybe'], index=['sellers'])
        self.buyers = pd.DataFrame(columns=['Yes', 'No'], index=['buyers'])
        self.known = pd.DataFrame(columns=['Yes', 'No'], index=['known'])


    def save_reports(self):
        if (self.save_report == True):
            self.saveReport()
            self.print_report_pdf()

    def saveReport(self):

        #print (self.input_files)
        #self.self_duplicates.iloc[:,0] = self.input_files
        #print (self.self_duplicates)
        start_row = start_column = 0
        writer = pd.ExcelWriter(self.output_path_xl, engine='xlsxwriter')

        self.num_rows['File Name'] = self.input_files
        self.num_rows.to_excel(writer,
                                    self.sheet_name,
                                    startrow=start_row,
                                    startcol=start_column
                                    )
        start_row = start_row + len(self.num_rows) + 2
        self.self_duplicates['File Name'] = self.input_files
        self.self_duplicates.to_excel(writer,
                                      self.sheet_name,
                                      startrow = start_row,
                                      startcol = start_column
                                      )
        if len(self.num_rows) > 1:

            start_row = start_row + len(self.self_duplicates) + 2
            output_str = [ 'temp_uniontable_'+ str(output) for output in range(len(self.input_files)-1)]
            output_str[-1] = 'uniontable.csv'

            self.cross_matches['Input File 1'] = [self.input_files[0]] + output_str[:-1]
            self.cross_matches['Input File 2'] = self.input_files[1:]
            self.cross_matches['Output File'] = output_str

            self.cross_matches.to_excel(writer,
                                    self.sheet_name,
                                    startrow = start_row,
                                    startcol = start_column)

            start_row = start_row + len(self.cross_matches) + 2
            self.final_count.to_excel(writer,
                                        self.sheet_name,
                                        startrow=start_row,
                                        startcol=start_column
                                        )

        start_row = start_row + len(self.final_count) + 2
        self.sellers.to_excel(writer,
                                      self.sheet_name,
                                      startrow=start_row,
                                      startcol=start_column
                                      )

        start_row = start_row + len(self.final_count) + 2
        self.buyers.to_excel(writer,
                              self.sheet_name,
                              startrow=start_row,
                              startcol=start_column
                              )

        start_row = start_row + len(self.final_count) + 2
        self.known.to_excel(writer,
                              self.sheet_name,
                              startrow=start_row,
                              startcol=start_column
                              )



        worksheet = writer.sheets['Summary Stats']
        worksheet.set_column('B:B',15)
        worksheet.set_column('C:C', 45)
        worksheet.set_column('D:D', 45)
        worksheet.set_column('E:E', 45)
        print ('Report saved')

        writer.save()

    def print_report_pdf(self):
        """ print pdf report.
            :param names: List of input files' names
            :param rows: List of number of rows in each file
            :param duplications: List of number of duplications in each file
            :param matches: list of number of matches across all files
            :param total: total number of pocs
            :type names: List
            :type rows: list
            :type duplications: List
            :type matches: list
            :type total: int

            Example:
            print_report_pdf(["google","ABI","Yelp"],[20,10,30],100)
        """
        # setup Template and style
        pdf_document = SimpleDocTemplate(self.output_path_pdf, pagesize=letter,
                                rightMargin=72, leftMargin=72,
                                topMargin=72, bottomMargin=18)

        styles = getSampleStyleSheet()
        styles.add(ParagraphStyle(name='Center', alignment=TA_CENTER))

        story_list = []
        # basic info
        # logo = "plots/AB_InBev_logo.jpg"
        bud_lab_logo = os.path.join('plots','bud_lab_logo.jpg')
        # im = Image(logo, 2 * inch, 2 * inch)
        sized_image = Image(bud_lab_logo, 2 * inch, 0.75 * inch)
        formatted_time = time.ctime()
        BudLab = "BudLab"
        address_parts = ["1800 S Oak St", "Champaign, IL 61820"]
        title = "Eagle Eye Report"
        subtitle = "i- Record Linkage"
        subtitle2 = "ii- Analysis"

        # pre_calculations
        number_of_files = len(self.input_files)
        number_of_dup = self.self_duplicates.sum()
        number_of_matches = self.cross_matches

        # adding logo
        # Story.append(im)
        story_list.append(sized_image)

        # adding time
        ptext = '<font size=12>%s</font>' % formatted_time
        story_list.append(Paragraph(ptext, styles["Normal"]))
        story_list.append(Spacer(1, 12))

        # adding name
        ptext = '<font size=12>%s</font>' % BudLab
        story_list.append(Paragraph(ptext, styles["Normal"]))

        # adding address
        for part in address_parts:
            ptext = '<font size=12>%s</font>' % part.strip()
            story_list.append(Paragraph(ptext, styles["Normal"]))

        story_list.append(Spacer(1, 12))

        # adding a line
        line = MCLine(450)
        story_list.append(line)
        story_list.append(Spacer(1, 12))

        # adding title
        ptext = '<font size=14>%s</font>' % title
        story_list.append(Paragraph(ptext, styles["Center"]))
        story_list.append(Spacer(1, 12))
        story_list.append(Spacer(1, 12))

        # adding subtitle
        ptext = '<font size=14>%s</font>' % subtitle
        story_list.append(Paragraph(ptext, styles["Normal"]))
        story_list.append(Spacer(1, 12))
        story_list.append(Spacer(1, 12))

        # adding files info
        ptext = '<font size=12>%s files have been entered: </font>' % len(self.input_files)
        story_list.append(Paragraph(ptext, styles["Normal"]))
        story_list.append(Spacer(1, 4))
        for file, row in zip(self.input_files, list(self.num_rows['Num Records'])):
            ptext = '<font size=12>* %s has %s rows</font>' % (file.strip(), row)
            story_list.append(Paragraph(ptext, styles["Normal"]))
            story_list.append(Spacer(1, 2))

        story_list.append(Spacer(1, 12))

        # adding duplications
        ptext = '<font size=12>%s total duplicates were deleted: </font>' % self.self_duplicates['Num Duplicates'].sum()
        story_list.append(Paragraph(ptext, styles["Normal"]))
        story_list.append(Spacer(1, 4))
        #print(self.self_duplicates)
        for idx, file in enumerate(self.input_files):
            n_duplicates = int(self.self_duplicates['Num Duplicates'][idx])
            file = os.path.basename(file)
            ptext = '<font size=12>* %i duplicates were deleted from %s file</font>' %(n_duplicates, file)
            story_list.append(Paragraph(ptext, styles["Normal"]))
            story_list.append(Spacer(1, 2))

        story_list.append(Spacer(1, 12))

        # generating duplication plot
        fig, ax =plt.subplots(figsize = self.plot_size)

        left = np.arange(len(self.input_files))
        height = self.self_duplicates['Num Duplicates'].values
        x_labels = [os.path.basename(path) for path in self.input_files]


        ax.bar(left, height, align='center', alpha=0.5, width = 0.55)
        plt.xticks(left, x_labels)

        plt.ylabel('Number of Duplicates')

        filaname_duplicates_plots = 'plots/dup2.png'
        plt.savefig(filaname_duplicates_plots)

        # adding plot of duplciates

        im3 = Image(filaname_duplicates_plots, self.plot_size[0] * inch, self.plot_size[1] * inch)

        story_list.append(im3)
        story_list.append(Spacer(1, 12))

        # moving to next page
        # TODO moving to next page should be done automatically

        story_list.append(Spacer(1, 12))

        # adding matches
        ptext = '<font size=12>%s total matches were found: </font>' % self.cross_matches['Num Matches'].sum()
        story_list.append(Paragraph(ptext, styles["Normal"]))
        story_list.append(Spacer(1, 4))
        for idx, num_matches in enumerate(self.cross_matches['Num Matches']):

            num_matches = int(num_matches)
            if idx == 0:

                filename_1 = os.path.basename(self.input_files[0])
                filename_2 = os.path.basename(self.input_files[1])

                ptext = '<font size=12>* %i matches were found between %s and %s</font>' % (
                                            num_matches, filename_1, filename_2)
                story_list.append(Paragraph(ptext, styles["Normal"]))
                story_list.append(Spacer(1, 2))
            elif idx > 0:
                filename = self.input_files[idx+1]
                ptext = '<font size=12>* %i additional entries were matched with %s</font>' % (
                    num_matches, filename)
                story_list.append(Paragraph(ptext, styles["Normal"]))
                story_list.append(Spacer(1, 2))


        story_list.append(Spacer(1, 12))

        # generating matches plot

        if( len(self.input_files) > 1 ):

            #plotting variables
            left = np.arange(len(self.input_files) - 1)
            height = self.cross_matches['Num Matches'].values

            fig, ax = plt.subplots(figsize=self.plot_size)
            # create x-labels
            x_labels = list()

            filename_1 = os.path.basename( self.input_files[0] )
            filename_2 = os.path.basename( self.input_files[1] )

            x_labels.append(filename_1 + '-' + filename_2)
            for i in range(2, len(self.input_files)):
                filename_2 = os.path.basename(self.input_files[i])
                x_labels.append( 'union_table.csv -' + filename_2)

            ax.bar(left, height, align='center', alpha=0.5, width = 0.55)

            #decorate the plots
            plt.xticks(left, x_labels)
            plt.ylabel('Number of Linked Records')

            #plt.title('Fig 2. Number of linked records for a pair of files')
            filename_linked_records = "plots/match.tiff"
            fig.savefig(filename_linked_records)

            # adding matches plot
            image_linked_records = Image(filename_linked_records,
                        self.plot_size[0] * inch, self.plot_size[1] * inch)
            story_list.append(image_linked_records)
            story_list.append(Spacer(1, 12))

        ptext = '<font size=12>** final total number of POCs is %s</font>' % self.final_count.iloc[0,0]
        story_list.append(Paragraph(ptext, styles["Normal"]))
        story_list.append(Spacer(1, 12))

        for idx, final_num in enumerate([i-j for i,j in zip(list(self.num_rows['Num Records']),  list(self.self_duplicates['Num Duplicates']))]):
            ptext = '<font size=12>* %s came from %s</font>' % (final_num, self.input_files[idx])
            story_list.append(Paragraph(ptext, styles["Normal"]))
            story_list.append(Spacer(1, 2))

        story_list.append(Spacer(1, 12))

        # analysis
        ptext = '<font size=14>%s</font>' % subtitle2
        story_list.append(Paragraph(ptext, styles["Normal"]))
        story_list.append(Spacer(1, 12))
        story_list.append(Spacer(1, 12))

        # sellers
        ptext = '<font size=12>Sellers:</font>'
        story_list.append(Paragraph(ptext, styles["Normal"]))
        story_list.append(Spacer(1, 2))
        if self.num_sellers == 0 and self.num_non_sellers == 0 and self.num_maybe_sellers == 0:
            ptext = '<font size=12>* There were not enough information to calculate sellers info</font>'
            story_list.append(Paragraph(ptext, styles["Normal"]))
        else:
            ptext = '<font size=12>* %s of POCs sell alcohol</font>' % self.num_sellers
            story_list.append(Paragraph(ptext, styles["Normal"]))
            story_list.append(Spacer(1, 2))
            ptext = '<font size=12>* %s of POCs do not sell alcohol</font>' % self.num_non_sellers
            story_list.append(Paragraph(ptext, styles["Normal"]))
            story_list.append(Spacer(1, 2))
            ptext = '<font size=12>* %s of POCs might be selling alcohol</font>' % self.num_maybe_sellers
            story_list.append(Paragraph(ptext, styles["Normal"]))
            story_list.append(Spacer(1, 2))

        story_list.append(Spacer(1, 12))

        # generating sellers plot

        plt.clf()
        y_pos = np.arange(3)

        plt.bar(y_pos, [self.num_sellers,self.num_non_sellers,self.num_maybe_sellers], align='center', alpha=0.5)
        plt.xticks(y_pos, ["Yes", "No", "Maybe"])
        plt.ylabel('NUmber of POCs')
        plt.title('Fig 3')
        plt.savefig('plots/sellers.png')

        # adding sellers plot
        bar3 = "plots/sellers.png"
        im5 = Image(bar3, 5 * inch, 2.5 * inch)
        story_list.append(im5)
        story_list.append(Spacer(1, 12))


        # buyers
        ptext = '<font size=12>Buyers:</font>'
        story_list.append(Paragraph(ptext, styles["Normal"]))
        story_list.append(Spacer(1, 2))
        if self.num_buyers == 0 and self.num_non_buyers == 0:
            ptext = '<font size=12>* There were not enough information to calculate buyers info</font>'
            story_list.append(Paragraph(ptext, styles["Normal"]))
        else:
            ptext = '<font size=12>* %s of POCs are ABI buyers</font>' % self.num_buyers
            story_list.append(Paragraph(ptext, styles["Normal"]))
            story_list.append(Spacer(1, 2))
            ptext = '<font size=12>* %s of POCs are not ABI buyers</font>' % self.num_non_buyers
            story_list.append(Paragraph(ptext, styles["Normal"]))
            story_list.append(Spacer(1, 2))

        story_list.append(Spacer(1, 12))

        # generating buyers plot

        plt.clf()
        y_pos = np.arange(2)

        plt.bar(y_pos, [self.num_buyers, self.num_non_buyers], align='center', alpha=0.5)
        plt.xticks(y_pos, ["Yes", "No"])
        plt.ylabel('NUmber of POCs')
        plt.title('Fig 4')
        plt.savefig('plots/buyers.png')

        # adding buyers plot
        bar4 = "plots/buyers.png"
        im6 = Image(bar4, 5 * inch, 2.5 * inch)
        story_list.append(im6)
        story_list.append(Spacer(1, 12))


        # known

        ptext = '<font size=12>Known POCs:</font>'
        story_list.append(Paragraph(ptext, styles["Normal"]))
        story_list.append(Spacer(1, 2))
        if self.num_known == 0 and self.num_unknown == 0:
            ptext = '<font size=12>* There were not enough information to calculate known POCs info</font>'
            story_list.append(Paragraph(ptext, styles["Normal"]))
        else:
            ptext = '<font size=12>* %s of POCs are known to ABI</font>' % self.num_known
            story_list.append(Paragraph(ptext, styles["Normal"]))
            story_list.append(Spacer(1, 2))
            ptext = '<font size=12>* %s of POCs are not known to ABI</font>' % self.num_unknown
            story_list.append(Paragraph(ptext, styles["Normal"]))
            story_list.append(Spacer(1, 2))

        story_list.append(Spacer(1, 12))


        # generating known plot

        plt.clf()
        y_pos = np.arange(2)

        plt.bar(y_pos, [self.num_known, self.num_unknown], align='center', alpha=0.5)
        plt.xticks(y_pos, ["Yes", "No"])
        plt.ylabel('NUmber of POCs')
        plt.title('Fig 5')
        plt.savefig('plots/known.png')

        # adding unkown plot
        bar5 = "plots/known.png"
        im7 = Image(bar5, 5 * inch, 2.5 * inch)
        story_list.append(im7)
        story_list.append(Spacer(1, 12))

        story_list.append(Spacer(1, 12))

        pdf_document.build(story_list)

        # ptext = '<font size=14>%s</font>' % subtitle2
        # Story.append(Paragraph(ptext, styles["Normal"]))
        # Story.append(Spacer(1, 12))
        # Story.append(Spacer(1, 12))
        #pdf_document.build(story_list)


#### Initialize Report ####
#Initializing an instance of the report class here. This will allow myReport to be accessed
#throught the program by just importing reports.py (from tool import reports).
#Look at StackOverflow URLs below for more details.
#http://stackoverflow.com/questions/10942831/import-instance-of-class-from-a-different-module
#http://stackoverflow.com/questions/7290164/access-class-instance-from-one-file-in-another-file
#http://stackoverflow.com/questions/12487549/how-safe-is-it-to-import-a-module-multiple-times
####
output_filename = config.REPORT_FILE_NAME + '_' + utils.time_now()
output_path = os.path.join(config.REPORT_OUTPUT_FOLDER, output_filename)
report = Report(output_path)




#### For testing - Run the file directly ####
if __name__ == '__main__':
    from tools import config
    output_filename = config.REPORT_FILE_NAME + '_' + utils.time_now() + '.' + config.REPORT_EXTENSION
    output_path = os.path.join(output_filename)
    myReport = Report(output_path)
    myReport.self_duplicates.fillna(0)
    for i in range(5):
        myReport.self_duplicates.loc[i] = [randint(-100,100) for n in range(2)]
        myReport.cross_matches.loc[i]= [randint(-100, 100) for n in range(3)]
    myReport.saveReport()