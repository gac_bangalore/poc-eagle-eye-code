import pycuda.driver as cuda
import pycuda.autoinit
from pycuda.compiler import SourceModule
import numpy as np
from math import ceil


def match_algo(doc, doctypes, secondDoc=None, secondDocTypes = None):
    
    names = doc[:, 0].astype(np.str_)
    stNum = doc[:, 1].astype(np.str_)
    stName = doc[:, 2].astype(np.str_)
    city = doc[:, 3].astype(np.str_)
    postCode = doc[:, 4].astype(np.str_)
    lat = doc[:, 5].astype(np.str_)
    lon = doc[:, 6].astype(np.str_)
    
    if secondDoc:
        names2 = doc[:, 0].astype(np.str_)
        stNum2 = doc[:, 1].astype(np.str_)
        stName2 = doc[:, 2].astype(np.str_)
        city2 = doc[:, 3].astype(np.str_)
        postCode2 = doc[:, 4].astype(np.str_)
        lat2 = doc[:, 5].astype(np.str_)
        lon2 = doc[:, 6].astype(np.str_)
        names2_gpu = cuda.mem_alloc(names2.nbytes)
        stNum2_gpu = cuda.mem_alloc(stNum2.nbytes)
        stName2_gpu = cuda.mem_alloc(stName2.nbytes)
        city2_gpu = cuda.mem_alloc(city2.nbytes)
        postCode2_gpu = cuda.mem_alloc(postCode2.nbytes)
        lat2_gpu = cuda.mem_alloc(lat2.nbytes)
        lon2_gpu = cuda.mem_alloc(lon2.nbytes)
        cuda.memcpy_htod(names2_gpu, names2)
        cuda.memcpy_htod(stNum2_gpu, stNum2)
        cuda.memcpy_htod(stName2_gpu, stName2)
        cuda.memcpy_htod(city2_gpu, city2)
        cuda.memcpy_htod(postCode2_gpu, postCode2)
        cuda.memcpy_htod(lat2_gpu, lat2)
        cuda.memcpy_htod(lon2_gpu, lon2)
        doc_gpu = cuda.mem_alloc(secondDoc.nbytes)
        cuda.memcpy_htod(doc_gpu, secondDoc)
    else:
        #Allocate memory and transfer data to GPU
        names_gpu = cuda.mem_alloc(names.nbytes)
        stNum_gpu = cuda.mem_alloc(stNum.nbytes)
        stName_gpu = cuda.mem_alloc(stName.nbytes)
        city_gpu = cuda.mem_alloc(city.nbytes)
        postCode_gpu = cuda.mem_alloc(postCode.nbytes)
        lat_gpu = cuda.mem_alloc(lat.nbytes)
        lon_gpu = cuda.mem_alloc(lon.nbytes)
        cuda.memcpy_htod(names_gpu, names)
        cuda.memcpy_htod(stNum_gpu, stNum)
        cuda.memcpy_htod(stName_gpu, stName)
        cuda.memcpy_htod(city_gpu, city)
        cuda.memcpy_htod(postCode_gpu, postCode)
        cuda.memcpy_htod(lat_gpu, lat)
        cuda.memcpy_htod(lon_gpu, lon)
    duplicates = np.zeros((len(doc), 1), dtype=np.ndarray)
    duplicates_gpu = cuda.mem_alloc(duplicates.nbytes)
    cuda.memcpy_htod(duplicates_gpu, duplicates)
    

    blockDimX = 1024
    blockDimY = 1
    blockDimZ = 1
    gridDimX = ceil(len(doc)/blockDimX)
    gridDimY = 1
    gridDimZ = 1
    
    
    #Row = blockIdx.x * blockDim.x + threadIdx.x
    if secondDoc:
        pass
        mod = SourceModule("""
        __global__ void match(char **names, char  **stNum, char **stName, char **city, char **postCode, char **lat, char **lon)
        {
    
        
        }    
        """)
    
    else:
        
        mod = SourceModule("""
        #include <stdio.h>
        #include <string.h>        
        
        __global__ void match(int *duplicates, char **names, char  **stNum, char **stName, char **city, char **postCode, char **lat, char **lon)
        {
        
            int _postal_code_exists(char *recordA, char *recordB){
                if ((recordA[4] && records[b]) && 
                    (recordA[4] != 'NA' && recordB[4] != 'NA')){
                    return 1;                    
                    }
                else {
                    return 0;                
                }
            }
            int _match_postal_code(char *recordA, char *recordB){
                if (recordA[4] == recordB[4])
                    return 1;
                return 0;
            }
            int _match_street_number(char *recordA, char *recordB){
                if (recordA[1] == recordB[1])
                    return 1;
                return 0;
            }
            int _match_street_name(char *recordA, char *recordB){
                if (recordA[2] == recordB[2])
                    return 1;
                return 0;
            }
            int _match_name(char *recordA, char *recordB){
                if (recordA[0] == recordB[0])
                    return 1;
                return 0;
            }
            int _match_city(char *recordA, char *recordB){
                if (recordA[3] == recordB[3])
                    return 1;
                return 0;
            }
            
            def _is_bigram(stringA, stringB, threshold=0.70):
                stringA = {stringA[i:i+2] for i in range(len(stringA) - 1)}
                stringB = {stringB[i:i+2] for i in range(len(stringB) - 1)}
                intersection =len(stringA & stringB)
            
            int _is_bigram(char *stringA, char *stringB, float threshold){
                for(int i=0; i < strlen(stringA) - 1; i++){
                
                }
                for(int i=0; i < strlen(stringB) - 1; i++){
                
                }
                
                int minimum = 0
                if (strlen(stringA) < strlen(stringB)
                    minimum = strlen(stringA)
                else
                    minimum = strlen(stringB)
                if (intersection >= 0 && minimum == 0)
                    return 1;
                if (intersection / minimum > threshold)
                    return 1;
                return 0;
            }
            
            int _similar_postal_code(char *recordA, char *recordB){
                if (recordA[4][:3] == recordB[4][:3])
                    return 1;
                return 0;
            }
            
            int _match_address(char *addressA, char *addressB){
                if (addressA[5] == addressB[5])
                    return 1;
                return 0;
            }
            
            int _similar_address(char *addressA, char *addressB, float threshold){
                return _is_bigram(addressA[5], addressB[5], threshold);           
            }
     
            int _similar_name(char *recordA, char *recordB, float threshold){
                recordA = standardize(recordA[0]);
                recordB = standardize(recordB[0]);
                return _is_bigram(recordA, recordB, threshold);
            }
            
            int _similar_geo_position(record, row, float bound){
                if ((record[5] && record[6] && row[5] && row[6]) && 
                    (str(record[5]) != 'NA' && str(record[6]) != 'NA' && str(row[5]) != 'NA' && str(row[6]) != 'NA')){
                    int distLat = int(record[5]) - int(row[5])
                    int distLon = int(record[6]) - int(row[6])                
                    
                    if ((distLat > (distLat - bound) && distLat < (distLat + bound)) && 
                        (distLon > (distLon - bound) && distLon < (distLon + bound))){
                            return 1;                    
                    }
                }     
                return 0;
            }


            

        
            void _compare_records(rowIndex, row):
                if (_postal_code_exists(record,row))
                    if (_match_postal_code(record, row))
                        if (_match_address(record, row)){
                            if (_match_name(record, row)){
                                (_match_found(rowIndex));
                            }
                            else{
                                if (_similar_name(record, row, var_threshold))
                                    _match_found(rowIndex);
                                }
                        }
                        else{
                            if (_similar_address(record, row, threshold)){
                                if (_match_name(record, row)){
                                    _match_found(rowIndex);
                                }
                                else{
                                    if (_similar_name(record, row, var_threshold))
                                        _match_found(rowIndex);
                                }
                            }
                        }
                    else{
                        if (_similar_postal_code(record, row)){
                            if (_match_address(record, row)){
                                if (_match_name(record, row)){
                                    _match_found(rowIndex);
                                }
                                else{
                                    if (_similar_name(record, row, var_threshold))
                                        _match_found(rowIndex);
                                }
                            }
                            else{
                                if (_similar_address(record, row, threshold)){
                                    if (_match_name(record, row)){
                                        _match_found(rowIndex);
                                    }
                                    else{
                                        if (_similar_name(record, row, var_threshold))
                                            _match_found(rowIndex);
                                    }
                                }
                            }
                        }
                    }
                else{
                    if (_match_city(record, row)){
                        if (_match_address(record, row)){
                            if (_match_name(record, row)){
                                _match_found(rowIndex);
                            }
                            else{
                                if (_similar_name(record, row, var_threshold))
                                    _match_found(rowIndex);
                            }
                        }
                        else{
                            if (_similar_address(record, row, threshold)){
                                if (_match_name(record, row)){
                                    _match_found(rowIndex);
                                }
                                else{
                                    if (_similar_name(record, row, var_threshold))
                                        _match_found(rowIndex);
                                }
                            }
                        }
                    }
                    else{
                        if (_similiar_geo_position(record, row, 0.0005)){
                            if (_match_name(record, row)){
                                _match_found(rowIndex);
                            }
                            else{
                                if (_similar_name(record, row, var_threshold))
                                    _match_found(rowIndex);  
                            }
                        }
                    }
                }
              
            void _match_found(int rowIndex){
                duplicates.append([recordIndex, rowIndex]);            
            }
              
            
            duplicates = []
            var_threshold = threshold
            
            # Save full street address is single field
            record[5] = str(record[1]) +  ' ' + str(record[2])
            for rowIndex, row in enumerate(docShared):
                row[5] = str(row[1]) + ' ' + str(row[2])
            
            for rowIndex, row in enumerate(docShared):
                if filesNum == 1:
                    if rowIndex > recordIndex:
                        _compare_records(rowIndex, row)
                else:
                    _compare_records(rowIndex, row)
            return duplicates
        
        }    
        """)
    
    #Ref compiled & loaded func from device
    match = mod.get_function("match")
    match(cuda.Out(duplicates), cuda.In(names), grid=(gridDimX, gridDimY, gridDimZ), block=(blockDimX,blockDimY,blockDimZ))
    
    #Fetch data back from GPU
    doc_dup = np.empty_like(doc)
    cuda.memcpy_dtoh(doc_dup, doc_gpu)